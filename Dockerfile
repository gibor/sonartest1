FROM node:12.18.4
WORKDIR /usr/src/app
#RUN apk --no-cache add --virtual native-deps \ g++ gcc libgcc libstdc++ linux-headers make python && \ npm install --quiet node-gyp -g &&\ npm install --quiet && \ apk del native-deps
COPY package.json ./
#RUN npm install gulp-sass --save-dev
#RUN npm install node-sass@4.14.1
#RUN npm install node-sass
#RUN npm install node-sass@4.14.1
RUN npm install -g serve && npm install -g --unsafe-perm node-sass@4.14.1
#RUN npm install -g serve && npm install -g --unsafe-perm node-sass
#COPY . .
#RUN apk add g++ make python
RUN npm install
RUN npm install -g react-scripts
COPY . ./
#RUN apk add g++ make python
#Run npm install node-sass
#USER root
#RUN react-scripts build
RUN react-scripts build
EXPOSE 3000
CMD [ "serve", "-l", "3000", "-s", "build/" ]
