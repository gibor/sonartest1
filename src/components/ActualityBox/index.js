import React, { useState, useEffect } from "react";
import "./index.css";
import {  Link } from "react-router-dom";
import service from "../../service";
import config from "../../config";
import axios from "axios";
import moment from 'moment';
import 'moment/locale/fr'

moment.locale('fr');
var mois = 'Janvier_Février_Mars_Avril_Mai_Juin_Juillet_Août_Septembre_Octobre_Novembre_Décembre'.split('_');
moment.updateLocale('fr', { months : mois });
const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);

export default function ActualityBox(props) {
  const [apiAllActuality, setApiAllActuality] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    fetchActualitiesData();
  }, [props.exclus]);

  const fetchActualitiesData = async () => {
    //Fetch actualities from API...
    setIsLoading(true);
    setIsError(false);

    try {
      let res = await $service.getAllActualities({
        _limit: props.limit,
        _start: 0,
        _sort:"published_at:DESC",
        _id_ne:props.exclus,
      });

      if (res) {
        
        setApiAllActuality(res);
      }

      setIsLoading(false);
    } catch (err) {
      
      setIsError(true);
      setIsLoading(false);
    }
  };

  let listActualities;
  if (apiAllActuality) {
    listActualities = apiAllActuality.map((post, i) => {
      let r = (
        <div
          className="uk-card uk-card-default uk-card-body lpb-paddingcard"
          key={i.toString()}
        >
          <div className="lpb-foyer">
            <div />
            <img
              src={`${config.backofficeURL}${post.cover.url}`}
              alt="cover"
              className="lpb-imgblocbbpreinsciption"
            />
          </div>
          <div className="">
            <div className="lpb-home-page-actuality-post-box">
              <div className="lpb-home-page-actuality-post-date">
                {moment(post.createdAt).format('DD MMMM YYYY')}
              </div>
              <div className="lpb-home-page-actuality-post-title">
                {post.title}
              </div>
              <div className="lpb-home-page-actuality-post-desc">
                {post.description}
              </div>
              <div className="uk-text-center lpb-home-page-actuality-post-btn">
                <Link to={`/actualites/details/${post.id}`}>
                  <button className="uk-button uk-button-default lpb-home-page-actuality-post-read-more">
                    Lire la suite
                  </button>
                </Link>
              </div>
            </div>

            <div className="lpb-home-page-actuality-post-divider" />
          </div>
        </div>
      );
      return r;
    });
  }

  return (
    <div>
      <div className="uk-container">
        <div
          className="uk-grid-match  uk-child-width-1-4@s lpb-marginbloc"
          data-uk-grid
        >
          {listActualities}
        </div>
      </div>

      {props.isOthers && (
        <div className="uk-text-center lpb-actubtnvoirplus">
          <button className=" lpb-btnactualite">Voir plus</button>
        </div>
      )}
    </div>
  );
}
