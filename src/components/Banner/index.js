import React from "react";
import "./index.css";
import config from "../../config";
export default function Banner(props) {
  return (
    <div>
      <div>
        <div className="uk-section-default">
          <div
            className="uk-section lpb-banner-have-space uk-light uk-background-cover"
            style={{ backgroundImage: `url(${props.backgroundImage})` }}
          >
            <div className="uk-container lpb-banner-container">
              {props.position && (
                <div className={`uk-text-${props.position}`}>
                  {props.title && (
                    <div className="lpb-banner-title-content">
                      {props.title}
                    </div>
                  )}
                  {props.desc && (
                    <div className="lpb-banner-desc-content">{props.desc}</div>
                  )}
                  {props.typeBtn === "comingSoon" && (
                    <button className="uk-button uk-button-default lpb-header-btn-comingsoong">
                      {props.btnText}
                    </button>
                  )}
                  {props.typeBtn === "another" && (
                    <a
                      style={{ textDecoration: "none" }}
                      href={`${props.btnUrl}`}
                    >
                      <button className="uk-button uk-button-default lpb-header-btn-comingsoong">
                        {props.btnText}
                      </button>
                    </a>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
