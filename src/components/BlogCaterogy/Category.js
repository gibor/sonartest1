import React from "react";
import { Link } from "react-router-dom";
export default function Category(props) {
    let {category,onClick,isChecked}=props;
    return (
    <li
        class="uk-parent lpb-margindrop"
        onClick={onClick}
    >
        <a className="lpb-typeBlog " href="#">
        <input
            className="uk-checkbox lpb-checkbox-blog"
            checked={isChecked ? "checked" : null}
            type="checkbox"
        />
        {category.name}
        </a>
        { category.blogs.length>0 &&
        <ul class="uk-nav-sub lpb-liste-uk-parent">
            {category.blogs.map((article,key)=>
                <li key={key}>
                    <Link to={`/posts/${article.id}`} className="lpb-textType">
                        {article.title}
                    </Link>
                </li>
            )}
        </ul>
        }
    </li>
    );
}
