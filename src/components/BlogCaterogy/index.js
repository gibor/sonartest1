import React from "react";
import "./index.css";
import Category from "./Category";

export default function BlogCaterogy(props) {
  const {activeCategory,categories,onCategoryChange}=props
  const onSearch=(e)=>{
      props.filter(e.target.value);
  }
  return (
  <div className=" sidebarcolor ">
    <div className="uk-card uk-card-default paddingsearch">
      <form class="uk-search uk-search-navbar colorsearch ">
        <span
          class="uk-search-icon-flip lpb-icon-search"
          data-uk-search-icon
        ></span>
        <input
          class="uk-search-input lpb-search-input"
          type="search"
          onChange={onSearch}
          placeholder="Recherche..."
        />
      </form>
    </div>
    <div className="lpb-trait"></div>

    <div className="lpb-sizecat">Catégories</div>

    <div>
      {
        categories.length>0 &&
        <ul class="uk-nav-default uk-nav-parent-icon" data-uk-nav>
          {categories.map((category,key)=>
            <Category isChecked={activeCategory && activeCategory.name===category.name } onClick={()=>onCategoryChange(category)} category={category} key={key}/>)}
        </ul>
      }
    </div>
  </div>
  );
}
