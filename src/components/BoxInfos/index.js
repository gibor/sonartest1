import React from "react";
import "./index.css";

export default function BoxInfos(props) {
  return (
    <div>
      <div>
        <div className="lpb-contact-box-infos-container">
          <div className="lpb-contact-box-infos-title">{props.title}</div>
          <hr className="lpb-contact-box-infos-divider" />
          {props.address && (
            <div className="lpb-contact-box-infos-subtitle">
              <svg width="9" height="13" viewBox="0 0 9 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4.03125 12.2656C4.24219 12.5938 4.73438 12.5938 4.94531 12.2656C8.36719 7.34375 9 6.82812 9 5C9 2.51562 6.98438 0.5 4.5 0.5C1.99219 0.5 0 2.51562 0 5C0 6.82812 0.609375 7.34375 4.03125 12.2656ZM4.5 6.875C3.44531 6.875 2.625 6.05469 2.625 5C2.625 3.96875 3.44531 3.125 4.5 3.125C5.53125 3.125 6.375 3.96875 6.375 5C6.375 6.05469 5.53125 6.875 4.5 6.875Z" fill="#A8B674" />
              </svg>
              <div dangerouslySetInnerHTML={{
                __html: props.address
              }} />
            </div>
          )}
          {props.phone && (
            <div className="lpb-contact-box-infos-subtitle">
              <svg width={12} height={13} viewBox="0 0 12 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.6484 8.98438L9.02344 7.85938C8.95312 7.83594 8.88281 7.8125 8.78906 7.8125C8.625 7.8125 8.46094 7.90625 8.36719 8.02344L7.19531 9.45312C5.36719 8.58594 3.91406 7.13281 3.04688 5.30469L4.47656 4.13281C4.59375 4.03906 4.6875 3.875 4.6875 3.6875C4.6875 3.61719 4.66406 3.54688 4.64062 3.47656L3.51562 0.851562C3.42188 0.640625 3.21094 0.5 2.97656 0.5C2.95312 0.5 2.90625 0.523438 2.85938 0.523438L0.421875 1.08594C0.164062 1.15625 0 1.36719 0 1.625C0 7.64844 4.85156 12.5 10.875 12.5C11.1328 12.5 11.3438 12.3359 11.4141 12.0781L11.9766 9.64062C11.9766 9.59375 11.9766 9.54688 11.9766 9.52344C11.9766 9.28906 11.8359 9.07812 11.6484 8.98438Z" fill="#A8B674" />
              </svg>
              <a href={`tel:${props.number}`} style={{ color: "#565656" }}>
                <div dangerouslySetInnerHTML={{
                  __html: props.phone
                }} />
              </a>
            </div>
          )}
          {props.time && (
            <div className="lpb-contact-box-infos-subtitle">
              <svg width={12} height={13} viewBox="0 0 12 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 0.6875C2.78906 0.6875 0.1875 3.28906 0.1875 6.5C0.1875 9.71094 2.78906 12.3125 6 12.3125C9.21094 12.3125 11.8125 9.71094 11.8125 6.5C11.8125 3.28906 9.21094 0.6875 6 0.6875ZM8.15625 8.02344L7.6875 8.60938C7.61719 8.70312 7.52344 8.77344 7.38281 8.77344C7.3125 8.77344 7.21875 8.72656 7.17188 8.67969L5.60156 7.50781C5.36719 7.34375 5.25 7.08594 5.25 6.78125V3.125C5.25 2.9375 5.41406 2.75 5.625 2.75H6.375C6.5625 2.75 6.75 2.9375 6.75 3.125V6.5L8.10938 7.50781C8.17969 7.57812 8.25 7.67188 8.25 7.78906C8.25 7.88281 8.20312 7.97656 8.15625 8.02344Z" fill="#A8B674" />
              </svg>
              <div dangerouslySetInnerHTML={{
                __html: props.time
              }} />
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
