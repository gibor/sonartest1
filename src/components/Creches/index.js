import React from "react";
import parse from "html-react-parser";
import config from "../../config";
import { HashLink } from "react-router-hash-link";
import "./index.css";
import imgsectionbb from "../../assets/img/imgsectionbb.png";
import imgsectionpetit from "../../assets/img/imgsectionpetit.png";
import imgsectiongrand from "../../assets/img/imgsectiongrand.png";
import imagemarron from "../../assets/img/imagemarron.png";

const scrollWidthOffset = (el, yOffset = -90) => {
  const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
  window.scrollTo({ top: yCoordinate + yOffset, behavior: "smooth" });
}; 

export default function Creches(props) {
  return (
    <div className="">
      <div className="">
        <div className="uk-container">
          <p className="uk-text-center lpb-textcenter">
            {props.bloctexte}
          </p>

          <p className="uk-text-center lpb-textcenterMaria">
            {props.authortexte}{" "}
          </p>
        </div>
        <div className="lpb-position uk-container">
          <div className="uk-margin  uk-width-1-2@s uk-card uk-card-default uk-card-body lpb-blocgreen">
            <div className="lpb-blocgreenint">
              {props.address}
            </div>
          </div>

          <div
            class="uk-grid-match uk-child-width-expand@s uk-text-center"
            data-uk-grid
          >
            <div class="uk-card uk-card-default uk-card-body lpb-foyerbloc">
              <img
                className="uk-align-left uk-margin-remove-adjacent lpb-imgleftbloc2"
                src={props.imagebloc2}
              />
            </div>

            <div class="uk-card uk-card-default uk-card-body lpb-foyerbloc">
              {" "}
              <p className="lpb-titrebloc2"> {props.titrecreche}</p>
              <p className="lpb-textebloc2">{props.textecreche}</p>
              {props.btncrechetexte && (
                <div className="lpb-leftbtn">
                  <a href={`/pré-inscription`}>
                    <button className=" lpb-btncreche">
                      {props.btncrechetexte}
                    </button>
                  </a>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      {props.titlebebe && (
        <div
          className="lpb-backsection uk-container"
          id="apropos"
          style={{
            backgroundImage: "url(" + imagemarron + ")",
          }}
        >
          <div
            class="uk-grid-match uk-child-width-expand@s uk-text-center lpb-marginbloc"
            data-uk-grid
          >
            <div class="uk-card uk-card-default uk-card-body lpb-paddingcard">
              {" "}
              <HashLink
                to="/nos-sections#bebe"
                scroll={(el) => scrollWidthOffset(el)}
                className="link primary"
              >
                <div className="lpb-foyer">
                  <div className="lpb-foyer-cover" />
                  <img
                    src={imgsectionbb}
                    alt="philosophy"
                    className="lpb-imgblocbbpreinsciption"
                  />
                  <div className="uk-text-center lpb-contentfoyer-text">
                    <h3 className="uk-text-center lpb-titleimgblocpreinscription">
                      {props.titlebebe}
                    </h3>
                  </div>
                </div>
              </HashLink>
              <p className="uk-text-center lpb-textcenterbloc">
                {" "}
                {props.textebebe}
              </p>
            </div>

            <div class="uk-card uk-card-default uk-card-body lpb-paddingcard">
              {" "}
              <HashLink
                to="/nos-sections#petit"
                scroll={(el) => scrollWidthOffset(el)}
                className="link primary"
              >
                <div className="lpb-foyer">
                  <div className="lpb-foyer-cover" />
                  <img
                    src={imgsectionpetit}
                    alt="philosophy"
                    className="lpb-imgblocbbpreinsciption"
                  />
                  <div className="uk-text-center lpb-contentfoyer-text">
                    <h3 className="uk-text-center lpb-titleimgblocpreinscription">
                      {props.titlepetit}
                    </h3>
                  </div>
                </div>
              </HashLink>
              <p className="uk-text-center lpb-textcenterbloc">
                {props.textepetit}
              </p>
            </div>

            <div class="uk-card uk-card-default uk-card-body lpb-paddingcard">
              {" "}
              <HashLink
                to="/nos-sections#moyen"
                scroll={(el) => scrollWidthOffset(el)}
                className="link primary"
              >
                <div className="lpb-foyer">
                  <div className="lpb-foyer-cover" />
                  <img
                    src={imgsectiongrand}
                    alt="philosophy"
                    className="lpb-imgblocbbpreinsciption"
                  />
                  <div className="uk-text-center lpb-contentfoyer-text">
                    <h3 className="uk-text-center lpb-titleimgblocpreinscription">
                      {props.titlemoyen}
                    </h3>
                  </div>
                </div>
              </HashLink>
              <p className="uk-text-center lpb-textcenterbloc ">
                {props.textemoyen}
              </p>
            </div>
          </div>
          <div className="uk-text-center">
            <a href={`/pré-inscription`}>
              <button className="uk-text-center lpb-btnpreinscription">
                Pré-inscription
              </button>
            </a>
          </div>
        </div>
      )}
    </div>
  );
}
