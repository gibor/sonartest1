import React, { useState, useEffect, Fragment } from "react";
import "./index.css";
import moment from "../../helpers/moment";
import service from "../../service";
import config from "../../config";
import axios from "axios";
import { Link } from "react-router-dom";
const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);
const LastArticle=(props)=> {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [articles, setArticles]=useState([]);
  useEffect(() => {
    getArticles();
  }, [props.exclus]);

  const  getArticles = async () => {
    try {
      let response = await $service.getArticles({
        _limit: 3,
        _sort:"published_at:DESC",
        _id_ne:props.exclus,
      });
      if (response) {
        setArticles(response);
      }
    } catch (err) {
      setIsError(true);
    }
    setIsLoading(false);
  };

    return (
      <div>
        <div className=" sidebarcolor2 ">
          <div className="lpb-titreDernierA">Derniers articles</div>
          {
            !isLoading && articles.length>0 && articles.map((article,index)=>
            <Fragment key={index}>
              <div className="uk-card uk-card-default paddingsearch" data-uk-grid>
                <div className="uk-width-1-2">
                  {" "}
                  <div className="uk-text-center">
                    <img src={`${config.backofficeURL}${article.cover.url}`} alt={article.title} className="dernArticleimg" />
                    <div className="lpb-date"> </div>

                    <div className="lpb-month"> 
                       {moment(new Date(article.published_at)).format("MMMM YYYY")}
                    </div>
                  </div>
                </div>
                <div className="uk-width-expand lpb-padA">
                  {" "}
                  <Link to={`/posts/${article.id}`}>
                    {" "}
                    <div className="lpb-subTitle">{article.title}</div>
                  </Link>
                  <p className="lpb-textdernA">
                    {article.description}
                  </p>
                </div>{" "}
              </div>
              {index< (articles.length - 1) ? <div className="lpb-trait-dernierA"></div>:null}
            </Fragment>
            )
          }
        </div>
      </div>
    );
  
}

export default LastArticle;
