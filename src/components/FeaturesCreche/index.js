import React from 'react';
import config from '../../config'
import "./index.css";
import imgTetine from "../../assets/img/tetine1.png";
import imgTetineGauche from "../../assets/img/tetinegauche.png";
import imgTetineDroite from "../../assets/img/tetinedroite.png";
import imgTetineDroiteMobile from "../../assets/img/bouchon-mobile-droite.png";

export default function FeaturesCreche() {
  return (
    <div>
      <div className="lpb-home-page-features-max-w">
        <div className="uk-text-center lpb-home-page-features-title">
          Le fonctionnement de nos structures
        </div>
        <div className="lpb-home-page-features-grid" data-uk-grid>
          <div className="uk-width-1-1@s uk-width-1-3@m">

            <div className="lpb-home-page-features-box-item lpb-home-page-features-box-item-red">
              <div className="lpb-home-page-features-box-item-mobile-disp">
                <div className="lpb-home-page-features-box-item-icon-content lpb-home-page-features-box-item-mobile-disp-first-item lpb-home-page-features-box-item-icon-content-red">
                  <svg viewBox="0 0 54 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M53.0081 15.4082L27.1516 0.118626C26.8705 -0.0395419 26.5332 -0.0395419 26.2522 0.118626L0.395643 15.4082C0.00217375 15.6191 -0.110246 16.0936 0.114594 16.4626L1.18258 17.9916C1.40742 18.3606 1.91331 18.4661 2.30678 18.2552L8.60229 14.5119V34.1774C8.60229 34.5992 8.93955 34.9155 9.38923 34.9155H11.3566C11.8063 34.9155 12.1435 34.5992 12.1435 34.1774V12.403L26.7019 3.80921L41.2602 12.403V34.1774C41.2602 34.5992 41.5975 34.9155 42.0472 34.9155H44.0145C44.4642 34.9155 44.8015 34.5992 44.8015 34.1774V14.5119L51.097 18.2552C51.4904 18.4661 51.9963 18.3606 52.2212 17.9916L53.2892 16.4626C53.514 16.0936 53.4016 15.6191 53.0081 15.4082Z" fill="#F251B1" />
                    <path d="M26.7023 17.886C28.8753 17.886 30.637 16.2337 30.637 14.1955C30.637 12.1573 28.8753 10.505 26.7023 10.505C24.5292 10.505 22.7676 12.1573 22.7676 14.1955C22.7676 16.2337 24.5292 17.886 26.7023 17.886Z" fill="#F251B1" />
                    <path d="M24.5101 29.9069L25.6905 28.1144L24.3977 27.3763C24.1167 27.2181 23.8918 26.9545 23.7232 26.6909L22.0931 29.1688C21.8683 29.4851 21.8683 29.8542 21.9807 30.1705L22.9363 32.543L22.6552 32.6484C21.9807 32.8066 21.5872 33.4392 21.8121 34.0719C21.9807 34.5991 22.4866 34.9154 22.9925 34.9154C23.1049 34.9154 23.2173 34.9154 23.3297 34.8627L24.9598 34.4409C25.2971 34.3355 25.5781 34.1246 25.7468 33.861C25.9154 33.5447 25.9154 33.2283 25.803 32.912L24.5101 29.9069Z" fill="#F251B1" />
                    <path d="M30.7488 32.6486L30.4115 32.5432L31.4233 29.9598C31.5357 29.5908 31.5357 29.2217 31.3109 28.9054L29.6808 26.5857C29.5122 26.8493 29.2874 27.1129 28.9501 27.2711L27.7135 28.0092L28.8377 29.6435L27.6011 32.8595C27.4886 33.1758 27.4886 33.4922 27.6573 33.8085C27.8259 34.0721 28.1069 34.283 28.4442 34.3884L30.0743 34.8102C30.1867 34.8629 30.2991 34.8629 30.4115 34.8629C30.9736 34.8629 31.4233 34.5466 31.592 34.0194C31.8168 33.4922 31.4233 32.8595 30.7488 32.6486Z" fill="#F251B1" />
                    <path d="M32.6043 21.9458L30.0748 19.2568C29.85 18.9931 29.5127 18.8877 29.1193 18.8877H24.229C23.8917 18.8877 23.5545 19.0459 23.2734 19.2568L20.8002 21.893L18.7204 24.002C18.2708 24.4766 18.2708 25.2147 18.7766 25.6365C19.0015 25.8474 19.3387 25.9529 19.6198 25.9529C19.9571 25.9529 20.2943 25.8474 20.5192 25.5838L22.5989 23.4221L23.4983 22.473L23.6669 25.5311H29.5127L29.6814 22.473L30.6369 23.4748L32.7167 25.5838C32.9415 25.8474 33.2788 25.9529 33.6161 25.9529C33.8971 25.9529 34.2344 25.8474 34.4592 25.6365C34.9651 25.2147 35.0213 24.4766 34.5154 24.002L32.6043 21.9458Z" fill="#F251B1" />
                  </svg>
                </div>
                <div className="lpb-home-page-features-box-item-title">
                  Crèches
                </div>
              </div>
              <div className="lpb-home-page-features-box-item-desc">
                Nous accueillons vos enfants âgés de 2 mois à 4 ans du <strong>lundi au vendredi de 5h à 22h.</strong> <br /> Nos établissements sont composés de
                plusieurs sections afin de proposer des activités adaptées et
                ajustées selon les âges et le développement des enfants.
              </div>
              <div className="lpb-home-page-features-box-item-btn-container">
                <a href={`/pré-inscription`}>
                  <button className="uk-button uk-button-default lpb-home-page-features-box-item-btn">
                    Pré- inscription
                    <svg
                      width={10}
                      height={14}
                      viewBox="0 0 10 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M8.90625 7.53125C9.1875 7.25 9.1875 6.78125 8.90625 6.5L2.84375 0.40625C2.53125 0.125 2.0625 0.125 1.78125 0.40625L1.0625 1.125C0.78125 1.40625 0.78125 1.875 1.0625 2.1875L5.875 7L1.0625 11.8438C0.78125 12.1562 0.78125 12.625 1.0625 12.9062L1.78125 13.625C2.0625 13.9062 2.53125 13.9062 2.84375 13.625L8.90625 7.53125Z"
                        fill="white"
                      />
                    </svg>
                  </button>
                </a>
              </div>
            </div>
            <div className="log-creche-pink-tetinegauche">
              <img src={imgTetineGauche} alt="Petits bouchons" />
            </div>
          </div>

          <div className="uk-width-1-1@s uk-width-1-3@m log-creche-green-tetine-container-relative" >

            <div className="lpb-home-page-features-box-item lpb-home-page-features-box-item-green">
              <div className="lpb-home-page-features-box-item-mobile-disp">
                <div className="lpb-home-page-features-box-item-icon-content lpb-home-page-features-box-item-icon-content-green">
                  <svg
                    width={54}
                    height={35}
                    viewBox="0 0 54 35"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M53.0081 15.4082L27.1516 0.118626C26.8705 -0.0395419 26.5332 -0.0395419 26.2522 0.118626L0.395643 15.4082C0.00217375 15.6191 -0.110246 16.0936 0.114594 16.4626L1.18258 17.9916C1.40742 18.3606 1.91331 18.4661 2.30678 18.2552L8.60229 14.5119V34.1774C8.60229 34.5992 8.93955 34.9155 9.38923 34.9155H11.3566C11.8063 34.9155 12.1435 34.5992 12.1435 34.1774V12.403L26.7019 3.80921L41.2602 12.403V34.1774C41.2602 34.5992 41.5975 34.9155 42.0472 34.9155H44.0145C44.4642 34.9155 44.8015 34.5992 44.8015 34.1774V14.5119L51.097 18.2552C51.4904 18.4661 51.9963 18.3606 52.2212 17.9916L53.2892 16.4626C53.514 16.0936 53.4016 15.6191 53.0081 15.4082Z"
                      fill="#3A8542"
                    />
                    <path
                      d="M26.7023 17.886C28.8753 17.886 30.637 16.2337 30.637 14.1955C30.637 12.1573 28.8753 10.505 26.7023 10.505C24.5292 10.505 22.7676 12.1573 22.7676 14.1955C22.7676 16.2337 24.5292 17.886 26.7023 17.886Z"
                      fill="#3A8542"
                    />
                    <path
                      d="M24.5103 29.9069L25.6907 28.1144L24.3979 27.3763C24.1168 27.2181 23.892 26.9545 23.7233 26.6909L22.0932 29.1688C21.8684 29.4851 21.8684 29.8542 21.9808 30.1705L22.9364 32.543L22.6553 32.6484C21.9808 32.8066 21.5873 33.4392 21.8122 34.0719C21.9808 34.5991 22.4867 34.9154 22.9926 34.9154C23.105 34.9154 23.2174 34.9154 23.3299 34.8627L24.96 34.4409C25.2972 34.3355 25.5783 34.1246 25.7469 33.861C25.9155 33.5447 25.9155 33.2283 25.8031 32.912L24.5103 29.9069Z"
                      fill="#3A8542"
                    />
                    <path
                      d="M30.7489 32.6486L30.4117 32.5432L31.4234 29.9598C31.5359 29.5908 31.5359 29.2217 31.311 28.9054L29.6809 26.5857C29.5123 26.8493 29.2875 27.1129 28.9502 27.2711L27.7136 28.0092L28.8378 29.6435L27.6012 32.8595C27.4888 33.1758 27.4888 33.4922 27.6574 33.8085C27.826 34.0721 28.1071 34.283 28.4443 34.3884L30.0744 34.8102C30.1868 34.8629 30.2992 34.8629 30.4117 34.8629C30.9738 34.8629 31.4234 34.5466 31.5921 34.0194C31.8169 33.4922 31.4234 32.8595 30.7489 32.6486Z"
                      fill="#3A8542"
                    />
                    <path
                      d="M32.6042 21.9458L30.0748 19.2568C29.85 18.9931 29.5127 18.8877 29.1192 18.8877H24.229C23.8917 18.8877 23.5545 19.0459 23.2734 19.2568L20.8002 21.893L18.7204 24.002C18.2708 24.4766 18.2708 25.2147 18.7766 25.6365C19.0015 25.8474 19.3387 25.9529 19.6198 25.9529C19.9571 25.9529 20.2943 25.8474 20.5191 25.5838L22.5989 23.4221L23.4983 22.473L23.6669 25.5311H29.5127L29.6813 22.473L30.6369 23.4748L32.7167 25.5838C32.9415 25.8474 33.2788 25.9529 33.616 25.9529C33.8971 25.9529 34.2343 25.8474 34.4592 25.6365C34.9651 25.2147 35.0213 24.4766 34.5154 24.002L32.6042 21.9458Z"
                      fill="#3A8542"
                    />
                  </svg>
                </div>
                <div className="lpb-home-page-features-box-item-title">
                  Foyers
                </div>
              </div>
              <div className="lpb-home-page-features-box-item-desc">
                Nos foyers de jour ouvrent leurs portes aux enfants scolarisés
                âgés de 4 à 12 ans du <strong> lundi au vendredi de 11h à 20h </strong> et de <strong>7h
                à 20h pendant les vacances scolaires.</strong> Nos équipes éducatives
                assurent les transports scolaires à midi et à 16h. Un service
                d’aide aux devoirs vous est également proposé.
              </div>
              <div className="lpb-home-page-features-box-item-btn-container">
                <a href={`/pré-inscription`}>
                  <button className="uk-button uk-button-default lpb-home-page-features-box-item-btn">
                    Pré- inscription
                    <svg
                      width={10}
                      height={14}
                      viewBox="0 0 10 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M8.90625 7.53125C9.1875 7.25 9.1875 6.78125 8.90625 6.5L2.84375 0.40625C2.53125 0.125 2.0625 0.125 1.78125 0.40625L1.0625 1.125C0.78125 1.40625 0.78125 1.875 1.0625 2.1875L5.875 7L1.0625 11.8438C0.78125 12.1562 0.78125 12.625 1.0625 12.9062L1.78125 13.625C2.0625 13.9062 2.53125 13.9062 2.84375 13.625L8.90625 7.53125Z"
                        fill="white"
                      />
                    </svg>
                  </button>
                </a>
              </div>
            </div>
            <div className="log-creche-green-tetine">
              <img src={imgTetine} alt="Petits bouchons" />
            </div>
            <div className="log-creche-green-tetine-mobile">
              <img src={imgTetineDroiteMobile} alt="Petits bouchons" />
            </div>
          </div>

          <div className="uk-width-1-1@s uk-width-1-3@m">

            <div className="lpb-home-page-features-box-item lpb-home-page-features-box-item-blue">
              <div className="lpb-home-page-features-box-item-mobile-disp">
                <div className="lpb-home-page-features-box-item-icon-content lpb-home-page-features-box-item-icon-content-blue">
                  <svg
                    width={54}
                    height={35}
                    viewBox="0 0 54 35"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M53.0082 15.4082L27.1516 0.118626C26.8705 -0.0395419 26.5333 -0.0395419 26.2522 0.118626L0.395643 15.4082C0.00217376 15.6191 -0.110246 16.0936 0.114594 16.4626L1.18258 17.9916C1.40742 18.3606 1.91331 18.4661 2.30678 18.2552L8.6023 14.5119V34.1774C8.6023 34.5992 8.93956 34.9155 9.38924 34.9155H11.3566C11.8063 34.9155 12.1435 34.5992 12.1435 34.1774V12.403L26.7019 3.80921L41.2603 12.403V34.1774C41.2603 34.5992 41.5975 34.9155 42.0472 34.9155H44.0146C44.4643 34.9155 44.8015 34.5992 44.8015 34.1774V14.5119L51.097 18.2552C51.4905 18.4661 51.9964 18.3606 52.2212 17.9916L53.2892 16.4626C53.5141 16.0936 53.4016 15.6191 53.0082 15.4082Z"
                      fill="#40B0CA"
                    />
                    <path
                      d="M26.7023 17.886C28.8753 17.886 30.637 16.2337 30.637 14.1955C30.637 12.1573 28.8753 10.505 26.7023 10.505C24.5292 10.505 22.7676 12.1573 22.7676 14.1955C22.7676 16.2337 24.5292 17.886 26.7023 17.886Z"
                      fill="#40B0CA"
                    />
                    <path
                      d="M24.5102 29.9069L25.6906 28.1144L24.3977 27.3763C24.1167 27.2181 23.8918 26.9545 23.7232 26.6909L22.0931 29.1688C21.8683 29.4851 21.8683 29.8542 21.9807 30.1705L22.9363 32.543L22.6552 32.6484C21.9807 32.8066 21.5872 33.4392 21.8121 34.0719C21.9807 34.5991 22.4866 34.9154 22.9925 34.9154C23.1049 34.9154 23.2173 34.9154 23.3297 34.8627L24.9598 34.4409C25.2971 34.3355 25.5782 34.1246 25.7468 33.861C25.9154 33.5447 25.9154 33.2283 25.803 32.912L24.5102 29.9069Z"
                      fill="#40B0CA"
                    />
                    <path
                      d="M30.749 32.6486L30.4117 32.5432L31.4235 29.9598C31.5359 29.5908 31.5359 29.2217 31.3111 28.9054L29.681 26.5857C29.5124 26.8493 29.2876 27.1129 28.9503 27.2711L27.7137 28.0092L28.8379 29.6435L27.6013 32.8595C27.4889 33.1758 27.4889 33.4922 27.6575 33.8085C27.8261 34.0721 28.1072 34.283 28.4444 34.3884L30.0745 34.8102C30.1869 34.8629 30.2993 34.8629 30.4117 34.8629C30.9738 34.8629 31.4235 34.5466 31.5921 34.0194C31.817 33.4922 31.4235 32.8595 30.749 32.6486Z"
                      fill="#40B0CA"
                    />
                    <path
                      d="M32.6043 21.9458L30.0749 19.2568C29.85 18.9931 29.5128 18.8877 29.1193 18.8877H24.229C23.8918 18.8877 23.5545 19.0459 23.2734 19.2568L20.8002 21.893L18.7204 24.002C18.2708 24.4766 18.2708 25.2147 18.7766 25.6365C19.0015 25.8474 19.3387 25.9529 19.6198 25.9529C19.9571 25.9529 20.2943 25.8474 20.5192 25.5838L22.5989 23.4221L23.4983 22.473L23.6669 25.5311H29.5128L29.6814 22.473L30.637 23.4748L32.7167 25.5838C32.9416 25.8474 33.2788 25.9529 33.6161 25.9529C33.8971 25.9529 34.2344 25.8474 34.4592 25.6365C34.9651 25.2147 35.0213 24.4766 34.5154 24.002L32.6043 21.9458Z"
                      fill="#40B0CA"
                    />
                  </svg>
                </div>
                <div className="lpb-home-page-features-box-item-title">
                  Garderie
                </div>
              </div>
              <div className="lpb-home-page-features-box-item-desc">
                Nous accueillons vos enfants tous les samedis de <strong>7h à 19h sur
                notre site de Foetz.</strong> <br /> Chaque samedi, nous proposons à votre
                enfant des activités ludiques et variées, afin que ce temps
                passé à la garderie devienne, pour lui, le moment du week-end
                à ne pas manquer !
              </div>
              <div className="lpb-home-page-features-box-item-btn-container">
                <a href={`/pré-inscription`}>
                  <button className="uk-button uk-button-default lpb-home-page-features-box-item-btn">
                    Pré- inscription
                    <svg
                      width={10}
                      height={14}
                      viewBox="0 0 10 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M8.90625 7.53125C9.1875 7.25 9.1875 6.78125 8.90625 6.5L2.84375 0.40625C2.53125 0.125 2.0625 0.125 1.78125 0.40625L1.0625 1.125C0.78125 1.40625 0.78125 1.875 1.0625 2.1875L5.875 7L1.0625 11.8438C0.78125 12.1562 0.78125 12.625 1.0625 12.9062L1.78125 13.625C2.0625 13.9062 2.53125 13.9062 2.84375 13.625L8.90625 7.53125Z"
                        fill="white"
                      />
                    </svg>
                  </button>
                </a>
              </div>
            </div>
            <div className="log-creche-blue-tetinedroite">
              <img src={imgTetineDroite} alt="Petits bouchons" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
