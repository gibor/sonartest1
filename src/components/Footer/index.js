import React, { useState, useEffect } from 'react';
import './index.css';
import {
    useLocation,
} from "react-router-dom";
import config from '../../config'
import logoImg from '../../assets/img/logo.png'
import auraImg from '../../assets/img/aura.png'
import aura2Img from '../../assets/img/aura2.png'
import naigymImg from '../../assets/img/naigym.png'
import { HashLink } from 'react-router-hash-link';
const scrollWidthOffset = (el,yOffset=-90) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' }); 
  }

export default function Footer() {
    let location = useLocation();
    const [menuFooter, setMenuFooter] = useState({})

    useEffect(() => {
        const fetchRoutesData = async () => {
            // Replace by API Fetch
            const routesFooter = {
                copyright: "© 2021 Crèche et Foyer de Jour Les P’tits bouchons. Tous droits réservés.",
                legalMentions: {
                    title: "Mentions légales",
                    url: "/mentions-legales",
                },
                politiqueConf: {
                    title: "Politique de confidentialité",
                    url: "/politique",
                },
                section1: [
                    {
                        title: "Aura Groupe",
                        url: "/a-propos-de-nous#blocauragroupe",
                    },
                    {
                        title: "Les P'tits Bouchons",
                        url: "/a-propos-de-nous#blocpetitsbouchons",
                    },
                    {
                        title: "N'AIRGYM",
                        url: config.NairgymLink,
                        isExternal: true,
                    },
                    { 
                        title: "Aura Academy",
                        url: "/a-propos-de-nous#blocauraacademy",
                    },
                ],
                section2: [
                    {
                        title: "Crèche et foyer de jour de Foetz",
                        url: "/foetz-crèche-et-foyer-de-jour",
                    },
                    { 
                        title: "Crèche de Leudelange",
                        url: "/leudelange-crèche",
                    },
                    {
                        title: "Foyer de Esch-sur-Alzette",
                        url: "/esch-sur-alzette-foyer",
                    },
                    {
                        title: "Crèche de Schifflange",
                        url: "/schifflange-crèche",
                    },
                ],
                section3: [
                    {
                        title: "Contactez-nous",
                        url: "/contact",
                    },
                    {
                        title: "Mentions légales",
                        url: "/mentions-legales",
                    },
                    {
                        title: "Politique de confidentialité",
                        url: "/politique",
                    },
                    {
                        title: "Partenaires",
                        url: "/partenaires",
                    },
                ],
                section4: [
                    {
                        title: "Facebook",
                        slug: "facebook",
                        url: "https://www.facebook.com/crechelesptitsbouchons.lu",
                    },
                    {
                        title: "Instagram",
                        slug: "instagram",
                        url: "https://www.instagram.com/lesptitsbouchons.lu/?hl=fr",
                    },
                    {
                        title: "Linkedin",
                        slug: "linkedin",
                        url: "https://lu.linkedin.com/company/aura-group-integrate-humanity",
                    },
                ],
            }
            setMenuFooter(routesFooter)
        }
        fetchRoutesData()
    }, [])

    let section1MenuWeb
    if (menuFooter.section1) {
        // A revoir pour le menu sur mobile
        section1MenuWeb = menuFooter.section1.map((el, i) =>
            <div key={i.toString()} className="lpb-footer-navbar-content">
                {el.isExternal ? (
                    <a className="lpb-footer-navbar-item" target="_blank" href={el.url}>{el.title}</a>
                ) : (
                    <HashLink className="lpb-footer-navbar-item" to={`${el.url}`} scroll={item => scrollWidthOffset(item)}>{el.title}</HashLink>
                )}
            </div>
        )
    }
    let section2MenuWeb
    if (menuFooter.section2) {
        // A revoir pour le menu sur mobile
        section2MenuWeb = menuFooter.section2.map((el, i) =>
            <div key={i.toString()} className="lpb-footer-navbar-content">
                <a className="lpb-footer-navbar-item" href={`${el.url}`}>{el.title}</a>
            </div>
        )
    }
    let section3MenuWeb
    if (menuFooter.section3) {
        // A revoir pour le menu sur mobile
        section3MenuWeb = menuFooter.section3.map((el, i) =>
            <div key={i.toString()} className="lpb-footer-navbar-content">
                <a className="lpb-footer-navbar-item" href={`${el.url}`}>{el.title}</a>
            </div>
        )
    }
    let section4MenuWeb
    if (menuFooter.section4) {
        // A revoir pour le menu sur mobile
        section4MenuWeb = menuFooter.section4.map((el, i) =>
            <div key={i.toString()} className="lpb-footer-navbar-content">
                {el.slug === "facebook" && (
                    <svg width={12} height={13} viewBox="0 0 12 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.8125 6.5C11.8125 3.28906 9.21094 0.6875 6 0.6875C2.78906 0.6875 0.1875 3.28906 0.1875 6.5C0.1875 9.40625 2.29688 11.8203 5.08594 12.2422V8.1875H3.60938V6.5H5.08594V5.23438C5.08594 3.78125 5.95312 2.96094 7.26562 2.96094C7.92188 2.96094 8.57812 3.07812 8.57812 3.07812V4.50781H7.85156C7.125 4.50781 6.89062 4.95312 6.89062 5.42188V6.5H8.50781L8.25 8.1875H6.89062V12.2422C9.67969 11.8203 11.8125 9.40625 11.8125 6.5Z" fill="#555555" />
                    </svg>
                )}
                {el.slug === "instagram" && (
                    <svg width={11} height={11} viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5 2.80469C4 2.80469 2.80469 4.02344 2.80469 5.5C2.80469 7 4 8.19531 5.5 8.19531C6.97656 8.19531 8.19531 7 8.19531 5.5C8.19531 4.02344 6.97656 2.80469 5.5 2.80469ZM5.5 7.25781C4.53906 7.25781 3.74219 6.48438 3.74219 5.5C3.74219 4.53906 4.51562 3.76562 5.5 3.76562C6.46094 3.76562 7.23438 4.53906 7.23438 5.5C7.23438 6.48438 6.46094 7.25781 5.5 7.25781ZM8.92188 2.71094C8.92188 2.35938 8.64062 2.07812 8.28906 2.07812C7.9375 2.07812 7.65625 2.35938 7.65625 2.71094C7.65625 3.0625 7.9375 3.34375 8.28906 3.34375C8.64062 3.34375 8.92188 3.0625 8.92188 2.71094ZM10.7031 3.34375C10.6562 2.5 10.4688 1.75 9.85938 1.14062C9.25 0.53125 8.5 0.34375 7.65625 0.296875C6.78906 0.25 4.1875 0.25 3.32031 0.296875C2.47656 0.34375 1.75 0.53125 1.11719 1.14062C0.507812 1.75 0.320312 2.5 0.273438 3.34375C0.226562 4.21094 0.226562 6.8125 0.273438 7.67969C0.320312 8.52344 0.507812 9.25 1.11719 9.88281C1.75 10.4922 2.47656 10.6797 3.32031 10.7266C4.1875 10.7734 6.78906 10.7734 7.65625 10.7266C8.5 10.6797 9.25 10.4922 9.85938 9.88281C10.4688 9.25 10.6562 8.52344 10.7031 7.67969C10.75 6.8125 10.75 4.21094 10.7031 3.34375ZM9.57812 8.59375C9.41406 9.0625 9.03906 9.41406 8.59375 9.60156C7.89062 9.88281 6.25 9.8125 5.5 9.8125C4.72656 9.8125 3.08594 9.88281 2.40625 9.60156C1.9375 9.41406 1.58594 9.0625 1.39844 8.59375C1.11719 7.91406 1.1875 6.27344 1.1875 5.5C1.1875 4.75 1.11719 3.10938 1.39844 2.40625C1.58594 1.96094 1.9375 1.60938 2.40625 1.42188C3.08594 1.14062 4.72656 1.21094 5.5 1.21094C6.25 1.21094 7.89062 1.14062 8.59375 1.42188C9.03906 1.58594 9.39062 1.96094 9.57812 2.40625C9.85938 3.10938 9.78906 4.75 9.78906 5.5C9.78906 6.27344 9.85938 7.91406 9.57812 8.59375Z" fill="#555555" />
                    </svg>
                )}
                {el.slug === "linkedin" && (
                    <svg width={11} height={11} viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10 0.25H0.976562C0.578125 0.25 0.25 0.601562 0.25 1.02344V10C0.25 10.4219 0.578125 10.75 0.976562 10.75H10C10.3984 10.75 10.75 10.4219 10.75 10V1.02344C10.75 0.601562 10.3984 0.25 10 0.25ZM3.41406 9.25H1.86719V4.25781H3.41406V9.25ZM2.64062 3.55469C2.125 3.55469 1.72656 3.15625 1.72656 2.66406C1.72656 2.17188 2.125 1.75 2.64062 1.75C3.13281 1.75 3.53125 2.17188 3.53125 2.66406C3.53125 3.15625 3.13281 3.55469 2.64062 3.55469ZM9.25 9.25H7.67969V6.8125C7.67969 6.25 7.67969 5.5 6.88281 5.5C6.0625 5.5 5.94531 6.13281 5.94531 6.78906V9.25H4.39844V4.25781H5.875V4.9375H5.89844C6.10938 4.53906 6.625 4.11719 7.375 4.11719C8.94531 4.11719 9.25 5.17188 9.25 6.50781V9.25Z" fill="#555555" />
                    </svg>
                )}
                <a style={{ marginLeft: '5px' }} className="lpb-footer-navbar-item" href={`${el.url}`} target="_blank" rel="noreferrer">{el.title}</a>
            </div>
        )
    }

    return (
        <div style={{ backgroundColor: "#F6F6F6" }}>
            <div className="uk-container lpb-footer-menu-wrap">
                <div className="lpb-footer-menu" data-uk-grid>
                    <div className="uk-width-1-1@s uk-width-1-5@m">
                    <HashLink to="/a-propos-de-nous#blocpetitsbouchons"    scroll={el => scrollWidthOffset(el)}>
                        <img className="lpb-footer-logo" src={logoImg} alt="logo" />
                        </HashLink>
                    </div>
                    <div className="uk-width-expand">
                        <div data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-4@m">
                                <div className="lpb-footer-navbar-group-title">
                                    NOTRE GROUPE
                                </div>
                                {section1MenuWeb}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-4@m">
                                <div className="lpb-footer-navbar-group-title">
                                    NOS CRÈCHES ET FOYERS
                                </div>
                                {section2MenuWeb}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-4@m">
                                <div className="lpb-footer-navbar-group-title">
                                    INFORMATIONS
                                </div>
                                {section3MenuWeb}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-4@m">
                                <div className="lpb-footer-navbar-group-title">
                                    NOS RÉSEAUX SOCIAUX
                                </div>
                                {section4MenuWeb}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="lpb-footer-logo-mobile">
                    <div className="uk-text-center">
                        <img className="lpb-footer-logo" src={logoImg} alt="logo" />
                    </div>
                </div>


                <div className="uk-text-center">
                    <HashLink to="/a-propos-de-nous#blocauragroupe"    scroll={el => scrollWidthOffset(el)}><img className="lpb-footer-partners-logo" src={auraImg} alt="logo" /></HashLink>
                    <HashLink to="/a-propos-de-nous#blocauraacademy"    scroll={el => scrollWidthOffset(el)}><img className="lpb-footer-partners-logo" src={aura2Img} alt="logo" /></HashLink>
                    <a href={`${config.NairgymLink}`} target="_blank" ><img className="lpb-footer-partners-logo" src={naigymImg} alt="logo" /></a>
                </div>
                <div className="lpb-footer-divider" />
                {menuFooter.legalMentions && (
                    <div className="uk-text-center lpb-footer-copyright">
                        {menuFooter.copyright}
                        <div className="lpb-footer-copyright-link">
                            <a href={`${menuFooter.legalMentions.url}`}>
                                {menuFooter.legalMentions.title}
                            </a> et <a href={`${menuFooter.politiqueConf.url}`}>
                                {menuFooter.politiqueConf.title}
                            </a>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}

 