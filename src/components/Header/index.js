import React, { useState, useEffect } from 'react';
import './index.css';
import {
    useLocation,
} from "react-router-dom";
import config from '../../config'
import logoImg from '../../assets/img/logo.png'
import menuMobileRight from '../../assets/svg/menu-mobile-right.svg'

export default function Header() {
    let location = useLocation();
    const [menuHeader, setMenuHeader] = useState([])

    useEffect(() => {
        const fetchRoutesData = async () => {
            // Replace by API Fetch
            const routesHeader = [
                {
                    title: "Accueil",
                    url: "/",
                    subMenu: false,
                },
                {
                    title: "Crèches & Foyers",
                    url: "#",
                    subMenu: true,
                    menu: [
                        {
                            title: "Foetz",
                            subTitle: "Crèche et Foyer de jour",
                            url: "/foetz-crèche-et-foyer-de-jour",
                        },
                        {
                            title: "Leudelange",
                            subTitle: "Crèche",
                            url: "/leudelange-crèche",
                        },
                        {
                            title: "Esch sur Alzette",
                            subTitle: "Foyer",
                            url: "/esch-sur-alzette-foyer",
                        },
                        {
                            title: "Schifflange",
                            subTitle: "Crèche",
                            url: "/schifflange-crèche",
                        },
                    ]
                },
                {
                    title: "Grandir avec nous",
                    url: "#",
                    subMenu: true,
                    menu: [
                        {
                            title: "Notre pédagogie",
                            url: "/notre-pédagogie",
                        },
                        {
                            title: "Nos sections",
                            url: "/nos-sections",
                        },
                        {
                            title: "Nos engagements",
                            url: "/nos-engagements",
                        },
                    ]
                },
                {
                    title: "A propos de nous",
                    url: "/a-propos-de-nous",
                    subMenu: false,
                },
                {
                    title: "Actualités",
                    url: "/actualites",
                    subMenu: false,
                },
                {
                    title: "Blog",
                    url: "/blog",
                    subMenu: false,
                },
                {
                    title: "Contact",
                    url: "/contact",
                    subMenu: false,
                },
            ]
            setMenuHeader(routesHeader)
        }
        fetchRoutesData()
    }, [])

    const checkSubMenuActive = (menu) => {
        var hasValueLessThanTen = false

        for (var i = 0; i < menu.length; i++) {
            if (location.pathname === menu[i].url) {
                hasValueLessThanTen = true
                break
            }
        }
        return hasValueLessThanTen
    }

    const listNavbarWeb = menuHeader.map((el, i) => {
        let r 
        if (el.subMenu) {
            r = <li key={i.toString()} className={`lpb-header-navbar-item-parent ${checkSubMenuActive(el.menu) ? 'lpb-header-navbar-item-parent-active' : ''}`}>
                <a className={`lpb-header-navbar-item ${checkSubMenuActive(el.menu) ? 'uk-active' : ''}`} href="#" onClick="return false">{el.title}</a>
                <div className="uk-navbar-dropdown lpb-header-navbar-drop-submenu-content">
                    <ul className="uk-nav uk-navbar-dropdown-nav uk-text-left">
                        {el.menu.map((item, j) =>
                            <li key={j.toString()} className={`lpb-header-navbar-item-drop-submenu ${location.pathname === item.url ? 'uk-active' : ''}`}>
                                <a href={`${item.url}`}>
                                    {item.subTitle ?
                                        <span><strong>{item.title} - </strong>{item.subTitle}</span>
                                        :
                                        <span>{item.title}</span>
                                    }
                                </a>
                            </li>
                        )}
                    </ul>
                </div>
            </li>
        } else {
            r = <li key={i.toString()} className={`lpb-header-navbar-item-parent ${location.pathname === el.url ? 'lpb-header-navbar-item-parent-active' : ''}`}>
                <a className={`lpb-header-navbar-item ${location.pathname === el.url ? 'uk-active' : ''}`} href={`${el.url}`}>{el.title}</a>
            </li>
        }
        return r
    })

    // A revoir pour les sous-menu sur mobile
    const listNavbarMobile = menuHeader.map((el, i) => {
        let r 
        if (el.subMenu) {
            r = <ul  key={i.toString()} className="lpb-header-mobile-link-content-ul uk-nav-default uk-nav-parent-icon" data-uk-nav>
                <li className="uk-parent lpb-header-mobile-link-content-li">
                    <a onClick="return false" className={`${checkSubMenuActive(el.menu) ? 'lpb-header-mobile-navbar-active' : ''}`} href="#">{el.title}</a>
                    <ul className="uk-nav-sub">
                        {el.menu.map((item, j) =>
                            <li key={j.toString()} className={`lpb-header-mobile-link-content-li ${location.pathname === item.url ? 'lpb-header-mobile-navbar-active-bg' : ''}`}>
                                <a href={`${item.url}`}>
                                    {item.subTitle ?
                                        <span><strong>{item.title} - </strong>{item.subTitle}</span>
                                        :
                                        <span>{item.title}</span>
                                    }
                                </a>
                            </li>
                        )}
                    </ul>
                </li>
            </ul>
        } else {
            r = <p key={i.toString()} className={`${location.pathname === el.url ? 'lpb-header-mobile-navbar-active' : ''}`}><a href={`${el.url}`}>{el.title}</a></p>
        }
        return r
    })

    return (
        <div>
            <div style={{ zIndex: 9980, background: 'white' }} data-uk-sticky="bottom: #offset" className="lpb-header-menu-web">
                <div className="uk-container">
                    <div className="uk-text-center lpb-header-menu">
                        <div className="lpb-vertical-auto">
                            <a className="" href={`/`}>
                                <img className="lpb-header-logo" src={logoImg} alt="logo" />
                            </a>
                        </div>
                        <div className="lpb-header-menu2">
                            <div className="lpb-vertical-auto">
                                <nav className="uk-navbar-container lpb-header-navbar" data-uk-navbar>
                                    <div className="uk-navbar-center">
                                        <ul className="uk-navbar-nav">
                                            {listNavbarWeb}
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                            <div className="lpb-vertical-auto">
                                <a className="lpb-header-pre-registration" href={`/pré-inscription`}>
                                    <button style={{ marginLeft: "14px" }} className="uk-button uk-button-default lpb-header-btn-register-full">
                                        Pré-Inscription
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{ zIndex: 9980 }} data-uk-sticky="bottom: #offset" className="lpb-header-menu-mobile">
                <div className="uk-text-left lpb-header-menu">
                    <div className="lpb-header-mobile-logo-wrap">
                        <a className="" href={`/`}>
                            <img className="lpb-header-logo" src={logoImg} alt="logo" />
                        </a>
                    </div>
                    <div className="lpb-header-mobile-line-navbar-wrap">
                        <button className="lpb-header-menu-mobile-btn" data-uk-toggle="target: #lpb-header-toggle-menu-mobile" type="button">
                            <div className="lpb-header-mobile-line-navbar-container">
                            <img src={menuMobileRight} alt="menu" />
                            </div>
                        </button>
                    </div>
                    <div>
                        <div id="lpb-header-toggle-menu-mobile" className="uk-offcanvas" data-uk-offcanvas="flip: true; overlay: true">
                            <div className="uk-offcanvas-bar lpb-header-mobile-toggle-container">
                                <button className="uk-offcanvas-close lpb-header-mobile-toggle-close" type="button"><span data-uk-icon="icon: close; ratio: 1" /></button>
                                <div className="lpb-header-mobile-toggle-navbar-content">
                                    <div className="lpb-header-mobile-link-content">
                                        {listNavbarMobile}
                                    </div>
                                    <a className="lpb-header-pre-registration" href={`/pré-inscription`}>
                                        <button className="uk-button uk-button-default lpb-header-btn-register-full">
                                            Pré-Inscription
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ); 
}

