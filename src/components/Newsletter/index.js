import React, { Component } from "react";
import './index.css'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import bgNewsletter from "../../assets/img/bg-newsletter.png";
import logo from "../../assets/img/logo.png";
import Thanks from '../../components/Thanks'
import { message as myMessage, Form, Input, Button } from "antd";
import jsonp from "jsonp";
import queryString from "query-string";
import { message, Spin } from 'antd';
/*import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'*/
import { LoadingOutlined } from '@ant-design/icons';
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
myMessage.config({
  top: 150,
});

export default class newsletter extends Component {
  state = {
    email: "",
    isSuscribe: false,
    isLoading: false,
    btnDesable: false,
    resultmessage: "",
    status: "",
  };
  changeEmail = (event) => {

    if(this.state.resultmessage.length>0) {
      this.setState({resultmessage:''});
    }
    const target = event.target;
    const value = target.value;
    const name = target.name;
    
    this.setState({ email: value });
    if(this.state.email=='') {
      myMessage.success('');
    }

    var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!target.value.match(re)) {
      this.setState({ btnDesable: false });
    }
    else {
      this.setState({ btnDesable: true });
    }



  };
  submit1 = () => {
    if ( window.fbq){
      window.fbq('track', 'Lead');
    }
    this.setState({ resultmessage: null });
    this.setState({ isLoading: true });
    // var data = new FormData();
    // data.append("EMAIL", "lolo@gmail.com");
    // 
    
    //   https://gmail.us2.list-manage.com/subscribe/post?u=db2d2664c0ba26c06465a09f9&amp;id=d8a5ce5c0b
    jsonp(
      `https://luxdatadigit.us7.list-manage.com/subscribe/post-json?u=1cf303e76b8130d578d97b92c&id=cf3caa1ee2&EMAIL=${this.state.email}`,
      {
        param: "c",
      },
      (err, data) => {
        
        
        if (data) {
          if (data.result === "success") {
            this.setState({ email: "" });
            this.setState({ isSuscribe: true, status: "success", isLoading: false, });
            
          } else {
            this.setState({ isLoading: false });
            this.setState({ email: "" });
            if (data.msg.indexOf("Worlis already subscribed to list tisloyd")) {
              this.setState({
                resultmessage: "Vous avez déjà souscrit à notre newsletter",
              });
              this.setState({ status: "duplicate" });
            } else {
              this.setState({ resultmessage: "Une erreur s'est produite " });
              this.setState({ status: "error" });
            }
          }
        } else {
          this.setState({
            resultmessage:
              "Il s'est produit une erreur, Veuillez réessayer ultérieurement ",
          });
          this.setState({ status: "error" });
        }
      }
    );
  };

  onClosedPopup = (isClosed) => {
    this.setState({ isSuscribe: false, });
  }

  render() {
    const CustomForm = ({ status, message, onValidated }) => {
      let email;
      const submit = () => {
        email &&
          email.value.indexOf("@") > -1 &&
          onValidated({ EMAIL: email.value });
      };
      return (
        <div>
          {status === "sending" && (
            <div style={{ color: "blue" }}>Envoi en cours...</div>
          )}
          {status === "error" &&
            myMessage.error("L'opération a échoué: " + message, 700)}
          {status === "success" &&
            myMessage.success("L'email a bien été enregistré !")}
          {status === "empty" && myMessage.error("Veuillez entrer un email !")}
          {status === "duplicate" &&
            myMessage.error("Cet email a déjà souscrit à notre newsletter  !")}
        </div>
      );
    };
    return (
      <div
      >
        <div  className="blocnewsletter" style={{ backgroundImage: "url(" + bgNewsletter + ")" }}>
          <div className="lpb-home-page-newsletter-container">
            <div className="lpb-home-page-newsletter-container-cover" />
            <div className="uk-text-center lpb-home-page-newsletter-content">
              <div>
              {this.state.isSuscribe === true ? (
            <p className="blocnewsletter-message">
                <img className="lpb-img-newsletter" src={logo} />
            <h3 className="lpb-congratulation">Merci !</h3>
            Votre inscription à notre newsletter a bien été prise en compte.
        </p>
        ) : (
                  <div>
                  <div className="lpb-home-page-newsletter-title">
                  Restez informé(e)
                </div>
                      <div className="lpb-home-page-newsletter-desc">
                        Abonnez-vous à notre newsletter <br />
                        pour être informé(e) de toutes nos activités.
                      </div>
                      <Form
                        className="blocnewsletter-myform"
                        layout="inline"
                        onFinish={this.submit1}
                      >
                        <Form.Item
                          name="EMAIL"
                          className="blocnewsletter-antInput"
                          rules={[
                            {
                              type: "email",
                              message:"Veuillez saisir votre adresse e-mai!",
                                
                            },
                            {
                              required: true,
                              message: "",
                            },
                          ]}
                        >
                          <div className="lpb-home-page-newsletter-from-wrap">
                            <Input
                              id="email"
                              value={this.state.email}
                              onChange={this.changeEmail}
                              placeholder="Saisissez votre adresse e-mail"
                              className="uk-input lpb-home-page-newsletter-from-field-input"
                            />
                            <Form.Item>
                              <Button type="submit" disabled={!this.state.btnDesable ? "disabled" : ""} id="SendButton" htmlType="submit" className="uk-button uk-button-default lpb-home-page-newsletter-from-submit lpb-traking-newsletter-btn">
                                Je m'abonne
                              </Button>
                            </Form.Item>
                          </div>
                        </Form.Item>
                      </Form>

                      {this.state.isLoading === false ? (
                        <div></div>
                      ) : (
                        <div class="text-center">
                          <div className="lpb-spinner" data-uk-spinner></div>

                        </div>
                      )}
                      <p style={{ "color": "red", "marginTop": "-8px" }}>{this.state.resultmessage}</p>
                    </div>
                  
                    )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

