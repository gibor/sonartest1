import React, { useState, useEffect} from "react";
import "./index.css";
import parse from "html-react-parser";
import { Link } from "react-router-dom";
import service from "../../service";
import config from "../../config";
import axios from "axios";

const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);
export default function Nurseries(props) {
  const [nurseries,setNurseries]=useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  useEffect(() => {
    fetchNurseriesData();
  }, []);

  const fetchNurseriesData = async () => {
    try {
      let response = await $service.getPages({
        url: "/nurseries"
      });
      if (response && response.length>0) {
        setNurseries(response[0].sections);
      }
    } catch (err) {
      setIsError(true);
    }
    setIsLoading(false);
  };
  return (
    
    <div className="lpb-component-nuseries-wrap">
       {isLoading && <div><div style={{flex: "initial",width: "auto"}} className="loader"><div data-uk-spinner /></div></div>}
       {
          (!isLoading && nurseries.length>0) &&
           
          <div className="uk-container lpb-component-nuseries-content-creche uk-text-center " >
            <h1 className="lpb-component-nuseries-title">{nurseries[1].title}</h1>
            <div className="lpb-component-nuseries-desc-container">
              <div className="lpb-component-nuseries-desc-1">
                {parse(nurseries[1].content)}
              </div>
            </div>
    
            <div className="uk-grid-small uk-child-width-expand@s uk-text-center lpb-component-nuseries-grandivcreche" data-uk-grid>
              {
              nurseries.map((el,i) =>  el.image? <Link key={i} to={el.url}>
                <div className="uk-card ">
                  <img src={`${config.backofficeURL}${el.image.url}`} width="100%" alt="image" />
                  <p className="lpb-component-nuseries-crechename">
                    {parse(el.content)}
                  </p>
                </div>
              </Link>:null
              )
            }  
            </div>

            <div tabindex="-1" className="uk-position-relative uk-visible-toggle uk-light lpb-component-nuseries-grandivcreche-mobile" data-uk-slider="autoplay: true">
              <ul className="uk-slider-items uk-grid">
                {nurseries.map((el,i) =>  el.image?<Link key={i} to={el.url}>
                  <div className="uk-card ">
                    <img src={`${config.backofficeURL}${el.image.url}`}  width="100%" alt="image" />
                    <p className="lpb-component-nuseries-crechename">
                      {parse(el.content)}
                    </p>
                  </div>
                </Link>:null
              )}  
              </ul>
            </div>

            <div className="lpb-component-nuseries-btn-registration-wrap">
              <a href={`/pré-inscription`}>
                <button style={{ marginLeft: "14px" }} className="uk-button uk-button-default lpb-component-nuseries-btn-registration">
                  Pré-Inscription
                </button>
              </a>
            </div>
          </div>
  
         }
       </div>
  )
}






