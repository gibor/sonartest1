import React from "react"
import "./index.css";

export default function Choice(props) {
    const addCrech = (crech) => {
        let data = { ...props.formChoice }

        // Choix Simple 
        data.crecheType = crech.id 

        /*
        if (crech.active) {
            // Choix Multiple 
            if (data.crecheType.includes(crech.id)) {
                data.crecheType.splice(data.crecheType.indexOf(crech.id), 1)
            } else {
                data.crecheType.push(crech.id)
            }
        }
        */
        
        props.formChoiceUpdate({ ...props.formChoice, crecheType: data.crecheType })
    }

    let listCreches
    if (props.formChoice.allCreches) {
        listCreches = props.formChoice.allCreches.map((crech, i) => {
            let r = <div className="uk-width-1-1@s uk-width-1-2@m" key={i.toString()}>
                <div
                    onClick={() => addCrech(crech)}
                    className={`uk-card uk-card-default uk-card-body lpb-register-creche-card-not-selected lpb-register-creche-card-content ${props.formChoice.crecheType.includes(crech.id) ? 'lpb-register-creche-card-selected' : ''}`}
                > 
                    <div>
                        {!crech.active && (
                            <div className="lpb-register-creche-card-inactive">Coming soon</div>
                        )}
                        <div className="uk-card-badge">
                            <div className="uk-text-center lpb-register-creche-card-pick">
                                <svg width={17} height={12} viewBox="0 0 17 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6.40625 11.75C6.71875 12.0625 7.25 12.0625 7.5625 11.75L16.75 2.5625C17.0625 2.25 17.0625 1.71875 16.75 1.40625L15.625 0.28125C15.3125 -0.03125 14.8125 -0.03125 14.5 0.28125L7 7.78125L3.46875 4.28125C3.15625 3.96875 2.65625 3.96875 2.34375 4.28125L1.21875 5.40625C0.90625 5.71875 0.90625 6.25 1.21875 6.5625L6.40625 11.75Z" />
                                </svg>
                            </div>
                        </div>
                        <div className="lpb-register-creche-card-container">
                            <div className="lpb-register-creche-card-title">
                                {crech.title}
                            </div>
                            {crech.adress && (
                            <div className="lpb-register-creche-card-infos-wrap">
                                <svg width={12} height={17} viewBox="0 0 12 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.375 15.6875C5.65625 16.125 6.3125 16.125 6.59375 15.6875C11.1562 9.125 12 8.4375 12 6C12 2.6875 9.3125 0 6 0C2.65625 0 0 2.6875 0 6C0 8.4375 0.8125 9.125 5.375 15.6875ZM6 8.5C4.59375 8.5 3.5 7.40625 3.5 6C3.5 4.625 4.59375 3.5 6 3.5C7.375 3.5 8.5 4.625 8.5 6C8.5 7.40625 7.375 8.5 6 8.5Z" fill="#A8B674" />
                                </svg>
                                <span className="lpb-register-creche-card-infos-text">
                                    {crech.adress}
                                </span>
                            </div>
                            )}
                            {crech.phone && (
                            <div className="lpb-register-creche-card-infos-wrap">
                                <svg width={16} height={16} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.5312 11.3125L12.0312 9.8125C11.9375 9.78125 11.8438 9.75 11.7188 9.75C11.5 9.75 11.2812 9.875 11.1562 10.0312L9.59375 11.9375C7.15625 10.7812 5.21875 8.84375 4.0625 6.40625L5.96875 4.84375C6.125 4.71875 6.25 4.5 6.25 4.25C6.25 4.15625 6.21875 4.0625 6.1875 3.96875L4.6875 0.46875C4.5625 0.1875 4.28125 0 3.96875 0C3.9375 0 3.875 0.03125 3.8125 0.03125L0.5625 0.78125C0.21875 0.875 0 1.15625 0 1.5C0 9.53125 6.46875 16 14.5 16C14.8438 16 15.125 15.7812 15.2188 15.4375L15.9688 12.1875C15.9688 12.125 15.9688 12.0625 15.9688 12.0312C15.9688 11.7188 15.7812 11.4375 15.5312 11.3125Z" fill="#A8B674" />
                                </svg>
                                <a href={`tel:${crech.number}`}>
                                    <span className="lpb-register-creche-card-infos-text">
                                        {crech.phone}
                                    </span>
                                </a>
                            </div>
                            )}
                            {crech.time && (
                            <div className="lpb-register-creche-card-infos-wrap">
                                <svg width={16} height={16} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 0.25C3.71875 0.25 0.25 3.71875 0.25 8C0.25 12.2812 3.71875 15.75 8 15.75C12.2812 15.75 15.75 12.2812 15.75 8C15.75 3.71875 12.2812 0.25 8 0.25ZM10.875 10.0312L10.25 10.8125C10.1562 10.9375 10.0312 11.0312 9.84375 11.0312C9.75 11.0312 9.625 10.9688 9.5625 10.9062L7.46875 9.34375C7.15625 9.125 7 8.78125 7 8.375V3.5C7 3.25 7.21875 3 7.5 3H8.5C8.75 3 9 3.25 9 3.5V8L10.8125 9.34375C10.9062 9.4375 11 9.5625 11 9.71875C11 9.84375 10.9375 9.96875 10.875 10.0312Z" fill="#A8B674" />
                                </svg>
                                <span className="lpb-register-creche-card-infos-text">
                                    {crech.time}
                                </span>
                            </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            return r
        })
    }

    return (
        <div>
            <div className="lpb-register-creche-card-wrap">
                <div data-uk-grid className="lpb-register-creche-card-box">
                    {listCreches}
                </div>
            </div>
        </div>
    )
}