import React, { useState } from "react"
import "./index.css";
import mixins from '../../../mixins'
import validator from 'validator'
import { postcodeValidator, postcodeValidatorExistsForCountry } from 'postcode-validator';

export default function Info(props) {
    const [localParents, setLocalParents] = useState([...props.formInfo.parents])
    const [haveMonoParent, setHaveMonoParent] = useState(false)

    const handleHaveMonoParent = (e, i) => {
        if (!haveMonoParent) {
            handlerForm({ ...props.formInfo, nbrParent: 1 }) // Monoparentale
        } else {
            handlerForm({ ...props.formInfo, nbrParent: 2 }) // 2 parents
        }

        setHaveMonoParent(!haveMonoParent)
    }

    const handleInputChangeInfo = (e, i, name) => {
        const value = e.target.value
        localParents[i][name] = value

        handlerForm({ ...props.formInfo, parents: localParents })
    }

    const forValidated = (e, i, name) => {
        let x = props.formInfo.parents

        const value = e.target.value
        x[i].errors[name] = value !== ""

        handlerForm({ ...props.formInfo, parents: x })
    }

    const forValidatedEmail = (e, i, name) => {
        let x = props.formInfo.parents

        const value = e.target.value
        x[i].errors[name] = validator.isEmail(value)

        handlerForm({ ...props.formInfo, parents: x })
    }

    const handlerForm = (x) => {
        let data = { ...x }

        let newFormValid = []
        data.parents.forEach(el => {
            let item = { ...el }

            if (mixins.infosFieldProvide(item)) {
                item.isValid = true
            } else {
                item.isValid = false
            }
            newFormValid.push(item)
        })

        data = { ...data, parents: newFormValid }

        if (data.nbrParent !== data.parents.length) {
            if (data.nbrParent > data.parents.length) {
                let res = data.nbrParent - data.parents.length

                let n = 0;

                while (n < res) {
                    let y = {
                        isValid: false,
                        civility: "",
                        lastname: "",
                        firstname: "",
                        adress: "",
                        codepostal: "",
                        city: "",
                        country: "",
                        phone: "",
                        email: "",
                    }
                    data.parents.push(y)
                    n++;
                }
            }
            if (data.nbrParent < data.parents.length) {
                let res = data.parents.length - data.nbrParent
                let j = 0;

                while (j < res) {
                    data.parents.splice(-1, 1)
                    j++;
                }
            }
        }

        props.formInfoUpdate(data)
    }

    let listParents
    if (props.formInfo.parents) {
        listParents = props.formInfo.parents.map((currentParent, i) => {
            let r = <div className="lpb-register-child-container" key={i.toString()}>
                <div>
                    <div data-uk-grid>
                        <div className="lpb-register-child-title uk-width-1-2">
                            Parent
                        </div>
                    </div>
                    <div className="lpb-pb-20">
                        <hr className="lpb-register-child-divider" />
                    </div>
                    <div className="lpb-register-form-field-wrap" data-uk-grid>
                        <div className="uk-width-1-1@s uk-width-1-2@m">
                            <label className="lpb-register-form-field-label">Civilité *</label>
                            <select name={'civility' + i} onChange={(e) => handleInputChangeInfo(e, i, 'civility')} value={currentParent.civility} className="uk-select lpb-register-form-field-select">
                                <option value="Monsieur">Monsieur</option>
                                <option value="Madame">Madame</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Nom *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Nom *" name={'lastname' + i} onKeyUp={(e) => forValidated(e, i, 'lastname')} onChange={(e) => handleInputChangeInfo(e, i, 'lastname')} value={currentParent.lastname} />
                                {!currentParent.errors.lastname && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Prénom(s) *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Prénom(s) *" name={'firstname' + i} onKeyUp={(e) => forValidated(e, i, 'firstname')} onChange={(e) => handleInputChangeInfo(e, i, 'firstname')} value={currentParent.firstname} />
                                {!currentParent.errors.firstname && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Adresse *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Adresse" name={'adress' + i} onKeyUp={(e) => forValidated(e, i, 'adress')} onChange={(e) => handleInputChangeInfo(e, i, 'adress')} value={currentParent.adress} />
                                {!currentParent.errors.adress && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <div>
                                    <label className="lpb-register-form-field-label">Code postal *</label>
                                    <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Code postal" name={'codepostal' + i} onKeyUp={(e) => forValidated(e, i, 'codepostal')} onChange={(e) => handleInputChangeInfo(e, i, 'codepostal')} value={currentParent.codepostal} />
                                    {!currentParent.errors.codepostal && (
                                        <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                            Veuillez remplir ce champ
                                        </div>
                                    )}
                                </div>
                                {(currentParent.codepostal && !postcodeValidator(currentParent.codepostal, 'FR') && !postcodeValidator(currentParent.codepostal, 'LU')) && (
                                    <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                        Veuillez entrer un code postal correspondant au format demandé.
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Ville *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Ville" name={'city' + i} onKeyUp={(e) => forValidated(e, i, 'city')} onChange={(e) => handleInputChangeInfo(e, i, 'city')} value={currentParent.city} />
                                {!currentParent.errors.city && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Pays *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Pays" name={'country' + i} onKeyUp={(e) => forValidated(e, i, 'country')} onChange={(e) => handleInputChangeInfo(e, i, 'country')} value={currentParent.country} />
                                {!currentParent.errors.country && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <div>
                                    <div>
                                    <label className="lpb-register-form-field-label">Numéro de téléphone *</label>
                                        <input className="uk-input lpb-register-form-field-input"
                                            name="phone"
                                            value={currentParent.phone}
                                            onChange={(e) => handleInputChangeInfo(e, i, 'phone')}
                                            onKeyUp={(e) => forValidated(e, i, 'phone')}
                                            type="number" placeholder="Entrez votre numéro de téléphone" />
                                        {!currentParent.errors.phone && (
                                            <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                                Veuillez remplir ce champ
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Adresse e-mail *</label>
                                <input className="uk-input lpb-register-form-field-input" type="email" placeholder="Adresse e-mail" name={'email' + i} onKeyUp={(e) => forValidatedEmail(e, i, 'email')} onChange={(e) => handleInputChangeInfo(e, i, 'email')} value={currentParent.email} />
                                {!currentParent.errors.email && (
                                    <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                        Veuillez saisir un e-mail correct
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="lpb-register-fields-input-required-all">
                    * Les champs marqués d'un astérisque sont obligatoires.
                </div>
            </div>

            return r
        })
    }

    return (
        <div>
            {/*<div className="uk-text-center lpb-register-choice-children-wrap">
                <div className="lpb-register-choice-children-title">
                    Veuillez renseigner vos informations personnelles.
                </div>
                
                <div> 
                    <div>
                        <label className="lpb-register-checkbox-child-label">
                            <input checked={haveMonoParent} onChange={(e) => handleHaveMonoParent(e)} className="uk-checkbox lpb-register-btn-checkbox-child" type="checkbox" />
                            Famille monoparentale
                        </label>
                    </div>
                </div>
                
            </div>*/}
            {props.formInfo.nbrParent && (
                <div>
                    {listParents}
                </div>
            )}
        </div>
    )
}