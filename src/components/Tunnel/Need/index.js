import React, { useState } from "react"
import "./index.css";
import moment from 'moment'
import mixins from '../../../mixins'

export default function Need(props) {
    const [minDateCrecheday, setMinDateCrecheday] = useState(moment().format('YYYY-MM-DD'))
    const [localChildren, setLocalChildren] = useState([...props.formNeed.children])

    const handleCheckedChild = (event) => {
        const target = event.target;
        const value = target.value;

        handlerForm({ ...props.formNeed, nbrChildren: value })
    }

    const handleNotChildFirstnameNeed = (e, i) => {
        let x = props.formNeed.children 

        x[i].notChildFirstname = !x[i].notChildFirstname
        x[i].firstname = ''

        if(x[i].notChildFirstname) {
            x[i].errors.firstname = true
        }

        handlerForm({ ...props.formNeed, children: x })
    }

    const handleInputChangeNeed = (e, i, name) => {
        let x = props.formNeed.children 

        const value = e.target.value
        x[i][name] = value

        handlerForm({ ...props.formNeed, children: x })
    }

    const forValidated = (e, i, name) => {
        let x = props.formNeed.children

        const value = e.target.value
        x[i].errors[name] = value !== ""

        handlerForm({ ...props.formNeed, children: x })
    }

    const handlerForm = (x) => {
        let data = { ...x } 

        let newFormValid = []
        data.children.forEach(el => {
            let item = { ...el }

            if (mixins.needFieldProvide(item)) {
                item.isValid = true
            } else {
                item.isValid = false
            }
            newFormValid.push(item)
        })

        data = { ...data, children: newFormValid } 

        if (data.nbrChildren !== data.children.length) {
          if (data.nbrChildren > data.children.length) {
            let res = data.nbrChildren - data.children.length

            let n = 0;

            while (n < res) {
              let y = {
                isValid: false,
                notChildFirstname: false,
                lastname: "",
                firstname: "",
                birthday: "",
                crecheday: "",
                errors: {
                    lastname: true,
                    firstname: true,
                    birthday: true,
                    crecheday: true,        
                },
              }
              data.children.push(y)
              n++;
            }
          }
          if (data.nbrChildren < data.children.length) {
            let res = data.children.length - data.nbrChildren
            let j = 0;

            while (j < res) {
              data.children.splice(-1, 1)
              j++;
            }
          }
        }

        props.formNeedUpdate(data)
    }

    let listChilds
    if (props.formNeed.children) {
        listChilds = props.formNeed.children.map((currentChild, i) => {
            let r = <div className="lpb-register-child-container" key={i.toString()}>
                <div>
                    <div data-uk-grid>
                        <div className="lpb-register-child-title uk-width-1-3">Enfant {i + 1}</div>
                        <div className="uk-text-right uk-width-expand">
                            <label className="lpb-register-checkbox-child-label lpb-register-child-label-web">
                                <input checked={currentChild.notChildFirstname} onChange={(e) => handleNotChildFirstnameNeed(e, i)} className="uk-checkbox lpb-register-btn-checkbox-child" type="checkbox" />
                                Je ne connais pas encore le prénom de mon enfant
                            </label>
                        </div>
                    </div>
                    <div className="lpb-pb-20">
                        <hr className="lpb-register-child-divider" />
                    </div>
                    <div>
                        <label className="lpb-register-checkbox-child-label lpb-register-child-label-mobile">
                            <input checked={currentChild.notChildFirstname} onChange={(e) => handleNotChildFirstnameNeed(e, i)} className="uk-checkbox lpb-register-btn-checkbox-child" type="checkbox" />
                            Je ne connais pas encore le prénom de mon enfant
                    </label>
                    </div>
                    <div className="lpb-register-form-field-wrap" data-uk-grid>
                        <div className="uk-width-1-1@s uk-width-1-2@m">
                            <label className="lpb-register-form-field-label">Nom *</label>
                            <input className="uk-input lpb-register-form-field-input" onKeyUp={(e) => forValidated(e, i, 'lastname')} onChange={(e) => handleInputChangeNeed(e, i, 'lastname')} type="text" placeholder="Nom *" name={'lastname' + i} value={currentChild.lastname} />
                            {!currentChild.errors.lastname && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m">
                            <label className="lpb-register-form-field-label">Prénom(s) *</label>
                            <input className="uk-input lpb-register-form-field-input" onKeyUp={(e) => forValidated(e, i, 'firstname')} onChange={(e) => handleInputChangeNeed(e, i, 'firstname')} type="text" disabled={currentChild.notChildFirstname} placeholder="Prénom(s) *" name={'firstname' + i} value={currentChild.firstname} />
                            {!currentChild.errors.firstname && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="lpb-register-form-field-wrap" data-uk-grid>
                        <div className="uk-width-1-1@s uk-width-1-2@m">
                            <label className="lpb-register-form-field-label">Date de naissance *</label>
                            <input className="uk-input lpb-register-form-field-input" type={currentChild.birthday ? 'date' : 'text'} onFocus={(e) => e.target.type = 'date'} placeholder="Date de naissance *" onBlur={(e) => forValidated(e, i, 'birthday')} onKeyUp={(e) => forValidated(e, i, 'birthday')} onChange={(e) => handleInputChangeNeed(e, i, 'birthday')} name={'birthday' + i} value={currentChild.birthday} />
                            {!currentChild.errors.birthday && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m">
                            <label className="lpb-register-form-field-label">Date d'entrée à la crèche/foyer *</label>
                            <input className="uk-input lpb-register-form-field-input" type={currentChild.crecheday ? 'date' : 'text'} onFocus={(e) => e.target.type = 'date'} placeholder="Date d'entrée à la crèche/foyer *"  onBlur={(e) => forValidated(e, i, 'crecheday')} onKeyUp={(e) => forValidated(e, i, 'crecheday')} onChange={(e) => handleInputChangeNeed(e, i, 'crecheday')} min={minDateCrecheday} name={'crecheday' + i} value={currentChild.crecheday} />
                            {!currentChild.errors.crecheday && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                            {currentChild.crecheday && (
                                <span>
                                    {mixins.checkCrecheDay(currentChild.birthday, currentChild.crecheday) < 60 && (
                                        <span className="lpb-form-erros-msg">
                                            La date d’inscription d’un enfant en crèche doit être postérieure d’au moins 60 jours à sa date de naissance
                                        </span>
                                    )}
                                </span>
                            )}
                        </div>
                    </div>
                </div>
                <div className="lpb-register-fields-input-required-all">
                    * Les champs marqués d'un astérisque sont obligatoires.
                </div>
            </div>
            return r
        })
    }

    return (
        <div>
            <div>
                <div className="uk-text-center lpb-register-choice-children-wrap">
                    <div className="lpb-register-choice-children-title">
                        Veuillez choisir le nombre d’enfant(s)  que vous voulez inscrire
                    </div>
                    <div>
                        <div className="lpb-register-choice-head-box">
                            <div className="lpb-register-radio-form-mobile-label-first">
                                <label className="lpb-register-radio-form-label">
                                    <input onChange={handleCheckedChild} checked={parseInt(props.formNeed.nbrChildren, 10) === 1} name="number-children" value={1} className="uk-radio lpb-register-btn-radio-form" type="radio" />
                                01 enfant
                                </label>
                            </div>
                            <div>
                                <label className="lpb-register-radio-form-label">
                                    <input onChange={handleCheckedChild} checked={parseInt(props.formNeed.nbrChildren, 10) === 2} name="number-children" value={2} className="uk-radio lpb-register-btn-radio-form" type="radio" />
                                    02 enfants
                                </label>
                            </div>
                            <div>
                                <label className="lpb-register-radio-form-label">
                                    <input onChange={handleCheckedChild} checked={parseInt(props.formNeed.nbrChildren, 10) === 3} name="number-children" value={3} className="uk-radio lpb-register-btn-radio-form" type="radio" />
                                    03 enfants
                                </label>
                            </div>
                            <div>
                                <label className="lpb-register-radio-form-label">
                                    <input onChange={handleCheckedChild} checked={parseInt(props.formNeed.nbrChildren, 10) === 4} name="number-children" value={4} className="uk-radio lpb-register-btn-radio-form" type="radio" />
                                     04 enfants
                                    </label>
                            </div>
                            <div>
                                <label className="lpb-register-radio-form-label">
                                    <input onChange={handleCheckedChild} checked={parseInt(props.formNeed.nbrChildren, 10) === 5} name="number-children" value={5} className="uk-radio lpb-register-btn-radio-form" type="radio" />
                                    05 enfants
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                {props.formNeed.nbrChildren && (
                    <div>
                        {listChilds}
                    </div>
                )}
            </div>
        </div>
    )
}