import React, { useState } from "react"
import "./index.css";
import moment from 'moment'
import mixins from '../../../mixins'
import validator from 'validator'
import { postcodeValidator, postcodeValidatorExistsForCountry } from 'postcode-validator';

export default function Validation(props) {
    const [minDateCrecheday, setMinDateCrecheday] = useState(moment().format('YYYY-MM-DD'))
    const [localChildren, setLocalChildren] = useState([...props.formNeed.children])
    const [localParents, setLocalParents] = useState([...props.formInfo.parents])

    const handleCheckValidationBox = (e, name) => {
        let x
        if (name == "validCGU") {
            x = { ...props.formValidation, validCGU: !props.formValidation.validCGU }
        }
        if (name == "validNewsletter") {
            x = { ...props.formValidation, validNewsletter: !props.formValidation.validNewsletter }
        }
        props.formValidationUpdate(x)
    }

    // Need 
    const handleInputChangeNeed = (e, i, name) => {
        const value = e.target.value
        localChildren[i][name] = value

        handlerFormNeed({ ...props.formNeed, children: localChildren })
    }

    const forValidatedNeed = (e, i, name) => {
        let x = props.formNeed.children

        const value = e.target.value
        x[i].errors[name] = value !== ""

        handlerFormNeed({ ...props.formNeed, children: x })
    }

    const handlerFormNeed = (x) => {
        let data = { ...x }

        let newFormValid = []
        data.children.forEach(el => {
            let item = { ...el }

            if (mixins.needFieldProvide(item)) {
                item.isValid = true
            } else {
                item.isValid = false
            }
            newFormValid.push(item)
        })

        data = { ...data, children: newFormValid }

        if (data.nbrChildren !== data.children.length) {
            if (data.nbrChildren > data.children.length) {
                let res = data.nbrChildren - data.children.length

                let n = 0;

                while (n < res) {
                    let y = {
                        isValid: false,
                        notChildFirstname: false,
                        lastname: "",
                        firstname: "",
                        birthday: "",
                        crecheday: "",
                    }
                    data.children.push(y)
                    n++;
                }
            }
            if (data.nbrChildren < data.children.length) {
                let res = data.children.length - data.nbrChildren
                let j = 0;

                while (j < res) {
                    data.children.splice(-1, 1)
                    j++;
                }
            }
        }

        props.formNeedUpdate(data)
    }

    let listChilds
    if (props.formNeed.children) {
        listChilds = props.formNeed.children.map((currentChild, i) => {
            let r = <div className="lpb-register-validation-form-wrap" key={i.toString()}>
                <div>
                    <div className="lpb-register-validation-form-title">
                        Enfant {i + 1}
                    </div>
                    <div className="lpb-register-form-field-wrap" data-uk-grid>
                        <div className="uk-width-1-1@s uk-width-1-4@m">
                            <label className="lpb-register-form-field-label">Nom *</label>
                            <input className="uk-input lpb-register-form-field-input" onKeyUp={(e) => forValidatedNeed(e, i, 'lastname')} onChange={(e) => handleInputChangeNeed(e, i, 'lastname')} type="text" placeholder="Nom *" name={'lastname' + i} value={currentChild.lastname} />
                            {!currentChild.errors.lastname && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-4@m">
                            <label className="lpb-register-form-field-label">Prénom(s) *</label>
                            <input className="uk-input lpb-register-form-field-input" onKeyUp={(e) => forValidatedNeed(e, i, 'firstname')} onChange={(e) => handleInputChangeNeed(e, i, 'firstname')} type="text" disabled={currentChild.notChildFirstname} placeholder="Prénom(s) *" name={'firstname' + i} value={currentChild.firstname} />
                            {!currentChild.errors.firstname && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-4@m">
                            <label className="lpb-register-form-field-label">Date de naissance *</label>
                            <input className="uk-input lpb-register-form-field-input" type={currentChild.birthday ? 'date' : 'text'} onFocus={(e) => e.target.type = 'date'} placeholder="Date de naissance *" onBlur={(e) => forValidatedNeed(e, i, 'birthday')}  onKeyUp={(e) => forValidatedNeed(e, i, 'birthday')} onChange={(e) => handleInputChangeNeed(e, i, 'birthday')} name={'birthday' + i} value={currentChild.birthday} />
                            {!currentChild.errors.birthday && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-4@m">
                            <label className="lpb-register-form-field-label">Date d'entrée à la crèche/foyer *</label>
                            <input className="uk-input lpb-register-form-field-input" type={currentChild.crecheday ? 'date' : 'text'} onFocus={(e) => e.target.type = 'date'} placeholder="Date d'entrée à la crèche/foyer *" onBlur={(e) => forValidatedNeed(e, i, 'crecheday')} onKeyUp={(e) => forValidatedNeed(e, i, 'crecheday')} onChange={(e) => handleInputChangeNeed(e, i, 'crecheday')} min={minDateCrecheday} name={'crecheday' + i} value={currentChild.crecheday} />
                            {!currentChild.errors.crecheday && (
                                <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                    Veuillez remplir ce champ
                                </div>
                            )}
                            {currentChild.crecheday && (
                                <span>
                                    {mixins.checkCrecheDay(currentChild.birthday, currentChild.crecheday) < 60 && (
                                        <span className="lpb-form-erros-msg">
                                            La date d’inscription d’un enfant en crèche doit être postérieure d’au moins 60 jours à sa date de naissance
                                        </span>
                                    )}
                                </span>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            return r
        })
    }
    // Need

    // Info

    const handleInputChangeInfo = (e, i, name) => {
        const value = e.target.value
        localParents[i][name] = value

        handlerFormInfo({ ...props.formInfo, parents: localParents })
    }

    const forValidatedInfo = (e, i, name) => {
        let x = props.formInfo.parents

        const value = e.target.value
        x[i].errors[name] = value !== ""

        handlerFormInfo({ ...props.formInfo, parents: x })
    }

    const forValidatedEmailInfo = (e, i, name) => {
        let x = props.formInfo.parents

        const value = e.target.value
        x[i].errors[name] = validator.isEmail(value)

        handlerFormInfo({ ...props.formInfo, parents: x })
    }

    const handlerFormInfo = (x) => {
        let data = { ...x }

        let newFormValid = []
        data.parents.forEach(el => {
            let item = { ...el }

            if (mixins.infosFieldProvide(item)) {
                item.isValid = true
            } else {
                item.isValid = false
            }
            newFormValid.push(item)
        })

        data = { ...data, parents: newFormValid }

        if (data.nbrParent !== data.parents.length) {
            if (data.nbrParent > data.parents.length) {
                let res = data.nbrParent - data.parents.length

                let n = 0;

                while (n < res) {
                    let y = {
                        isValid: false,
                        civility: "",
                        lastname: "",
                        firstname: "",
                        adress: "",
                        codepostal: "",
                        city: "",
                        country: "",
                        phone: "",
                        email: "",
                    }
                    data.parents.push(y)
                    n++;
                }
            }
            if (data.nbrParent < data.parents.length) {
                let res = data.parents.length - data.nbrParent
                let j = 0;

                while (j < res) {
                    data.parents.splice(-1, 1)
                    j++;
                }
            }
        }

        props.formInfoUpdate(data)
    }

    let listParents
    if (props.formInfo.parents) {
        listParents = props.formInfo.parents.map((currentParent, i) => {
            let r = <div className="lpb-register-validation-form-wrap" key={i.toString()}>
                <div>
                    <div className="lpb-register-validation-form-title">
                        Parent
                    </div>
                    <div className="lpb-register-form-field-wrap" data-uk-grid>
                        <div className="uk-width-1-1@s uk-width-1-2@m">
                            <label className="lpb-register-form-field-label">Civilité *</label>
                            <select name={'civility' + i} onChange={(e) => handleInputChangeInfo(e, i, 'civility')} value={currentParent.civility} className="uk-select lpb-register-form-field-select">
                                <option value="Monsieur">Monsieur</option>
                                <option value="Madame">Madame</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Nom *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Nom *" name={'lastname' + i} onKeyUp={(e) => forValidatedInfo(e, i, 'lastname')} onChange={(e) => handleInputChangeInfo(e, i, 'lastname')} value={currentParent.lastname} />
                                {!currentParent.errors.lastname && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Prénom(s) *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Prénom(s) *" name={'firstname' + i} onKeyUp={(e) => forValidatedInfo(e, i, 'firstname')} onChange={(e) => handleInputChangeInfo(e, i, 'firstname')} value={currentParent.firstname} />
                                {!currentParent.errors.firstname && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Adresse *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Adresse *" name={'adress' + i} onKeyUp={(e) => forValidatedInfo(e, i, 'adress')} onChange={(e) => handleInputChangeInfo(e, i, 'adress')} value={currentParent.adress} />
                                {!currentParent.errors.adress && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <div>
                                    <label className="lpb-register-form-field-label">Code postal *</label>
                                    <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Code postal *" name={'codepostal' + i} onKeyUp={(e) => forValidatedInfo(e, i, 'codepostal')} onChange={(e) => handleInputChangeInfo(e, i, 'codepostal')} value={currentParent.codepostal} />
                                    {!currentParent.errors.codepostal && (
                                        <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                            Veuillez remplir ce champ
                                        </div>
                                    )}
                                </div>
                                {(currentParent.codepostal && !postcodeValidator(currentParent.codepostal, 'FR') && !postcodeValidator(currentParent.codepostal, 'LU')) && (
                                    <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                    Veuillez entrer un code postal correspondant au format demandé.
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Ville *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Ville *" name={'city' + i} onKeyUp={(e) => forValidatedInfo(e, i, 'city')} onChange={(e) => handleInputChangeInfo(e, i, 'city')} value={currentParent.city} />
                                {!currentParent.errors.city && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Pays *</label>
                                <input className="uk-input lpb-register-form-field-input" type="text" placeholder="Pays *" name={'country' + i} onKeyUp={(e) => forValidatedInfo(e, i, 'country')} onChange={(e) => handleInputChangeInfo(e, i, 'country')} value={currentParent.country} />
                                {!currentParent.errors.country && (
                                    <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                        Veuillez remplir ce champ
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="lpb-register-form-field-wrap" data-uk-grid>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <div>
                                    <label className="lpb-register-form-field-label">Numéro de téléphone *</label>
                                    <input className="uk-input lpb-register-form-field-input"
                                        name={'phone' + i}
                                        value={currentParent.phone}
                                        onChange={(e) => handleInputChangeInfo(e, i, 'phone')}
                                        onKeyUp={(e) => forValidatedInfo(e, i, 'phone')}
                                        type="number" placeholder="Entrez votre numéro de téléphone" />
                                    {!currentParent.errors.phone && (
                                        <div style={{ marginTop: '3px' }} className="lpb-form-erros-msg">
                                            Veuillez remplir ce champ
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="uk-width-1-1@s uk-width-1-2@m">
                                <label className="lpb-register-form-field-label">Adresse e-mail *</label>
                                <input className="uk-input lpb-register-form-field-input" type="email" placeholder="Adresse e-mail *" name={'email' + i} onKeyUp={(e) => forValidatedEmailInfo(e, i, 'email')} onChange={(e) => handleInputChangeInfo(e, i, 'email')} value={currentParent.email} />
                                {!currentParent.errors.email && (
                                    <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                        Veuillez saisir un e-mail correct
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            return r
        })
    }

    // Info

    // Creche et Foyer 

    let crecheCheck = []
    if (props.formChoice.crecheType) {
        props.formChoice.allCreches.map((item) => {
            if (props.formChoice.crecheType.includes(item.id)) {
                crecheCheck.push(item)
            }
        })
    }

    let listCreches
    if (crecheCheck) {
        listCreches = crecheCheck.map((crech, i) => {
            let r = <div className="lpb-register-validation-creche-box" key={i.toString()}>
                <div className="lpb-register-validation-creche-box-title">
                    {crech.title}
                </div>
                <div className="lpb-register-validation-creche-box-divider">
                    <hr className="uk-divider-vertical" />
                </div>
                <div className="lpb-register-validation-creche-box-infos">
                    {crech.adress && (
                        <div className="lpb-register-validation-creche-box-infos-flex">
                            <div style={{ width: "10%" }}>
                                <svg width={12} height={17} viewBox="0 0 12 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.375 15.6875C5.65625 16.125 6.3125 16.125 6.59375 15.6875C11.1562 9.125 12 8.4375 12 6C12 2.6875 9.3125 0 6 0C2.65625 0 0 2.6875 0 6C0 8.4375 0.8125 9.125 5.375 15.6875ZM6 8.5C4.59375 8.5 3.5 7.40625 3.5 6C3.5 4.625 4.59375 3.5 6 3.5C7.375 3.5 8.5 4.625 8.5 6C8.5 7.40625 7.375 8.5 6 8.5Z" fill="#A8B674" />
                                </svg>
                            </div>
                            <div className="lpb-register-validation-creche-box-infos-details">
                                {crech.adress}                     
                            </div>
                        </div>
                    )}
                    {crech.phone && (
                        <div className="lpb-register-validation-creche-box-infos-flex">
                            <div style={{ width: "10%" }}>
                                <svg width={16} height={16} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.5312 11.3125L12.0312 9.8125C11.9375 9.78125 11.8125 9.75 11.7188 9.75C11.5312 9.75 11.2812 9.875 11.1562 10.0312L9.59375 11.9375C7.53125 10.9375 5.03125 8.46875 4.0625 6.40625L5.96875 4.84375C6.125 4.71875 6.25 4.46875 6.25 4.25C6.25 4.1875 6.21875 4.0625 6.1875 3.96875L4.6875 0.46875C4.5625 0.21875 4.25 0.03125 3.96875 0.03125C3.9375 0.03125 3.875 0.03125 3.8125 0.03125L0.5625 0.78125C0.25 0.84375 0 1.1875 0 1.5C0 9.53125 6.46875 16 14.5 16C14.8125 16 15.1562 15.75 15.2188 15.4375L15.9688 12.1875C15.9688 12.125 15.9688 12.0625 15.9688 12.0312C15.9688 11.75 15.7812 11.4375 15.5312 11.3125Z" fill="#A8B674" />
                                </svg>
                            </div>
                            <a href={`tel:${crech.number}`} style={{ color: "#565656" }}>
                                <div className="lpb-register-validation-creche-box-infos-details">
                                    {crech.phone}   
                                </div>
                            </a>
                        </div>
                    )}
                    {crech.time && (
                        <div className="lpb-register-validation-creche-box-infos-flex">
                            <div style={{ width: "10%" }}>
                                <svg width={16} height={16} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 0.25C3.71875 0.25 0.25 3.71875 0.25 8C0.25 12.2812 3.71875 15.75 8 15.75C12.2812 15.75 15.75 12.2812 15.75 8C15.75 3.71875 12.2812 0.25 8 0.25ZM9.78125 11.2188L7 9.1875C6.90625 9.125 6.875 9.03125 6.875 8.90625V3.625C6.875 3.4375 7.03125 3.25 7.25 3.25H8.75C8.9375 3.25 9.125 3.4375 9.125 3.625V7.9375L11.0938 9.375C11.25 9.5 11.3125 9.75 11.1875 9.90625L10.2812 11.125C10.1875 11.2812 9.9375 11.3125 9.78125 11.2188Z" fill="#A8B674" />
                                </svg>
                            </div>
                            <div className="lpb-register-validation-creche-box-infos-details">
                                {crech.time}
                            </div>
                        </div>
                    )}
                </div>
            </div>
            return r
        })
    }

    // Creche et Foyer 

    return (
        <div>
            <div className="lpb-register-validation-container">
                <div>
                    <ul data-uk-accordion>
                        <li class="uk-open">
                            <a className="uk-accordion-title lpb-register-validation-accordion-title" href="#">Votre/Vos enfant(s)</a>
                            <div className="uk-accordion-content lpb-register-validation-accordion-content-wrap">
                                {listChilds}
                            </div>
                        </li>
                        <li class="uk-open">
                            <a className="uk-accordion-title lpb-register-validation-accordion-title" href="#">Informations personnelles</a>
                            <div className="uk-accordion-content lpb-register-validation-accordion-content-wrap">
                                {listParents}
                            </div>
                        </li>
                        <li class="uk-open">
                            <a className="uk-accordion-title lpb-register-validation-accordion-title" href="#"
                            >Choix de la structure</a>
                            <div className="uk-accordion-content lpb-register-validation-accordion-content-wrap">
                                {listCreches}
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="lpb-register-validation-confirm-content">
                    <div className="lpb-register-validation-checkbox-container">
                        <div>
                            <input className="uk-checkbox lpb-register-btn-checkbox-child" type="checkbox" checked={props.formValidation.validCGU} onChange={(e) => handleCheckValidationBox(e, 'validCGU')} />
                        </div>
                        <div>
                            <label className="lpb-register-validation-checkbox-label">
                                J’ai pris connaissance et accepte les CGU et la politique de protection des données de Les P'tits Bouchons
                            </label>
                        </div>
                    </div>
                    <div style={{ paddingBottom: '0px' }} className="lpb-register-validation-checkbox-container">
                        <div>
                            <input className="uk-checkbox lpb-register-btn-checkbox-child" type="checkbox" checked={props.formValidation.validNewsletter} onChange={(e) => handleCheckValidationBox(e, 'validNewsletter')} />
                        </div>
                        <div>
                            <label className="lpb-register-validation-checkbox-label">
                                J’accepte de recevoir les communications de Aura groupe
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="lpb-register-validation-checkbox-notice">Cette pré-inscription n’a pas de valeur d’engagement. Ce formulaire est une demande de place en crèche/foyer de jour, toutes les données resteront confidentielles.</div>
                    <div className="lpb-register-validation-checkbox-notice">Nos structures sont agréées et prestataires des Chèques-Services Accueil.</div>
                </div>
            </div>

        </div>
    )
}