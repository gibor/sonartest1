import React, { Component } from "react";

import "./index.css";
import parse from "html-react-parser";

export default function composantactalite(props) {
  return (
    <div className="pdleft">
      <div className="uk-card uk-card-default uk-card-body lpb-suitecategoriecolor">
        {" "}
        <div className="">

          <div className="" />
          <img src={props.ImageBloc} alt="philosophy" className="" />
        </div>
        <div className="">
          <div className="">
            <div className="lpb-title-suite-blog">
              {parse(props.titrepremierblog)}
            </div>
            <div className="lpb-home-page-actuality-post-desc-suite">
              {parse(props.texteblog)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
