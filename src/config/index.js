export default {
  // tokenBackoffice:
  //   'eyJhbGciOiJIUzI1NiIsInRs5cCI6IkpXVCJ9.eyJpZCI6IjYwMjE2MWYyZjQ4NmJjMDAyMzMxNTIxZSIsImlhdCI6MTYxMzEyNDU1NSwiZXhwIjoxNjE1NzE2NTU1fQ.tXMXq0ChbABzFLt9sD08cPAlwBzrWRZ0DLksNLoQ2ko',
  backofficeURL: 'https://lpb-backoffice.luxdatadigit.lu',
  frontUrl: 'https://www.lesptitsbouchons.lu', //"https://lbpv2-dev.luxdatadigit.lu", //"http://localhost:3000",
  serverURL: 'https://api-lbpv2-staging.luxdatadigit.lu', //'api-lbpv2-dev.luxdatadigit.lu', // staging = https://api-lbpv2-staging.luxdatadigit.lu
  NairgymLink: 'https://www.nairgym.lu', // "https://nairgymv2staging.luxdatadigit.lu"
};
