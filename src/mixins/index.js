import moment from "../helpers/moment";
import validator from 'validator'
import { postcodeValidator, postcodeValidatorExistsForCountry } from 'postcode-validator';

export default {
  getAge(birthday) {
    if (birthday == "" || undefined) {
      return 0
    } else {
      let age = moment().diff(birthday, 'years')
      return age;
    }
  },

  checkCrecheDay(birthday, crecheday) {
    let A = moment(new Date(birthday))
    let B = moment(new Date(crecheday))
    return B.diff(A, 'days')
  },

  needFieldProvide(form) {
    return (
      form.lastname &&
      ((!form.notChildFirstname && form.firstname !== "") || (form.notChildFirstname && form.firstname === "")) &&
      form.birthday &&
      this.checkCrecheDay(form.birthday, form.crecheday) >= 60
    )
  },

  infosFieldProvide(form) {
    return (
      form.civility &&
      form.lastname &&
      form.firstname &&
      form.adress &&
      (postcodeValidator(form.codepostal, 'FR') || postcodeValidator(form.codepostal, 'LU')) &&
      form.city &&
      form.country &&
      form.phone &&
      validator.isEmail(form.email)
    )
  },
}
