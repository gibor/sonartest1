import React from "react";
import { 
    BrowserRouter as Router,
    Route,
    Switch,
} from "react-router-dom";

import NotFound from "../views/NotFound";
// Pages
import Home from "../views/Home";
import About from "../views/About";
import Tunnel from "../views/Tunnel";
import Contact from "../views/Contact";
import Foetz from "../views/Foetz";
import Leudelange from "../views/Leudelange";
import EschSurAlzette from "../views/EschSurAlzette";
import Schifflange from "../views/Schifflange";
import Actualites from "../views/Actualites";
import ReadActuality from "../views/Actualites/read";
import Partenaires from "../views/Partenaires";
import Mentions from "../views/Mentions";
import Pedagogie from "../views/Pedagogie";
import Engagement from "../views/Engagement";
import Blog from "../views/Blog";   
import Politique from "../views/Politique";
import OursSections from "../views/OursSections";
import Nairgym from "../views/Nairgym";
import ReadArticle from "../views/Blog/read";


 

const index = () => {
    return (
        <div>
            <Router>
                <Switch>
                    <Route exact path="/" render={(props) => <Home {...props} />} />
                    <Route exact path="/pré-inscription" render={(props) => <Tunnel {...props} />} />
                    <Route exact path="/contact" render={(props) => <Contact {...props} />} />
                    <Route exact path="/a-propos-de-nous" render={(props) => <About {...props} />} />
                    <Route exact path="/nos-sections" render={(props) => <OursSections {...props} />} />
                    <Route exact path="/foetz-crèche-et-foyer-de-jour" render={(props) => <Foetz {...props} />} /> 
                    <Route exact path="/leudelange-crèche" render={(props) => <Leudelange {...props} />} />
                    <Route exact path="/esch-sur-alzette-foyer" render={(props) => <EschSurAlzette {...props} />} />
                    <Route exact path="/schifflange-crèche" render={(props) => <Schifflange {...props} />} />
                    <Route exact path="/actualites" render={(props) => <Actualites {...props} />} />
                    <Route path="/actualites/details/:slug" render={(props) => <ReadActuality {...props} />} />
                    <Route path="/posts/:id" render={(props) => <ReadArticle {...props} />} />
                    <Route exact path="/partenaires" render={(props) => <Partenaires {...props} />} />
                    <Route exact path="/notre-pédagogie" render={(props) => <Pedagogie {...props} />} />
                    <Route exact path="/nos-engagements" render={(props) => <Engagement {...props} />} />
                    <Route exact path="/mentions-legales" render={(props) => <Mentions {...props} />} />
                    <Route exact path="/notre-pédagogie" render={(props) => <Pedagogie {...props} />} />
                    <Route exact path="/nos-engagements" render={(props) => <Engagement {...props} />} />
                    <Route exact path="/politique" render={(props) => <Politique {...props} />} />
                    <Route exact path="/blog" render={(props) => <Blog {...props} />} />
                    <Route exact path="/nairgym" render={(props) => <Nairgym {...props} />} />        
                    <Route path="**" exact>
                        <NotFound to="/not-found" />
                    </Route>
                </Switch>
            </Router>
        </div>
    )
}

export default index;

