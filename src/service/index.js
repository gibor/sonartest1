/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { AUTH_TOKEN_KEY } from '../constants';

export default ($http, $config, $baseURL, $fromToken) => {
  const $api = $http.create({
    baseURL: $baseURL,
    headers: { 'Content-Type': 'application/json' },
  });

  $api.interceptors.request.use(
    (config) => {
      let authToken;

      if ($fromToken) {
        authToken = $fromToken;

        //config.headers["Access-Token"] = sessionStorage.getItem(AUTH_TOKEN_KEY);
      } else {
        authToken = sessionStorage.getItem(AUTH_TOKEN_KEY);
        //config.headers["Access-Token"] = sessionStorage.getItem(AUTH_TOKEN_KEY);
      }

      config.headers['Authorization'] = `Bearer ${authToken}`;
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  /**
   * Send mail contact
   * @param {*} data
   */
  const sendsendMailContact = (data) => {
    return $api.post('/api/contact', data).then((res) => res.data);
  };

  /**
   * Register
   * @param {*} data
   */
  const preRegistration = (data) => {
    return $api.post('/api/preinscription', data).then((res) => res.data);
  };

  /**
   * allActualities
   * @param {*} data
   */
  const getAllActualities = (params) => {
    return $api
      .get('/actualites', {
        params,
      })
      .then((res) => res.data);
  };

  /**
   * @param{*}
   * Item Actuality
   */

  const getActuality = (params) => {
    return $api
      .get('/actualites', {
        params,
      })
      .then((res) => res.data);
  };

  const saveContactOffice = (data) => {
    return $api.post('/contacts', data).then((res) => res.data);
  };

  const savePreRegisterOffice = (data) => {
    return $api.post('/preinscriptions', data).then((res) => res.data);
  };

  const getArticles = (params) => {
    return $api
      .get('/articles', {
        params,
      })
      .then((res) => res.data);
  };

  const getCategories = (params) => {
    return $api
      .get('/categories', {
        params,
      })
      .then((res) => res.data);
  };
  const getPages = (params) => {
    return $api
      .get('/pages', {
        // params,
      })
      .then((res) => res.data);
  };
  const getPage = (params) => {
    return $api
      .get('/pages/602d4fca201585002639bfa1', {
        params,
      })
      .then((res) => res.data);
  };

  const getApropos = (params) => {
    return getPages({
      name: 'Apropos',
      ...params,
    });
  };
  const getSections = (params) => {
    return getPages({
      name: 'NosSections',
      ...params,
    });
  };
  const getPedagogie = (params) => {
    return getPages({
      name: 'NotrePedagogie',
      ...params,
    });
  };
  return {
    sendsendMailContact,
    preRegistration,
    getAllActualities,
    getActuality,
    getCategories,
    getArticles,
    getPage,
    getApropos,
    getSections,
    getPedagogie,
    getPages,
    saveContactOffice,
    savePreRegisterOffice,
  };
};
