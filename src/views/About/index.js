import React, { useState, useEffect, Component } from "react";
import "./index.css";
import MetaTags from 'react-meta-tags';
import service from "../../service";
import parse from "html-react-parser";
import axios from "axios";
import config from '../../config'
import logoptitsbouchons from "../../assets/img/logoptitsbouchons.png";
import aura from "../../assets/img/aura.svg";
import nairgymlogo from "../../assets/img/nairgymlogo.png";
import auraacademy from "../../assets/img/auraacademy.png";
import chartediversite from "../../assets/img/chartediversite.png";
import labelactions from "../../assets/img/labelactions.png";
import prixqualite from "../../assets/img/prixqualite.png";
import bg from "../../assets/img/bg.png";
import check from "../../assets/img/check.png";
import check1 from "../../assets/img/check1.png";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);

export default function Apropos() {


  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [aproposdata, setAproposdata] = useState([]);


  useEffect(() => {
    getAproposdata();
  }, []);

  const getAproposdata = async () => {
    let response = await $service.getApropos({});
    if (response) {
      setAproposdata(response[0].sections);
    }

    setIsLoading(false);
  };
  return (
    <div>
      <Header />
      <MetaTags>
        <title>A propos de nous - Les P’tits Bouchons</title>
        <meta name="description" content="Implantés aux Luxembourg depuis septembre 2012, nos crèches-foyers sont des acteurs forts du secteur économique du sud de Luxembourg." />
        <meta property="og:title" content="A propos de nous - Les P’tits Bouchons" />
      </MetaTags>
      <div
        className="lpb-background"
        id="apropos"
        style={{
          backgroundImage: "url(" + bg + ")",
        }}
      >
        <div className=" lpb-blocAbout ">
          <div className="uk-container" id="blocpetitsbouchons">
            <div class="uk-child-width-expand@s lpb-colorbloc" data-uk-grid>
              <div class="uk-grid-item-match lpb-bloclpbleft">

                <div class="uk-card uk-card-default uk-card-body lpb-content">
                  {aproposdata && aproposdata.length > 0 && <p className="lpb-textlpb">
                    {parse(aproposdata[0].content)}

                  </p>}
                  <p className="lpb-textgreenlpb">
                    Les P’tits Bouchons est membre de Aura groupe
                    </p>
                </div>
              </div>
              <div className="lpb-bloclpbright">
                <div className="lpb-blocimglpb">
                  <img
                    className="lpb-logoptitsbouchons"
                    src={logoptitsbouchons}

                  />
                </div>

              </div>
            </div >{" "}

          </div>

          <div className="uk-container lpb-mtbloc2" id="blocauragroupe" >
            <div class="uk-child-width-expand@s lpb-colorbloc" data-uk-grid>
              <div class="uk-grid-item-match lpb-bloclogoaura">
                <img className="lpb-logoauragroup" src={aura} />
              </div>
              <div className="lpb-bloctexteaura">
                <div className="lpb-blocimgaura">
                  <p className="lpb-titleauragroup">Aura groupe</p>
                  {aproposdata && aproposdata.length > 0 && <p className="lpb-titlesecondauragroup">
                    {aproposdata[1].title}
                  </p>}
                  {aproposdata && aproposdata.length > 0 && <p className="lpb-textauragroup">
                    {parse(aproposdata[1].content)}
                  </p>}








                  <button
                    className="lpb-auragroupbutton"
                    type="button"
                    disabled

                  >
                    Découvrez Aura groupe
                    </button>
                </div>
              </div>
            </div>
          </div>
          <div className="uk-container lpb-mtbloc2 " id="blocauraacademy">
            <div
              class="uk-child-width-expand@s uk-text-center lpb-c"
              data-uk-grid
            >
              <div className="lpb-b" id="blocnairgym">
                <div class="uk-card uk-card-default uk-card-body lpb-contoutr-bloc2">
                  <div class="uk-grid-item-match lpb-colorblocnairgym">
                    <div class="uk-card uk-card-default uk-card-body lpb-contentnairgym">
                      <img className="lpb-logoNairgym" src={nairgymlogo} />
                      {aproposdata && aproposdata.length > 0 && <p className="lpb-textnairgym">
                        {parse(aproposdata[2].content)}
                      </p>}






                      <div className="lpb-nairgymbutton lpb-onlybuttonnairgym">
                        <a
                          href={`${config.NairgymLink}`} target="_BLANK"
                          className="lpb-lienboutton lpb-traking-nairgym-about-btn"
                        >
                          Découvrez N’AIRGYM
                          </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="lpb-bloccontentauraacademyresponsive" >
                <div class="uk-card uk-card-default uk-card-body lpb-contoutr-bloc2">
                  <div className="lpb-colorblocnairgym">
                    <div className="lpb-contentacademy">
                      <div

                        className="lpb-logoacademy"
                        id=""
                        style={{
                          backgroundImage: "url(" + auraacademy + ")",
                        }}
                      ></div>
                      {aproposdata && aproposdata.length > 0 &&
                        <p className="lpb-textauraacademy">
                          {parse(aproposdata[3].content)}
                        </p>
                      }
                      <div className="lpb-auraacademybutton">
                        <a href="" className="lienaura">
                          Découvrez Auracademy
                          </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <h3 className="lpb-titlelabel uk-text-center">Nos labels</h3>
          <div class="lpb-blocimage">
            <div className="">
              <div class="uk-card uk-card-default uk-card-body">
                <img className="lpb-logobebe" src={chartediversite} />
              </div>
            </div>
            <div className="">
              <div class="uk-card uk-card-default uk-card-body">
                <img className="lpb-logobebe" src={labelactions} />
              </div>
            </div>
            <div className="">
              <div class="uk-card uk-card-default uk-card-body">
                <img className="lpb-logobebe" src={prixqualite} />
              </div>
            </div>
            <div></div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
}
