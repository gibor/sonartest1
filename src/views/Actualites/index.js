import React, { Component } from 'react';
import './index.css';
import MetaTags from 'react-meta-tags';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import ActualityBox from '../../components/ActualityBox';
import Footer from '../../components/Footer';
import bg from '../../assets/img/bg.png';
import slideactualite from '../../assets/img/slide-actualite.jpg';
import service from '../../service';
import config from '../../config';
import axios from 'axios';

const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);

export default class index extends Component {
  render() {
    return (
      <div>
        <Header />
        <MetaTags>
          <title>Actualités - Les P’tits Bouchons</title>
          <meta
            name="description"
            content="Quand sont prévues les prochaines ouvertures ? Quels sont les projets en cours ? Ne manquez rien de l'actualité des crèches et foyers de jour Les P’tits Bouchons"
          />
          <meta
            property="og:title"
            content="Actualités - Les P’tits Bouchons"
          />
        </MetaTags>
        <Banner
          backgroundImage={slideactualite}
          position="center"
          title="Suivez nos actualités"
          desc=""
        />

        <div
          className="lpb-backgroundfoyer"
          id="apropos"
          style={{
            backgroundImage: 'url(' + bg + ')',
          }}
        >
          <div
            className="uk-container "
            className="lpb-space-component-actualite"
          >
            <div className="titreActu">Dernières actualités</div>

            <ActualityBox isOthers={false} />
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}
