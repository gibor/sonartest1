import React, { useState, useEffect } from "react";
import "./read.css";
import moment from "../../helpers/moment";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { BrowserRouter as Router, useParams } from "react-router-dom";
import bg from "../../assets/img/bg.png";
import ActualityBox from "../../components/ActualityBox";
import ReactHtmlParser from "html-react-parser";
import service from "../../service";
import config from "../../config";
import axios from "axios";
const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);

export default function ReadActuality(props) {
  let { slug } = useParams();
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [byActuality, setAyActuality] = useState({});

  useEffect(() => {
    window.scrollTo(0,0);
    fetchActualityData();
  }, [slug]);

  const fetchActualityData = async () => {
    setIsLoading(true);
    setIsError(false);

    try {
      let res = await $service.getActuality({
        id: slug,
      });

      if (res) {
        
        setAyActuality(res[0]);
      }

      setIsLoading(false);
    } catch (err) {
      
      setIsError(true);
      setIsLoading(false);
    }
  };

  return (
    <div>
      <Header />
      <div
        style={{
          backgroundImage: "url(" + bg + ")",
        }}
      >
        <div>
          <div>
            <div>
              <div className="lpb-backgroundfoyer" id="apropos">
                <div className="uk-container">
                  {isLoading && (
                    <div className="lpb-read-actuality-spinner">
                      <div data-uk-spinner />
                    </div>
                  )}
                  {byActuality.id && (
                    <div
                      className="uk-grid-match uk-child-width-expand@s lpb-blocactalite"
                      data-uk-grid
                    >
                      <div className="uk-card uk-card-default uk-card-body lpb-paddingcardactu">
                        <div className="">
                          <img
                            src={`${config.backofficeURL}${byActuality.cover.url}`}
                            alt="Cover"
                          />
                        </div>
                        <div className="">
                          <div className="lpb-home-page-actuality-post-box-suite">
                            <div className="lpb-home-page-actuality-post-date-suite">
                              {moment(
                                new Date(byActuality.published_at)
                              ).format("D MMMM YYYY")}
                            </div>
                            <div className="lpb-home-page-actuality-post-title-suite">
                              {byActuality.title}
                            </div>

                            <div className="lpb-home-page-actuality-post-desc-suite">
                              {ReactHtmlParser(`${byActuality.content}`)}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <div className="uk-container  LPB-padding">
            <div className="titreActuautres">Autres actualités</div>

            <div>
              <ActualityBox exclus={slug} limit={4} isOthers={false} />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
