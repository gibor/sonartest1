import React, { useState, useEffect } from 'react';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import DernierArticle from '../../components/DernierArticle';
import Footer from '../../components/Footer';
import bg from '../../assets/img/bg.png';
import Bannerblog from '../../assets/img/blog.png';
import './index.css';
import MetaTags from 'react-meta-tags';
import BlogCaterogy from '../../components/BlogCaterogy';
import _filter from 'lodash.filter';
import service from '../../service';
import config from '../../config';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { ellipsis } from 'ellipsed';
const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);
export default function Blog(props) {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [blog, setBlog] = useState({
    categories: [],
    allPost: [],
    filtered: [],
    search: '',
  });
  const [activeCategory, setActiveCategory] = useState(null);
  useEffect(() => {
    getBlogData();
  }, []);
  useEffect(() => {
    ellipsis('.post-desc', 4);
  });
  const filter = (search) => {
    let filtered = [];
    if (search !== '') {
      filtered = blog.allPost.filter((post) => {
        const postText =
          `${post.title} ${post.description} ${post.content}`.toLowerCase();
        return postText.includes(search.toLowerCase());
      });
    }
    setBlog({
      ...blog,
      filtered: filtered,
      search: search,
    });
  };

  const getBlogData = async () => {
    try {
      let response = await $service.getCategories({});
      if (response) {
        let allPost = [];
        response.forEach((post) => (allPost = [...allPost, ...post.blogs]));
        setBlog({ ...blog, categories: response, allPost: allPost });
      }
    } catch (err) {
      setIsError(true);
    }
    setIsLoading(false);
  };

  const { categories, filtered, search } = blog;
  const articles =
    search !== ''
      ? filtered
      : activeCategory
      ? activeCategory.blogs
      : categories[0] && categories[0].blogs;

  return (
    <div>
      <Header />
      <MetaTags>
        <title>Blog - Les P’tits Bouchons</title>
        <meta name="description" content="" />
        <meta property="og:title" content="Blog - Les P’tits Bouchons" />
      </MetaTags>
      <Banner
        backgroundImage={Bannerblog}
        position="center"
        title="Blog"
        desc=""
      />

      <div
        className="lpb-backgroundfoyer"
        id="apropos"
        style={{
          backgroundImage: 'url(' + bg + ')',
        }}
      >
        <div className="uk-container">
          {isLoading && (
            <div className="loader">
              <div data-uk-spinner />
            </div>
          )}
          {!isLoading && categories.length > 0 ? (
            <div className="" data-uk-grid>
              <div className="uk-width-1-3@m  lpb-blogespace">
                <BlogCaterogy
                  categories={categories}
                  filter={filter}
                  activeCategory={activeCategory}
                  onCategoryChange={setActiveCategory}
                />
                <div className="  ">
                  <DernierArticle />
                </div>
              </div>
              <div className="uk-width-expand@m lpb-margin-mobile-blog">
                <div className="uk-card uk-card-default">
                  <div
                    className="uk-child-width-1-2@s uk-child-width-1-2@m uk-margin-bottom"
                    data-uk-grid
                  >
                    {articles && articles.length > 0 ? (
                      articles.map((article, key) => (
                        <div className="uk-card uk-card-default lpb-paddingblogcard">
                          {' '}
                          <div className="lpb-foyer">
                            <div
                              className="blogCover"
                              style={{
                                backgroundImage: `url(${config.backofficeURL}${article.cover.url})`,
                              }}
                            ></div>
                          </div>
                          <div className="">
                            <div className="lpb-actuality-post-blog">
                              <div className="lpb-home-page-actuality-post-title-blog post-title">
                                {article.title}
                              </div>
                              <div className="lpb-home-page-actuality-post-desc-blog ">
                                <p className="post-desc">
                                  {article.description}
                                </p>
                                <Link
                                  className="lpb-color-lire-suite"
                                  to={`/posts/${article.id}`}
                                >
                                  Lire la suite
                                </Link>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    ) : (
                      <p
                        style={{
                          margin: '50px auto',
                          textAlign: 'center',
                          fontWeight: 'bolder',
                        }}
                      >
                        Aucun article trouvé
                      </p>
                    )}
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>

      <Footer />
    </div>
  );
}
