import React, { useState,useEffect } from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import bg from "../../assets/img/bg.png";
import DernierArticle from "../../components/DernierArticle";
import Bloclecture from "../../components/bloclecture";
import { useParams } from "react-router-dom";
import service from "../../service";
import config from "../../config";
import axios from "axios";
const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);
const ReadArticle=(props)=>{ 
  let { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [article, setArticle]=useState(null);
  useEffect(() => {
    window.scrollTo(0,0);
    getArticle();
  }, [id]);

  const  getArticle = async () => {
    try {
      let response = await $service.getArticles({
        id:id
      });
      if (response && response.length>0) {
        setArticle(response[0]);
      }
    } catch (err) {
      setIsError(true);
    }
    setIsLoading(false);
  };
    return (
      <div>
        <Header />

        <div
          className="lpb-backgroundfoyer"
          id="apropos"
          style={{
            backgroundImage: "url(" + bg + ")",
          }}
        >
          <div className="uk-container">
            <div
              className="uk-grid-match uk-child-width-expand@s lpb-margin-suite-blog"
              data-uk-grid
            >
              {isLoading && <div><div style={{flex: "initial",width: "auto"}} className="loader"><div data-uk-spinner /></div></div>}
             {(!isLoading && article)?
              <Bloclecture 
                ImageBloc={`${config.backofficeURL}${article.cover.url}`}
                titrepremierblog={article.title}
                texteblog={article.content}/>
                :null
              }

              <div className="uk-width-1-3@m  lpb-catesuiteblog">
                <DernierArticle exclus={id} />
              </div>
            </div>
          </div>
        </div>

        <Footer />
      </div>
    )
  
}

export default ReadArticle;