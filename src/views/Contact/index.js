import React, { useRef, useState, useEffect } from 'react';
import './index.css';
import MetaTags from 'react-meta-tags';
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Banner from '../../components/Banner'
import Thanks from '../../components/Thanks'
import BoxInfos from '../../components/BoxInfos'
import BannerImage from "../../assets/img/Group188.png";
import PlaneBg from "../../assets/img/bg.png";
import validator from 'validator'

import service from '../../service'
import config from '../../config'
import axios from 'axios'
const $service = service(axios, config, config.serverURL)

const $serviceBackoffice = service(
    axios,
    config,
    config.backofficeURL,
    config.tokenBackoffice
)

export default function Contact() {
    const firstRender = useRef(true)

    const [isLoading, setIsLoading] = useState(false)
    const [success, setSuccess] = useState(false)
    const [error, setError] = useState({ isError: false, msg: "" })
    const [nurseries, setNurseries] = useState([])
    const [validationForm, setValidationForm] = useState(false)

    const [phone, setPhone] = useState(false)
    const initFormData={
        structure: "Crèche de Foetz",
        civility: "Monsieur",
        firstname: "",
        lastname: "",
        email: "",
        phone: "",
        message: "",
        accetContract: false,
    };
    const [formData, setFormData] = useState(initFormData);

    const [formDataError, setFormDataError] = useState({
        structure: true,
        civility: true,
        firstname: true,
        lastname: true,
        email: true,
        phone: true,
        message: true,
    })

    useEffect(() => {
        if (firstRender.current) {
            fetchNurseriesData()
            validatedForm()
            firstRender.current = false
            return
        }
        validatedForm()
    }, [formData])

    const fetchNurseriesData = async () => {
        // Replace by API Fetch
        const data = [
            {
                title: "Crèche & foyer de jour de Foetz",
                address: "3, rue des Artisans L-3895 Foetz",
                phone: "(352) 26 55 02 98 (Accueil)",
                number: "+35226550298",
                time: "Lun. au ven. 05h -22h <br /> Sam. 07h - 19h",
            },
            {
                title: "Crèche de Leudelange",
                address: "5 Rue Léon Laval,  L-3372 Leudelange",
                phone: "(352) 621 22 87 95",
                number: "+352621228795",
                time: "Lun. au ven. 05h -22h",
            },
            {
                title: "Foyer de jour Esch-sur-Alzette",
                address: "31 rue Louis Pasteur L-4276 <br /> Esch-sur-Alzette",
                phone: "(352) 26 55 02 98 (Sécrétariat)",
                number: "+35226550298",
                time: "",
            },
            {
                title: "Crèche de Schifflange",
                address: "Centre commercial Op Herbert <br /> Z.A Op Herbert L-3862 Schifflange",
                phone: "(352) 26 55 02 98 (Sécrétariat)",
                number: "+35226550298",
                time: "",
            },
        ]
        setNurseries(data)
    }

    const sendContactForm = async (data) => {
        setIsLoading(true)
        try {
            await $service.sendsendMailContact(data)
            await $serviceBackoffice.saveContactOffice({
                lastname: data.name,
                firstname: data.surname,
                phone: data.phone,
                email: data.email,
            }) // save to backoffice
            setSuccess(true)

            setFormData(initFormData);
            setIsLoading(false)
        } catch (error) {

            setError({
                isError: true,
                msg: '',
            })
            setSuccess(false)
            setIsLoading(false)
        }
    }

    const listNurseries = nurseries.map((el, i) => {
        let r = <BoxInfos
            key={i.toString()}
            title={el.title}
            address={el.address}
            phone={el.phone}
            number={el.number}
            time={el.time}
        />
        return r
    })

    const handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        let data = { ...formData, [name]: value }
        setFormData(data)
    }

    const forValidated = (name, value) => {
        let data = { ...formDataError, [name]: value !== "" }
        setFormDataError(data)
    }

    const forValidatedEmail = (name, value) => {
        let x = validator.isEmail(value)
        let data = { ...formDataError, [name]: x }
        setFormDataError(data)
    }

    const toggleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        let data = { ...formData, [name]: !formData.accetContract }
        setFormData(data)
    }

    const validatedForm = () => {
        let validationForm = (
            formData.structure.length !== 0 &&
            formData.civility.length !== 0 &&
            formData.firstname.length !== 0 &&
            formData.lastname.length !== 0 &&
            (formData.email.length !== 0 && validator.isEmail(formData.email)) &&
            formData.phone.length !== 0 &&
            formData.message.length !== 0 &&
            formData.accetContract
        )
        setValidationForm(!validationForm)
    }

    const handleSubmitForm = e => {
        if ( window.fbq){
            window.fbq('track', 'Contact');
          }
        e.preventDefault()
        setSuccess(false)
        setError({
            isError: false,
            msg: '',
        })

        let x = {
            structure: formData.structure.trim(),
            name: formData.lastname.trim(),
            surname: formData.firstname.trim(),
            phone: formData.phone.trim(),
            email: formData.email.trim(),
            gender: formData.civility.trim(),
            message: formData.message.trim(),
            condition: formData.accetContract,
        }

        sendContactForm(x)
    }

    const onClosedPopup = (isClosed) => {
        setSuccess(false)
    }

    return (
        <div>
            <Header />
            <MetaTags>
                <title>Contact - Les P’tits Bouchons</title>
                <meta name="description" content="Contactez-nous via notre site web pour vos différentes préocuppations ou pour en savoir plus sur nos crèches et foyer de jour." />
                <meta property="og:title" content="Contact - Les P’tits Bouchons" />
            </MetaTags>
            <Banner
                backgroundImage={BannerImage}
                position="center"
                title="Contactez-nous"
            />
            <div style={{ backgroundImage: `url(${PlaneBg})` }}>
                <div className="uk-container lpb-contact-form-wrap">
                    <div className="uk-grid-divider lpb-contact-content" data-uk-grid>
                        <div className="uk-width-1-1@s uk-width-1-3@m lpb-pl-0">
                            <div className="uk-text-center lpb-contact-box-infos-title-mobile">Informations</div>
                            {listNurseries}
                        </div>
                        <div className="uk-width-expand lpb-contact-form-container-box">
                            <form
                                method="post"
                                action="#"
                                onSubmit={handleSubmitForm}>
                                <div className="">
                                    <div className="lpb-contact-form-container" data-uk-grid>
                                        <div className="uk-width-1-1@s uk-width-1-2@m">
                                            <div className="lpb-form-simple-field-label">Sélectionnez la structure<span className="lpb-form-simple-legend-color">*</span></div>
                                            <select
                                                name="structure"
                                                value={formData.structure}
                                                onChange={handleInputChange}
                                                className="uk-select lpb-form-simple-field-select">
                                                {/*<option value="" disabled>Sélectionnez la structure</option>*/}
                                                <option value="Crèche de Foetz">Crèche de Foetz</option>
                                                <option value="Foyer de jour de Foetz">Foyer de jour de Foetz</option>
                                                <option value="Crèche de Leudelange">Crèche de Leudelange</option>
                                                <option value="Foyer de Esch-sur-Alzette">Foyer de Esch-sur-Alzette</option>
                                                <option value="Crèche de Schifflange">Crèche de Schifflange</option>
                                            </select>
                                        </div>
                                        <div className="uk-width-1-1@s uk-width-1-2@m">
                                            <div className="lpb-form-simple-field-label">Civilité<span className="lpb-form-simple-legend-color">*</span></div>
                                            <select
                                                name="civility"
                                                value={formData.civility}
                                                onChange={handleInputChange}
                                                className="uk-select lpb-form-simple-field-select">
                                                {/*<option value="" disabled>Civilité</option>*/}
                                                <option value="Monsieur">Monsieur</option>
                                                <option value="Madame">Madame</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="lpb-contact-form-container" data-uk-grid>
                                        <div className="uk-width-1-1@s uk-width-1-2@m">
                                            <div>
                                                <div className="lpb-form-simple-field-label">Nom<span className="lpb-form-simple-legend-color">*</span></div>
                                                <input className="uk-input lpb-form-simple-field-input"
                                                    name="lastname"
                                                    type="text" placeholder="Entrez votre nom"
                                                    value={formData.lastname}
                                                    onChange={handleInputChange}
                                                    onKeyUp={(e) => forValidated('lastname', formData.lastname)} />
                                            </div>
                                            {!formDataError.lastname && (
                                                <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                                    Veuillez remplir ce champ
                                                </div>
                                            )}
                                        </div>
                                        <div className="uk-width-1-1@s uk-width-1-2@m">
                                            <div>
                                                <div className="lpb-form-simple-field-label">Prénom(s)<span className="lpb-form-simple-legend-color">*</span></div>
                                                <input className="uk-input lpb-form-simple-field-input"
                                                    name="firstname"
                                                    value={formData.firstname}
                                                    onChange={handleInputChange}
                                                    onKeyUp={(e) => forValidated('firstname', formData.firstname)}
                                                    type="text" placeholder="Entrez votre prénom" />
                                            </div>
                                            {!formDataError.firstname && (
                                                <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                                    Veuillez remplir ce champ
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <div className="lpb-contact-form-container" data-uk-grid>
                                        <div className="uk-width-1-1@s uk-width-1-2@m">
                                            <div>
                                                <div className="lpb-form-simple-field-label">Adresse e-mail<span className="lpb-form-simple-legend-color">*</span></div>
                                                <input className="uk-input lpb-form-simple-field-input"
                                                    name="email"
                                                    value={formData.email}
                                                    onChange={handleInputChange}
                                                    onKeyUp={(e) => forValidatedEmail('email', formData.email)}
                                                    type="email" placeholder="Entrez votre adresse e-mail" />
                                            </div>
                                            {!formDataError.email && (
                                                <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                                    Veuillez saisir un e-mail correct
                                                </div>
                                            )}
                                        </div>
                                        <div className="uk-width-1-1@s uk-width-1-2@m">
                                            <div>
                                                <div className="lpb-form-simple-field-label">Téléphone<span className="lpb-form-simple-legend-color">*</span></div>
                                                <input className="uk-input lpb-form-simple-field-input"
                                                    name="phone"
                                                    value={formData.phone}
                                                    onChange={handleInputChange}
                                                    onKeyUp={(e) => forValidated('phone', formData.phone)}
                                                    type="number" placeholder="Entrez votre numéro de téléphone" />
                                            </div>
                                            {!formDataError.phone && (
                                                <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                                    Veuillez remplir ce champ
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <div className="lpb-contact-form-container" data-uk-grid>
                                        <div className="uk-width-1-1@s uk-width-1-1@m">
                                            <div>
                                                <div className="lpb-form-simple-field-label">Message<span className="lpb-form-simple-legend-color">*</span></div>
                                                <textarea className="uk-textarea lpb-form-simple-field-textarea"
                                                    name="message"
                                                    value={formData.message}
                                                    onKeyUp={(e) => forValidated('message', formData.message)}
                                                    onChange={handleInputChange} rows="5" placeholder="Saisissez votre message" />
                                            </div>
                                            {!formDataError.message && (
                                                <div style={{ marginTop: '3px', marginLeft: '10px' }} className="lpb-form-erros-msg">
                                                    Veuillez remplir ce champ
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <span className="lpb-form-simple-legend-color">*</span>
                                    <span className="lpb-form-simple-legend">
                                        Les champs marqués d'un astérisque sont obligatoires.
                                    </span>
                                    <div>
                                        <div className="lpb-form-simple-footer-checkbox-container">
                                            <div>
                                                <input className="uk-checkbox lpb-form-simple-btn-checkbox-child"
                                                    name="accetContract"
                                                    checked={formData.accetContract}
                                                    onChange={toggleInputChange} type="checkbox" />
                                            </div>
                                            <div>
                                                <label className="lpb-form-simple-footer-checkbox-label">
                                                    J’ai lu et j’accepte  <a target="_blank" href={`/politique`}><span>la politique en matière de respect de la vie privée</span></a>*
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="lpb-mt-30">
                                        <button type="submit" disabled={validationForm} className="uk-button uk-button-default lpb-header-btn-full-gray lpb-traking-contact-btn">
                                            Envoyer
                                        </button>
                                        {isLoading && (
                                            <div data-uk-spinner />
                                        )}
                                        {error.isError && (
                                            <div className="lpb-form-erros-send">
                                                Une erreur s'est produite lors de la soumission
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {success && (
                <Thanks byClosePopup={onClosedPopup} social="Suivez-nous" message="Votre message a été envoyé avec succès. Nous vous répondrons dans les meilleurs délais." />
            )}
            <Footer />
        </div>
    );
}

