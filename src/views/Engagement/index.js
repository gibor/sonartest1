import React, { useState, useEffect } from 'react'
import "./index.css";
import MetaTags from 'react-meta-tags';
import coverVideo from "../../assets/img/video.mp4";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

import imgengagementbouchons from "../../assets/img/imgengagementbouchons.png";
import bg from "../../assets/img/bg.png";
import video_mobile from "../../assets/img/video_mobile.gif";
import damn from "../../assets/img/damn.gif";


export default function Engagement() {
    const [nurseries, setNurseries] = useState({});

    return (
        <div>

            <Header />
            <MetaTags>
                <title>Nos engagements - Les P’tits Bouchons</title>
                <meta name="description" content="Nous souhaitons permettre aux parents de trouver en nous un solide appui, des professionnels à leur écoute, qui soutiennent au mieux leur parentalité." />
                <meta property="og:title" content="Nos engagements - Les P’tits Bouchons" />
            </MetaTags>
            <div
                className="lpb-backgroundfoyer"
                id="apropos"
                style={{
                    backgroundImage: "url(" + bg + ")",
                }}
            >

                <div className="lpb-home-pageslider-video-container">
                    <div className="uk-cover-container">

                        <video
                            src={coverVideo}
                            controls
                            controlsList="nodownload"
                            className="lpb-ours-section-pageslider-video-full"
                            playsInline
                            autoPlay
                            loop
                            muted
                            type="video/mp4"
                        />
                    </div>
                    <div className="uk-text-center lpb-home-pageslider-video-wrap-sections">
                        <div className="lpb-home-pageslider-video-title-sections">
                            Nos engagements
          </div>
                    </div>
                </div>

                <div className="uk-container lpb-bloctexteengagement">
                    <p className="uk-text-center lpb-titleengagement">Nous souhaitons permettre aux parents de trouver en nous un solide appui, des professionnels à leur<br /> écoute, qui soutiennent au mieux leur parentalité. Nous avons pleinement conscience que
                         le bien-être des <br />enfants ne peut-être total sans la pleine confiance que les parents accordent à l’entièreté de notre équipe.  <br /></p>
                </div>
                <div className="lpb-twoblocengagement">
                    <div className="lpb-blocescalier uk-text-center">
                        <div className="uk-text-center lpb-blocengagementbouchons" data-uk-grid>
                            <div>
                                <div class="uk-inline uk-dark">
                                    <div class="">
                                        <img src={imgengagementbouchons} alt="" className="lpb-imgbouchonsengagement uk-text-center" />
                                        <div class="uk-position-absolute uk-transform-center lpb-markengagement lpb-essai"
                                            style={{ "left": "42%", "top": "14%" }}
                                            data-uk-marker>

                                            <div className="uk-container lpb-blochoverbouchon uk-text-left lpb-blochoverbouchon-responsive">
                                                <h3 className="uk-text-left lpb-titlehoverbouchon">Phase d'adaptation</h3>
                                                <p className="uk-text-left lpb-textehoverbouchon">Cette phase de découverte est essentielle et sa durée est déterminée en fonctiondes
                                                besoins de chaque enfant, chaque famille.Durant ces jours d’acclimatation l’enfant, ses parents et l’équipe éducative apprennent à se
                                                connaitre. Assurée par un référent unique,
                                    cette période permet à l’enfant de s’habituer progressivement à son nouvel environnement qu’est la crèche.</p>
                                                <a className="lien" href="/posts/60261ebdede2a800236cc28a">
                                                    <button className="uk-text-left lpb-buttonhoverbouchon">En savoir plus</button>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="uk-position-absolute uk-transform-center lpb-markengagement lpb-markengagement1"
                                            style={{ "left": "66%", "top": "25%" }}
                                            href="" data-uk-marker>
                                            <div className="uk-container lpb-blochoverbouchon  uk-text-left lpb-blochoverbouchon-responsive1">
                                                <h3 className="uk-text-left lpb-titlehoverbouchon">Accueil de l’enfant</h3>
                                                <p className="uk-text-left lpb-textehoverbouchon">Le développement d’une relation de confiance mutuelle entre les parents,
                                                l’équipe éducative et l’enfant, permet la tenue d’échanges pertinents chaque matin, lors de l’accueil. Ces échanges sont ce que
                                                l’on appelle «les transmissions».
                                        Chacun a son rôle à y jouer et les besoins de l’enfant explicités permettent un ajustement de la prise en charge. </p>
                                                <a className="lien" href="/posts/60261f34ede2a800236cc28d">
                                                    <button className="uk-text-left lpb-buttonhoverbouchon">En savoir plus</button>
                                                </a>
                                            </div>

                                        </div>

                                        <div class="uk-position-absolute uk-transform-center lpb-markengagement"
                                            style={{ "left": "43%", "top": "38%" }}
                                            href="" data-uk-marker>
                                            <div className="uk-container lpb-blochoverbouchon  uk-text-left lpb-blochoverbouchon-responsive">
                                                <h3 className="uk-text-left lpb-titlehoverbouchon">La sécurité affective de l’enfant</h3>
                                                <p className="uk-text-left lpb-textehoverbouchon">Cette question revêt un double aspect: celui de la sécurité physique dont le dispositif,
                                                régulièrement évalué, inclut une composante technologique constamment active,et celui de la sécurité affective,
                                            essentielle à la constructionde la personnalité de l’enfant et relatif au lien d’attachement.</p>
                                                <a className="lien" href="/posts/60261fabede2a800236cc290">
                                                    <button className="uk-text-left lpb-buttonhoverbouchon">En savoir plus</button>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="uk-position-absolute uk-transform-center lpb-markengagement lpb-markengagement1"
                                            style={{ "left": "64%", "top": "49%" }}
                                            href="/a-propos-de-nous" data-uk-marker>

                                            <div className="uk-container lpb-blochoverbouchon  uk-text-left lpb-blochoverbouchon-responsive1">
                                                <h3 className="uk-text-left lpb-titlehoverbouchon">Le repas</h3>
                                                <p className="uk-text-left lpb-textehoverbouchon">Sous contrôle diététique et avec l’éclairage des parents,
                                                c’est une nourriture saine, variée et connue de l’enfant qui lui est présentée.
                                                L’ambiance autour de cetemps de soin est soignée de sorte à offrir, également,
                                                à l’enfant une nourriture affective nécessaire à son autonomie émotionnelle et fonctionnelle.
                                            </p>
                                                <a className="lien" href="/posts/60262015ede2a800236cc293">
                                                    <button className="uk-text-left lpb-buttonhoverbouchon">En savoir plus</button>
                                                </a>
                                            </div>

                                        </div>

                                        <div class="uk-position-absolute uk-transform-center lpb-markengagement lpb-markengagement1"
                                            style={{ "left": "65%", "top": "74%" }}
                                            href="" data-uk-marker>
                                            <div className="uk-container lpb-blochoverbouchon uk-text-left lpb-blochoverbouchon-responsive1">
                                                <h3 className="uk-text-left lpb-titlehoverbouchon">Le sommeil</h3>
                                                <p className="uk-text-left lpb-textehoverbouchon">Tout en étant à l’affût quant aux signes de fatigue chez chaque enfant,
                                                nous disposons de repères propices à un sommeil de qualité. Le rythme de l’enfant ainsi que ses habitudes familiales en matière
                                            de sommeil et d’endormissement sont préservés dans la mesure du possible.</p>
                                                <a className="lien" href="/posts/60262129ede2a800236cc299">
                                                    <button className="uk-text-left lpb-buttonhoverbouchon">En savoir plus</button>
                                                </a>

                                            </div>

                                        </div>

                                        <div class="uk-position-absolute uk-transform-center lpb-markengagement "
                                            style={{ "left": "41%", "top": "64%" }}
                                            href="" data-uk-marker>
                                            <div className="uk-container lpb-blochoverbouchon uk-text-left lpb-blochoverbouchon-responsive">
                                                <h3 className="uk-text-left lpb-titlehoverbouchon">Le change et le contôle des sphincters</h3>
                                                <p className="uk-text-left lpb-textehoverbouchon">Grâce à la «chorégraphie du change», l’enfant (dès nourrisson) intègre au fur et à mesure
                                                l’enchaînement des soins qui lui sont prodigués. Ceci lui permet d’être pleinement acteur de son change et ainsi de tendre,
                                                progressivement,
                                            à une pleine confiance en ses capacités nécessaire sur la voie de l’apprentissage dit «de la propreté».</p>
                                                <a className="lien" href="/posts/60262099ede2a800236cc296">
                                                    <button className="uk-text-left lpb-buttonhoverbouchon">En savoir plus</button>
                                                </a>
                                            </div>

                                        </div>

                                        <div class="uk-position-absolute uk-transform-center lpb-markengagement"
                                            style={{ "left": "43%", "top": "87%" }}
                                            href="/a-propos-de-nous" data-uk-marker>
                                            <div className="uk-container lpb-blochoverbouchon  uk-text-left lpb-blochoverbouchon-responsive">
                                                <h3 className="uk-text-left lpb-titlehoverbouchon">Le départ de l’enfant</h3>
                                                <p className="uk-text-left lpb-textehoverbouchon">Après l’achèvement du processus de séparation avec l’environnement de la crèche,
                                                viennent les retrouvailles de l’enfant avec ses parents. Ceux-ci sont ensuite informés, lors des transmissions, du déroulement de la
                                                journée,
                                            un déroulé que l’enfant peut décrire lui-même lorsqu’il le souhaite.</p>
                                                <a className="lien" href="/posts/60262278ede2a800236cc29e">
                                                    <button className="uk-text-left lpb-buttonhoverbouchon">En savoir plus</button>
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div>
                            </div>
                        </div>

                    </div>
                </div>
                <img src={damn} alt="" className="lpb-videohorloge uk-text-center" />
                <img src={video_mobile} alt="" className="lpb-videohorlogeresponsive uk-text-center" />








            </div>
            <Footer />
        </div>
    )
}
