import React, { useState, useEffect, Fragment } from "react";
import "./index.css";
import MetaTags from 'react-meta-tags';
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Creches from "../../components/Creches";
import Banner from "../../components/Banner";
import Nurseries from "../../components/Nurseries"; 
import bg from "../../assets/img/bg.png";
import parse from "html-react-parser";
import { useLocation } from "react-router-dom";
import service from "../../service";
import config from "../../config";
import axios from "axios";

const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);

export default function Foetz() {
  const { pathname } = useLocation();
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [page, setPage]=useState(null);
  useEffect(() => {
    getPageSections();
  }, []);

  const  getPageSections = async () => {
    try {
      let response = await $service.getPages({
        url: pathname
      });
      if (response && response.length>0) {
        setPage(response[0]);
      }
    } catch (err) {
      setIsError(true);
    }
    setIsLoading(false);
  };
  return (
    <div>
      <Header />
      <MetaTags>
        <title>Crèche et Foyer de jour à Foetz - Les P’tits Bouchons</title>
        <meta name="description" content="A Foetz notre crèche et foyer de jour accueille les enfants de 2 mois à 12 ans du lundi au vendredi, de 5h à 22H et de 7H à 19H les samedis." />
        <meta property="og:title" content="Crèche et Foyer de jour à Foetz - Les P’tits Bouchons" />
      </MetaTags>
      <div
        className="lpb-backgroundfoyer"
        id="apropos"
        style={{
          backgroundImage: "url(" + bg + ")",
        }}
      >
         {isLoading && <div><div style={{flex: "initial",width: "auto"}} className="loader"><div data-uk-spinner /></div></div>}
         {
            (!isLoading && page) &&
            <Fragment>
              <Banner
                backgroundImage={`${config.backofficeURL}${page.sections[0].image.url}`}
                position="center"
                title={page.sections[0].title}
                desc={parse(page.sections[0].content)}
              />
              <Creches
                bloctexte={parse(page.sections[1].content)}
                address={parse(page.sections[5].content)}
                titrecreche={page.sections[2].title}
                textecreche={parse(page.sections[2].content)}
                imagebloc2={`${config.backofficeURL}${page.sections[2].image.url}`}
                titlebebe=" Section bébés"
                textebebe=" De 2 à 18 mois"
                titlepetit="Section petits"
                textepetit="De 18 mois à 2 ans et demi"
                titlemoyen=" Section moyens"
                textemoyen=" De 2 ans et demi à 4 ans"
              />
              <div className="lpb-bigbloggreen">
                <div className="uk-container">
                  <div className="uk-child-width-expand@s" data-uk-grid>
                    <div className="uk-grid-item-match">
                      <div className="uk-card uk-card-default uk-card-body lpb-bloc-toutltletexte">
                        
                        <h3 className="lpb-titlebigblocgreen">{page.sections[3].title}</h3>
                        <p class="lpb-textebigblocgreen">{parse(page.sections[3].content)}</p>
                      </div>
                    </div>
                    <div>
                      <img className="lpb-imgbigblocgreen" src={`${config.backofficeURL}${page.sections[3].image.url}`} />
                    </div>
                  </div>

                  <div className="uk-text-center">
                  <a href={`/pré-inscription`}> 
                    <button
                      className="uk-text-center lpb-btnpreinscriptionbloc lpb-color"
                      
                    >
                      Pré-inscription
                    </button>
                    </a>
                  </div>
                </div>
              </div>
              <div className="uk-container">
                <div className="lpb-blocnocolor">
                  <div className="uk-child-width-expand@s" data-uk-grid>
                    <div className="uk-grid-item-match">
                      <div className="uk-card uk-card-default uk-card-body">
                        <img
                          className="lpb-imgbigblocnocolorpage"
                          src={`${config.backofficeURL}${page.sections[4].image.url}`}
                        />
                      </div>
                    </div>
                    <div className="lpb-mybloctextblocnocolor">
                      <p className="lpb-titleblocnocolor">
                      {page.sections[4].title}
                      </p>
                      <p className="lpb-texteblocnocolor">{parse(page.sections[4].content)} </p>
                      <div className="lpb-btn-responsivepreinscription">
                      <a href={`/pré-inscription`}>
                        <button
                          className="  lpb-btnpreinscriptionblocnocolor"
                          href={`/pré-inscription`}
                        >
                          Pré-inscription
                        </button>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Fragment>
         }
         
         <Nurseries/>
      </div>
      <Footer />
    </div>
  );
}
