import React, { useState, useEffect } from "react";
import "./index.css";

import MetaTags from 'react-meta-tags';
import service from "../../service";
import config from "../../config";
import axios from "axios";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Newsletter from "../../components/Newsletter";
import Nurseries from "../../components/Nurseries";
import bg from "../../assets/img/bg.png";
import coverVideo from "../../assets/img/video.mp4";
import parse from "html-react-parser";
import nairgymLogoWhite from "../../assets/img/nairgym-logo-white.png";
import nairgymAvatar from "../../assets/img/nairgymavatar.png";
import Bgcolum from "../../assets/img/bgcolum.png";
import BgcolumMobile from "../../assets/img/bg-creche.png";
import ActualityBox from "../../components/ActualityBox";
import FeaturesCreche from "../../components/FeaturesCreche"
//import FeaturesCrecheTwo from "../../components/FeaturesCrecheTwo"

const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);


export default function Home() {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [accueildata, setAccueildata]=useState([]);

  useEffect(() => {
    getAccueildata();
  }, []);

  const   getAccueildata = async () => {
      let response = await $service.getPage({});
      if (response) {
        setAccueildata(response.sections);
      }
    setIsLoading(false);
  };
  return (
    <div>
      <Header />
      <MetaTags>
        <title>Crèches et foyers au Luxembourg - Les P’tits Bouchons</title>
        <meta name="description" content="Nos crèches et foyers de jour au Luxembourg ont à coeur d'accompagner l'enfant dans son développement, l'envelopper, et le guider dans un cadre sécurisant." />
        <meta property="og:title" content="Crèches et foyers au Luxembourg - Les P’tits Bouchons" />
      </MetaTags>
      <div>
        <div className="lpb-home-pageslider-video-container">
          <div className="uk-cover-container">
            <video
              src={coverVideo}
              controls
              controlsList="nodownload"
              className="lpb-home-pageslider-video-full"
              playsInline
              autoPlay
              loop
              muted
              type="video/mp4"
            />
          </div>
          <div className="uk-text-center lpb-home-pageslider-video-wrap">
            <div className="lpb-home-pageslider-video-title">
              Nos horaires d’ouvertures :
            </div>
            <div className="lpb-home-pageslider-video-time">05H - 22H</div>
          </div>
        </div>

        <div style={{ backgroundImage: "url(" + bg + ")" }}>

        {accueildata && accueildata.length>0 &&
          <div className="uk-container lpb-home-page-philosophy-content">
            <div className=" uk-text-center lpb-home-page-philosophy-container-box">
              <div className="lpb-home-page-philosophy-title">
               {accueildata[0].title}
              </div>
              <div className="lpb-home-page-philosophy-desc">
           
              {parse(accueildata[0].content)}
              </div>
            </div>

            <div className="uk-grid-small" data-uk-grid>
              <div className="uk-width-1-1@s uk-width-1-3@m">
                <div className="lpb-home-page-philosophy-card-container">
                  <div className="lpb-home-page-philosophy-card-content-cover" />
                  <img src= {config.backofficeURL+accueildata[1].image.url} alt="philosophy" />
                  <div className="uk-text-center lpb-home-page-philosophy-card-content-text">
                  {accueildata[1].title}
                  </div>
                </div>
              </div>
              <div className="uk-width-1-1@s uk-width-1-3@m">
                <div className="lpb-home-page-philosophy-card-container">
                  <div className="lpb-home-page-philosophy-card-content-cover" />
                  <img src={config.backofficeURL+accueildata[2].image.url} alt="philosophy" />
                  <div className="uk-text-center lpb-home-page-philosophy-card-content-text">
                  {accueildata[2].title}
                  </div>
                </div>
              </div>
              <div className="uk-width-1-1@s uk-width-1-3@m">
                <div className="lpb-home-page-philosophy-card-container">
                  <div className="lpb-home-page-philosophy-card-content-cover" />
                  <img src={config.backofficeURL+accueildata[3].image.url} alt="philosophy" />
                  <div className="uk-text-center lpb-home-page-philosophy-card-content-text">
                  {accueildata[3].title}
                  </div>
                </div>
              </div>
            </div>
          </div>
           }
        </div>

        <Nurseries />

        <div
          className="lpb-home-page-features-wrap lpb-home-page-features-content-full"
          style={{
            backgroundImage: "url(" + Bgcolum + ")",
          }}
        >
          <FeaturesCreche />
        </div>

        <div
          className="lpb-home-page-features-wrap-mobile lpb-home-page-features-content-full"
          style={{
            backgroundImage: "url(" + BgcolumMobile + ")",
          }}
        >
          <FeaturesCreche />
        </div>
        {accueildata && accueildata.length>0 &&
        <div style={{ backgroundImage: "url(" + bg + ")" }}>
          <div className="uk-container lpb-home-page-actuality-content">
            <div className="uk-text-center lpb-home-page-actuality-container-box">
              <div className="lpb-home-page-actuality-title">
              {accueildata[5].title}
              </div>
              <div className="lpb-home-page-actuality-desc">
              {parse(accueildata[5].content)}
              </div>
              <div className="lpb-home-page-actuality-divider" />
              <div className="lpb-home-page-actuality-citation-author">
                Emmi Pikler
              </div>
            </div>
            <ActualityBox limit={4} isOthers={false} />
          </div>
        </div>
  }
        <div style={{ backgroundImage: "url(" + bg + ")" }}>
      
          <div className="uk-container lpb-home-page-nairgym-infos-container">
            <div className="lpb-home-page-nairgym-avatar-wrap">
              <img src={nairgymAvatar} alt="narigym" />
            </div>
          
            <div className="lpb-home-page-nairgym-infos-content">
              <div className="uk-text-center">
                <div>
                  <img src={nairgymLogoWhite} alt="narigym" />
                </div>
                {accueildata && accueildata.length>0 &&
                <div className="lpb-home-page-nairgym-infos-details-title">
                {accueildata[4].title}
                </div>
               }  </div>
                 {accueildata && accueildata.length>0 &&
              <div data-uk-grid>
                <div className="uk-width-1-1@s uk-width-1-2@m">
                  <div>
                    <img className="lpb-home-page-nairgym-infos-sport-img" src= {config.backofficeURL+accueildata[4].image.url} alt="narigym" />
                  </div>
                </div>
                <div className="uk-width-1-1@s uk-width-1-2@m">
                  <div className="lpb-home-page-nairgym-infos-details-desc-wrap">
                    <div className="lpb-home-page-nairgym-infos-details-desc">
                    {parse(accueildata[4].content)}
                    </div>
                    <div className="uk-text-center">
                    <a href={`${config.NairgymLink}`} target="_BLANK" className="lpb-a-buttondecouvrir-home"> 
                      <button className="uk-button uk-button-default lpb-home-page-nairgym-infos-btn lpb-traking-nairgym-home-btn">
                        Découvrir
                      </button></a> 
                    </div>
                  </div>
                </div>
              </div>
           }  </div>
           </div>
       </div>
      </div>
      <div className="lpb-home-page-newsletter-component-content">
        <Newsletter />
      </div>
      <Footer />

    </div>
  );
}
