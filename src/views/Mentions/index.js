import React, { Component } from "react";
import Header from "../../components/Header";
import bg from "../../assets/img/bg.png";
import Footer from "../../components/Footer";
import "./index.css";
import MetaTags from 'react-meta-tags';
import logoImg from "../../assets/img/logo.png";
export default class index extends Component {
  render() {
    return (
      <div>
        <Header />
        <MetaTags>
          <title>Mention légale</title>
          <meta name="description" content="" />
          <meta property="og:title" content="Mention légale" />
        </MetaTags>
        <div
          className="lpb-backgroundfoyer"
          id="apropos"
          style={{
            backgroundImage: "url(" + bg + ")",
          }}
        >
          {" "}
          <div className="uk-container lpb-mentionsbloc">
            <div className="lpb-mentionslegales">
              <div className="uk-text-center logopolitique">
                <img className="" src={logoImg} alt="logo" />
              </div>
              <div className="uk-text-center lpb-titrementionss">
                Mentions légales
              </div>

              <div className=" blocc">
                <div className="bloctexte">
                  <h3 className="titresecondmentions">Propriétaire du site</h3>
                  <p className="textementions">
                    Le site{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>{" "}
                    appartient à la société « Les P’tits Bouchons Sarl ». Pour
                    toute question sur ce site et son contenu, veuillez nous
                    contacter par e-mail :{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/contact"
                      className="lienUn"
                    >
                      contact@lesptitsbouchons.lu
                    </a>
                    .
                  </p>

                  <p className="textementions">
                    Les P’tits Bouchons
                    <br />
                    Sàrl au capital de 12 500 Euros
                    <br />
                    Siège social : 10 Rue de l’avenir L-3895 Foetz
                    <br />
                    Immatriculée au RCSL sous le numéro B88057
                  </p>

                  <p className="textementions position">
                    Directrice de la Publication : Héloïse Pierre
                  </p>

                  <h2 className="titresecondmentions">Éditeur du site</h2>
                  <p className="textementions">
                    Le site{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>{" "}
                    est édité par Hemlè{" "}
                    <a href="https://hemle.lu/" className="lienUn">
                      hemle.lu
                    </a>{" "}
                    une marque commerciale de la société LuxDataDigit.
                  </p>
                  <p className="textementions">
                    LuxDataDigit
                    <br />
                    Numéro d’autorisation : 10116648 –<br />
                    Numéro d’immatriculation : B244881 - Numéro TVA : LU
                    32217988 -<br />
                    Siège social : 10A rue des Mérovingiens L-8070 Bertrange -
                    Luxembourg
                    <br />
                    Tel. +352 691 318 601
                  </p>
                  <h1 className="titresecondmentions">Hébergeur</h1>
                  <div className="blocovh">
                    <p className="textementions">
                      OVH SAS est une filiale de la société OVH Groupe SAS,
                      société immatriculée au RCS de Lille sous le numéro 537
                      407 926 sise 2, rue Kellermann, 59100 Roubaix.{" "}
                    </p>
                  </div>
                  <h1 className="titresecondmentions"> Notice légale </h1>
                  <p className="textementions">
                    Les crèches et foyer de jour Les P’tits Bouchons alimentent
                    ce site en vue d’informer sur les services de garde proposés
                    par les différentes structures. <br />{" "}
                  </p>
                  <p className="textementions">
                    {" "}
                    L’objectif de ce site est de diffuser des informations
                    actualisées, mais nous ne saurions toutefois éviter toute
                    erreur ou tout défaut de mise à jour. <br />
                  </p>
                  <p className="textementions">
                    {" "}
                    Les informations diffusées sur le présent site n’engagent
                    pas la responsabilité des crèches et foyer de jour Les
                    P’tits Bouchons. <br />
                  </p>
                  <h1 className="titrementions">
                    {" "}
                    Loi applicable et Juridiction
                  </h1>
                  <p className="textementions">
                    {" "}
                    Tout litige relatif à l'utilisation du site Internet{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>{" "}
                    sera soumis à la loi luxembourgeoise et relève de la
                    compétence exclusive des tribunaux luxembourgeois. <br />
                  </p>
                  <h1 className="titrementions"> Copyright</h1>
                  <p className="textementions">
                    {" "}
                    © 2021 Les P’tits Bouchons. Tous droits réservés. <br />
                  </p>
                  <p className="textementions">
                    {" "}
                    La reproduction partielle ou intégrale des informations,
                    pictogrammes, photographies, images, textes, séquences
                    vidéo, et autres documents présents sur le site Internet est
                    uniquement autorisée avec l'accord préalable et écrit de Les
                    P’tits Bouchons.
                    <br />
                  </p>

                  <h1 className="titrementions">
                    Le respect de votre vie privée
                  </h1>
                  <p className="textementions">
                    {" "}
                    Les P’tits Bouchons s’engage à respecter la vie privée des
                    utilisateurs du site notamment dans le traitement de leurs
                    données à caractère personnel. En vertu du règlement (EU) n°
                    2016/679 du Parlement européen et du Conseil du 27 avril
                    2016 relatif à la protection des données à caractère
                    personnel et à la libre circulation de ces données, et
                    abrogeant la directive 95/46/CE, ci-après « RGPD », entré en
                    vigueur le 25 mai 2018, Les P’tits Bouchons est le
                    responsable du traitement des données à caractère personnel.
                  </p>
                  <p className="textementions">
                    {" "}
                    Ce traitement repose sur la ou les finalités suivantes :
                    exécution des obligations contractuelles Les P’tits Bouchons
                    envers les utilisateurs, réponses aux questions des
                    utilisateurs.
                  </p>
                  <p className="textementions">
                    La base juridique pour le traitement des données
                    personnelles des utilisateurs est notamment la bonne
                    exécution de la relation contractuelle entre Les P’tits
                    Bouchons et ses utilisateurs, respectivement l’entrée en
                    relation contractuelle.
                  </p>
                  <p className="textementions">
                    De même, le traitement des données personnelles des
                    utilisateurs peut être basé sur l’intérêt légitime de Les
                    P’tits Bouchons, sinon sur le consentement des utilisateurs
                    (lors de la prise en contact via le formulaire de contact
                    sur le site web lesptitsbouchons.lu).
                  </p>
                  <p className="textementions">
                    En l’occurrence, les données personnelles suivantes sont
                    éventuellement recueillies et traitées par Les P’tits
                    Bouchons (sans que cette liste ne soit à considérer comme
                    exhaustive) : prénom et nom, adresse, date et lieu de
                    naissance, adresse e-mail, numéro de téléphone.
                  </p>
                  <p className="textementions">
                    Ces données seront conservées pour une durée n’excédant pas
                    celle nécessaire à la réalisation des finalités pour
                    lesquelles elles ont été collectées, sans préjudice de Les
                    P’tits Bouchons de les conserver pour une durée plus longue
                    en raison d’obligations légales/réglementaires s’appliquant
                    à Les P’tits Bouchons, respectivement en raison de
                    situations exceptionnelles qui justifieraient une
                    conservation plus longue (procédure judiciaire etc.). Elles
                    seront donc conservées au moins pendant la durée du
                    traitement de la demande de l’utilisateur. Les P’tits
                    Bouchons s’engage à effacer les données en sa possession,
                    sans préjudice quant à une conservation plus longue de
                    certaines données personnelles telle que décrite dans le
                    présent paragraphe.{" "}
                  </p>

                  <p className="textementions">
                    En accédant sur le site{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>{" "}
                    (ci-après « Site ») et plus particulièrement en utilisant le
                    formulaire de contact, l’utilisateur donne son consentement
                    clair et non-équivoque au traitement de ses données
                    personnelles par Les P’tits Bouchons. L’utilisateur est
                    informé du fait qu’il dispose d’un droit de retrait de son
                    consentement. Si ce droit est exercé, les données à
                    caractère personnel de l’utilisateur ne seront plus
                    traitées.{" "}
                  </p>

                  <p className="textementions">
                    En outre, l’utilisateur est informé que même en l’absence de
                    l’utilisation d’un formulaire de Contact, certaines données
                    sont collectées automatiquement, à savoir p.ex. les données
                    techniques (y compris l’adresse IP, le fournisseur d’accès à
                    Internet, le système d’exploitation, type de navigateur
                    utilisé, date et heure d’accès au Site etc.), mais qu’elles
                    ne sont en aucun cas transmises à des tiers, ni utilisées
                    par Les P’tits Bouchons pour procéder à un profilage des
                    utilisateurs du Site. Ces données techniques servent
                    uniquement à des fins statistiques et/ou pour la gestion
                    et/ou l’amélioration du Site. Pour de plus amples
                    informations concernant maintenant les cookies, veuillez
                    consulter notre règlement concernant les Cookies.
                  </p>

                  <p className="textementions">
                    L’utilisateur du site est informé du fait qu’il dispose, en
                    vertu du règlement européen précité, dans des cas
                    particuliers, de divers droits : le droit d’être informé de
                    l’utilisation et du traitement de ses données personnelles,
                    le droit d’accès à ses données personnelles, le droit de
                    pouvoir demander la limitation du traitement, le droit de
                    rectification, d’effacement et d’opposition au traitement de
                    ses données personnelles, le droit à la portabilité de ses
                    données ainsi que le droit d’introduire une réclamation
                    auprès de la Commission nationale pour la protection des
                    données (CNPD).
                  </p>

                  <p className="textementions">
                    En outre, l’utilisateur est informé que même en l’absence de
                    l’utilisation d’un formulaire de Contact, certaines données
                    sont collectées automatiquement, à savoir p.ex. les données
                    techniques (y compris l’adresse IP, le fournisseur d’accès à
                    Internet, le système d’exploitation, type de navigateur
                    utilisé, date et heure d’accès au Site etc.), mais qu’elles
                    ne sont en aucun cas transmises à des tiers, ni utilisées
                    par Les P’tits Bouchons pour procéder à un profilage des
                    utilisateurs du Site. Ces données techniques servent
                    uniquement à des fins statistiques et/ou pour la gestion
                    et/ou l’amélioration du Site. Pour de plus amples
                    informations concernant maintenant les cookies, veuillez
                    consulter notre règlement concernant les Cookies.
                  </p>

                  <p className="textementions">
                    Pour faire valoir ces droits, l’utilisateur devra s’adresser
                    à l’adresse suivante :{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/contact"
                      className="lienUn"
                    >
                      contact@lesptitsbouchons.lu
                    </a>
                  </p>

                  <h1 className="titrementions">
                    Les P’tits Bouchons s’engage à
                  </h1>

                  <p className="textementions">
                    respecter les droits des utilisateurs
                  </p>
                  <ul className="textementions">
                    <li>
                      ne traiter que les données en corrélation avec la ou les
                      finalités déterminées.
                    </li>
                    <li>
                      fournir, sur demande expresse de l’utilisateur, les
                      données à caractère personnel dans les plus brefs délais
                      et au maximum après un délai de 1 mois à partir de la
                      réception de la demande.
                    </li>
                    <li>garantir la confidentialité des données collectées.</li>
                    <li>
                      adopter les niveaux de sécurité de protection des données
                      personnelles requis et prévoir tous les moyens et mesures
                      techniques à sa portée afin d’éviter la perte, la mauvaise
                      utilisation, l’altération, la fuite, l’accès non autorisé
                      et le vol des données personnelles.
                    </li>
                  </ul>
                  <p className="textementions">
                    Les P’tits Bouchons ne peut pas être responsable des
                    violations de sécurité ne relevant pas de son contrôle.
                  </p>
                  <p className="textementions">
                    {" "}
                    L’utilisateur et Les P’tits Bouchons s’engagent à respecter
                    les règles prévues par le règlement.
                  </p>

                  <h1 className="titrementions">Détails du contact</h1>

                  <p className="textementions">
                    {" "}
                    Les éventuelles questions et demandes relatives aux données
                    personnelles doivent être adressées à{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/"
                      className="lienUn"
                    >
                      contact@lesptitsbouchons.lu
                    </a>
                    .
                  </p>

                  <h1 className="titrementions">Cookies</h1>
                  <p className="textementions">
                    Un cookie est un petit fichier de données ou de texte qui
                    est placé sur votre ordinateur par des sites web. Il en
                    existe de différentes sortes comme par exemple les cookies
                    techniques qui sont utilisés pour paramétrer la langue du
                    site web, les cookies de sessions (également appelés cookies
                    temporaires) et les cookies de pistage installés afin
                    d’optimiser le site web en fonction de votre comportement.
                  </p>
                  <p className="textementions">
                    Si vous voulez consulter le site web lesptitsbouchons.lu,
                    nous vous recommandons d'accepter les cookies. Cependant,
                    vous êtes libre de ne pas les accepter. Vous pouvez
                    également librement et à tout moment retirer votre
                    consentement.
                  </p>

                  <h1 className="titrementions">
                    Liste des cookies que nous utilisons
                  </h1>
                  <table class="uk-table uk-table-striped bordertable">
                    <thead>
                      <tr>
                        <th></th>
                        <th className="uk-text-center">Liste des cookies</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td className="bordertable">Nom</td>
                        <td className="bordertable"> Domaine</td>
                        <td className="bordertable">Expiration</td>
                      </tr>
                      <tr>
                        <td className="bordertable">-</td>
                        <td className="bordertable">-</td>
                        <td className="bordertable">-</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                  <h1 className="titrementions">Gestion des cookies</h1>
                  <p className="textementions">
                    L’enregistrement d’un cookie dans votre terminal est
                    subordonné à votre volonté. Grâce à l’outil Ghostery ou aux
                    paramétrages de votre logiciel de navigation, vous pouvez, à
                    tout moment, simplement et gratuitement, choisir d’accepter
                    ou non l’enregistrement de cookies sur votre terminal.
                  </p>

                  <p className="textementions">
                    Les choix qui vous sont offerts grâce à l’outil Ghostery.
                  </p>
                  <p className="textementions">
                    Avec l’outil Ghostery, vous pouvez choisir les cookies
                    pouvant être déposés sur votre site internet, à l’exception
                    des cookies essentiels, strictement nécessaires à la
                    navigation du site{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>{" "}
                    ainsi qu’au pilotage de l’activité du site{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>
                    .
                  </p>

                  <p className="textementions">
                    Les choix qui vous sont offerts par votre logiciel de
                    navigation (Internet Explorer, Firefox, Google Chrome,
                    etc.).
                  </p>
                  <p className="textementions">
                    Vous pouvez configurer votre logiciel de navigation selon
                    votre volonté, de manière à ce que des cookies soient (1)
                    acceptés et enregistrés dans votre terminal ou, au
                    contraire, (2) afin qu'ils soient refusés.
                  </p>

                  <ol className="titremention">
                    <li>
                      {" "}
                      <h4 className="titrementions">
                        L'accord sur les Cookies
                      </h4>
                    </li>

                    <p className="textementions">
                      Si votre logiciel de navigation est paramétré de manière à
                      accepter l’enregistrement de cookies dans votre terminal,
                      les cookies intégrés dans les pages et contenus que vous
                      avez consultés seront systématiquement enregistrés dans
                      votre terminal.
                    </p>

                    <li>
                      <h4 className="titrementions">Le refus des Cookies</h4>
                    </li>
                    <p className="textementions">
                      Vous pouvez paramétrer votre logiciel de navigation de
                      manière :
                    </p>

                    <ul className="textementions">
                      <li>
                        à ce que l’acceptation ou le refus des cookies vous
                        soient proposés ponctuellement, avant qu’un cookie soit
                        susceptible d’être enregistré.
                      </li>
                      <li>
                        à refuser systématiquement l’enregistrement de cookies
                        dans votre terminal.
                      </li>
                    </ul>
                  </ol>

                  <p className="textementions">
                    Attention : Tout paramétrage que vous pouvez entreprendre
                    sur votre logiciel de navigation concernant l’acceptation ou
                    le refus des cookies sera susceptible de modifier votre
                    navigation sur Internet et vos conditions d'accès à certains
                    services nécessitant l'utilisation de ces mêmes cookies. Si
                    vous choisissez de refuser l’enregistrement de cookies dans
                    votre terminal ou si vous supprimez ceux qui y sont
                    enregistrés, nous déclinons toute responsabilité pour les
                    conséquences liées au fonctionnement dégradé de nos services
                    résultant de l'impossibilité pour nous d'enregistrer ou de
                    consulter les cookies nécessaires à leur fonctionnement et
                    que vous auriez refusés ou supprimés.
                  </p>
                  <p className="textementions">
                    Si vous ne souhaitez pas que le site{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>{" "}
                    puisse déposer tout ou partie des Cookies, vous pouvez
                    également sortir du site en cliquant sur ce lien :{" "}
                    <a href="https://www.google.com/" className="lienUn">
                      www.google.com
                    </a>
                    .
                  </p>

                  <h1 className="titrementions">
                    Comment exercer vos choix, selon le navigateur que vous
                    utilisez ?
                  </h1>
                  <p className="textementions">
                    La configuration de chaque logiciel de navigation est
                    différente. Elle est généralement décrite dans le menu
                    d'aide de votre logiciel de navigation. Nous vous invitons
                    donc à en prendre connaissance. Vous pourrez ainsi savoir de
                    quelle manière modifier vos souhaits en matière de cookies.
                  </p>

                  <ul className="textementions">
                    <li>
                      <a
                        href="https://support.microsoft.com/fr-fr/topic/supprimer-et-g%C3%A9rer-les-cookies-168dab11-0753-043d-7c16-ede5947fc64d"
                        className="lienUn"
                      >
                        Pour Internet Explorer™
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://support.apple.com/fr-fr/guide/safari/sfri11471/mac"
                        className="lienUn"
                      >
                        Pour Safari™
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://support.google.com/chrome/answer/95647?hl=fr&hlrm=en"
                        className="lienUn"
                      >
                        Pour Chrome™
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://support.mozilla.org/fr/kb/protection-renforcee-contre-pistage-firefox-ordinateur?redirectlocale=fr&redirectslug=activer-desactiver-cookies-preferences"
                        className="lienUn"
                      >
                        Pour Firefox™
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://help.opera.com/en/latest/web-preferences/"
                        className="lienUn"
                      >
                        Pour Opera™
                      </a>
                    </li>
                  </ul>
                  <h1 className="titrementions">
                    Droits liés à la protection des données personnelles
                  </h1>
                  <p className="textementions">
                    De manière générale, conformément au Règlement (UE) 2016/679
                    du parlement européen et du conseil du 27 avril 2016 relatif
                    à la protection des données à caractère personnel et à la
                    libre circulation de ces données, et abrogeant la directive
                    95/46/CE, ci-après « RGPD », entrée en vigueur le 25 mai
                    2018, dans des cas particuliers, vous disposez d'un droit
                    d’être informé de l’utilisation du traitement des données
                    personnelles conformément au RGPD, d’un droit d'accès, d’un
                    droit de rectification et de suppression de vos données à
                    caractère personnel, d’un droit de pouvoir limiter le
                    traitement, d’un droit de s’opposer, d’un droit à la
                    portabilité ainsi que d’un droit d’introduire une
                    réclamation auprès de la Commission nationale pour la
                    protection des données (CNPD).
                  </p>

                  <p className="textementions">
                    Vous pouvez également vous opposer pour un motif légitime à
                    l’utilisation de vos données, ou sans avoir à justifier de
                    motif lorsque votre droit d’opposition vise à faire cesser
                    toute prospection de{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/"
                      className="lienUn"
                    >
                      www.lesptitsbouchons.lu
                    </a>
                    . Vous pouvez enfin définir des directives relatives à la
                    conservation, l’effacement et la communication de vos
                    données à caractère personnel après votre décès. Pour
                    exercer vos droits, il vous suffit de nous écrire à{" "}
                    <a
                      href="https://lbpv2-dev.luxdatadigit.lu/contact"
                      className="lienUn"
                    >
                      contact@lesptitsbouchons.lu
                    </a>{" "}
                    ou par courrier à Les P’tits Bouchons, 3, rue des Artisans
                    L-3895 Foetz en nous indiquant vos nom, prénom, adresse
                    e-mail.
                  </p>

                  <p className="textementions">
                    Conformément à la réglementation en vigueur, toute demande
                    d’exercice de vos droits doit être signée et accompagnée de
                    la photocopie d’un titre d’identité portant votre signature
                    et préciser l’adresse à laquelle doit vous parvenir la
                    réponse. Une réponse vous sera alors adressée dans un délai
                    de 1 mois suivant la réception de la demande.
                  </p>

                  <h1 className="titrementions">
                    Violation / pertes de données
                  </h1>
                  <p className="textementions">
                    Toutes perte/violation des données personnelles sera
                    notifiée à la CNPD dans les 72 heures ainsi qu’aux personnes
                    concernées en vertu du RGPD, après en avoir pris
                    connaissance, à moins que la violation en question ne soit
                    pas susceptible d’engendrer un risque pour les droits et
                    libertés des personnes concernées.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
