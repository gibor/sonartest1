import React, { Component } from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import bg from "../../assets/img/bg.png";
import DernierArticle from "../../components/DernierArticle";
import Bloclecture from "../../components/bloclecture";
import "./index.css";
export default class index extends Component {
  render() {
    return (
      <div>
        <Header />

        <div
          className="lpb-backgroundfoyer"
          id="apropos"
          style={{
            backgroundImage: "url(" + bg + ")",
          }}
        >
          <div className="uk-container">
            <div
              className="uk-grid-match uk-child-width-expand@s lpb-margin-suite-blog"
              data-uk-grid
            >
              <Bloclecture
              titrepremierblog="N'AIRGYM"
              texteblog="contenu manquant
                      "

              
              />
             
              <div className="uk-width-1-3@m  lpb-catesuiteblog">
                <DernierArticle />
              </div>
            </div>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}
