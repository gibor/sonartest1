import React, { useState, useEffect } from 'react';
import './index.css';
import MetaTags from 'react-meta-tags';
import { HashLink } from 'react-router-hash-link';
import Nurseries from '../../components/Nurseries';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Image160 from '../../assets/img/image160.png';
import Image161 from '../../assets/img/image161.png';
import Imgcrechesuite1 from '../../assets/img/imgcrechesuite1.png';
import Imgcrechesuite2 from '../../assets/img/imgcrechesuite2.png';
import Imgcrechesuite3 from '../../assets/img/imgcrechesuite3.png';
import Imgcrechesuite4 from '../../assets/img/imgcrechesuite4.png';
import Imgcrechesuite5 from '../../assets/img/imgcrechesuite5.png';
import Imgespace1 from '../../assets/img/imgespace1.png';
import Imgespace2 from '../../assets/img/imgespace2.png';
import Imgespace3 from '../../assets/img/imgespace3.png';
import Imgespace4 from '../../assets/img/imgespace4.png';
import Imgespace5 from '../../assets/img/imgespace5.png';
import Imgespace6 from '../../assets/img/imgespace6.png';
import coverVideo from '../../assets/img/video.mp4';
import Bg from '../../assets/img/bg.png';
import service from '../../service';
import config from '../../config';
import axios from 'axios';
import Slick from 'react-slick';
import parse from 'html-react-parser';
import 'slick-carousel/slick/slick.css';
import { Link } from 'react-router-dom';

const $service = service(
  axios,
  config,
  config.backofficeURL
  // config.tokenBackoffice
);

const scrollWidthOffset = (el, yOffset = -90) => {
  const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
  window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' });
};
//import "slick-carousel/slick/slick-theme.css";
const settings = {
  dots: false,
  centerMode: false,

  slidesToShow: 5,
  infinite: false,

  autoplay: true,
  autoplaySpeed: 2000,
  // speed: 500,
  // autoplaySpeed: 500,
  arrows: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        centerMode: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        centerMode: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        centerPadding: '0px',
      },
    },
  ],
};

export default function OursSections() {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [page, setPage] = useState({
    sectionsdata: [],
    categories: [],
    motricitePost: null,
    individualisationPost: null,
  });
  useEffect(() => {
    getPageData();
  }, []);

  const getPageData = async () => {
    try {
      const categoriesResponse = await $service.getCategories({}),
        sectionsResponse = await $service.getSections({});
      let motricitePost = null,
        individualisationPost = null;
      if (categoriesResponse && sectionsResponse) {
        categoriesResponse.every((item) => {
          item.blogs.every((post) => {
            const postText =
              `${post.title} ${post.description} ${post.content}`.toLowerCase();
            if (!motricitePost && postText.includes('motricité libre')) {
              motricitePost = post;
            }
            if (
              !individualisationPost &&
              postText.includes('individualisation des temps de soin')
            ) {
              individualisationPost = post;
            }
            return !(motricitePost && individualisationPost);
          });
          return !(motricitePost && individualisationPost);
        });
        setPage({
          sectionsdata: sectionsResponse[0].sections,
          categories: categoriesResponse,
          motricitePost,
          individualisationPost,
        });
      }
    } catch (err) {
      setIsError(true);
    }
    setIsLoading(false);
  };
  const { sectionsdata, categories, motricitePost, individualisationPost } =
    page;
  return (
    <div>
      <Header />
      <MetaTags>
        <title>Nos sections - Les P’tits Bouchons</title>
        <meta name="description" content="" />
        <meta
          property="og:title"
          content="Nos sections - Les P’tits Bouchons"
        />
      </MetaTags>
      <div className="lpb-home-pageslider-video-container">
        <div className="uk-cover-container">
          <video
            src={coverVideo}
            controls
            controlsList="nodownload"
            className="lpb-ours-section-pageslider-video-full"
            playsInline
            autoPlay
            loop
            muted
            type="video/mp4"
          />
        </div>
        <div className="uk-text-center lpb-home-pageslider-video-wrap-sections">
          <div className="lpb-home-pageslider-video-title-sections">
            Nos sections
          </div>
        </div>
      </div>
      <div id="bebe">
        <div
          className="lpb-ours-section-imagesitebg"
          style={{
            backgroundImage: 'url(' + Bg + ')',
          }}
        >
          <div className="uk-container">
            <div className=" lpb-ours-section-content" data-uk-grid>
              {sectionsdata && sectionsdata.length > 0 && (
                <div
                  className="uk-width-1-3 lpb-ours-section-bgposition "
                  style={{
                    backgroundImage:
                      'url(' +
                      config.backofficeURL +
                      sectionsdata[0].image.url +
                      ')',
                  }}
                ></div>
              )}
              <div className="uk-width-expand lpb-paddingblocbebe">
                {sectionsdata && sectionsdata.length > 0 && (
                  <h1 className="uk-text-left lpb-ours-section-title">
                    {sectionsdata[0].title}
                  </h1>
                )}{' '}
                {sectionsdata && sectionsdata.length > 0 && (
                  <p className="uk-text-left lpb-ours-section-textTitle">
                    {parse(sectionsdata[0].content)}
                  </p>
                )}
                <div className="" data-uk-grid>
                  {motricitePost && (
                    <div className="uk-width-1-2 lpb-padding-bebe">
                      <Link
                        to={`/posts/${motricitePost.id}`}
                        className=" colorlien"
                      >
                        <div className="lpb-ours-section">
                          <div className="lpb-ours-section-cover" />
                          <div
                            className="img"
                            style={{
                              backgroundImage: `url(${config.backofficeURL}${motricitePost.cover.url})`,
                            }}
                          >
                            {' '}
                          </div>
                          <div className="uk-text-center lpb-content-text">
                            {motricitePost.title}
                          </div>
                        </div>
                      </Link>
                    </div>
                  )}
                  {individualisationPost && (
                    <div className="uk-width-1-2 lpb-padding-bebe">
                      <Link
                        to={`/posts/${individualisationPost.id}`}
                        className=" colorlien"
                      >
                        <div className="lpb-ours-section">
                          <div className="lpb-ours-section-cover" />
                          <div
                            className="img"
                            style={{
                              backgroundImage: `url(${config.backofficeURL}${individualisationPost.cover.url})`,
                            }}
                          >
                            {' '}
                          </div>
                          <div className="uk-text-center lpb-content-text">
                            {individualisationPost.title}
                          </div>
                        </div>
                      </Link>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="petit">
        <div className="lpb-ours-section-petit">
          <div className="uk-container">
            <div className="lpb-ours-section-content petit" data-uk-grid>
              <div className="uk-width-expand lpb-padsectpetit">
                {sectionsdata && sectionsdata.length > 0 && (
                  <h1 className="uk-text-left lpb-ours-section-titreSectOne">
                    {sectionsdata[3].title}
                  </h1>
                )}
                {sectionsdata && sectionsdata.length > 0 && (
                  <p className="uk-text-left lpb-ours-section-textTitle lpb-ours-section-textTitleresponsive">
                    {parse(sectionsdata[3].content)}
                  </p>
                )}
              </div>
              {sectionsdata && sectionsdata.length > 0 && (
                <div
                  className="uk-width-1-3 lpb-ours-section-bgposition-petit"
                  style={{
                    backgroundImage:
                      'url(' +
                      config.backofficeURL +
                      sectionsdata[3].image.url +
                      ')',
                  }}
                ></div>
              )}
            </div>
          </div>
        </div>
      </div>

      <div id="moyen">
        <div
          className="lpb-ours-section-imagesitebg"
          style={{
            backgroundImage: 'url(' + Bg + ')',
          }}
        >
          <div className="uk-container">
            <div className="lpb-ours-section-content" data-uk-grid>
              {sectionsdata && sectionsdata.length > 0 && (
                <div
                  className="uk-width-1-3 lpb-ours-section-bgposition-moyens"
                  style={{
                    backgroundImage:
                      'url(' +
                      config.backofficeURL +
                      sectionsdata[1].image.url +
                      ')',
                  }}
                ></div>
              )}
              <div className="uk-width-expand lpb-marginmoyen">
                {sectionsdata && sectionsdata.length > 0 && (
                  <h1 className="uk-text-left lpb-ours-section-titreSectOne lpb-ours-section-titreSectOneresponsive">
                    {sectionsdata[1].title}
                  </h1>
                )}
                {sectionsdata && sectionsdata.length > 0 && (
                  <p className="uk-text-left lpb-ours-section-textTitle">
                    {parse(sectionsdata[1].content)}
                  </p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="lpb-ours-section-petit">
        <div className="uk-container">
          <div className="lpb-ours-section-content grand" data-uk-grid>
            <div className="uk-width-expand lpb-padsectgrand">
              {sectionsdata && sectionsdata.length > 0 && (
                <h1 className="uk-text-left lpb-ours-section-titreSectOne">
                  {sectionsdata[2].title}
                </h1>
              )}
              {sectionsdata && sectionsdata.length > 0 && (
                <p className="uk-text-left lpb-ours-section-textTitle lpb-ours-section-textresponsive">
                  {parse(sectionsdata[2].content)}
                </p>
              )}
            </div>
            {sectionsdata && sectionsdata.length > 0 && (
              <div
                className="uk-width-1-3 lpb-ours-section-bgposition-grands"
                style={{
                  backgroundImage:
                    'url(' +
                    config.backofficeURL +
                    sectionsdata[2].image.url +
                    ')',
                }}
              ></div>
            )}
          </div>
        </div>
      </div>

      <div
        className="lpb-ours-section-imagesitebg"
        style={{
          backgroundImage: 'url(' + Bg + ')',
        }}
      >
        <div className="uk-container">
          <div className="lpb-ours-section-content-carroussel ">
            <h1 className="uk-text-center lpb-ours-section-titleprojet">
              Nos projets pédagogiques
            </h1>
            <div className="  ">
              <Slick className="" {...settings}>
                {categories.length > 0 &&
                  categories[0].blogs.map((article, key) => (
                    <Link
                      key={key}
                      to={`/posts/${article.id}`}
                      className=" colorlien"
                    >
                      <div className="lpb-ours-section-imgbloc">
                        <div
                          className="lpb-mobimg"
                          style={{
                            backgroundImage: `url(${config.backofficeURL}${article.cover.url})`,
                          }}
                        >
                          {' '}
                        </div>
                        <div>
                          <p className="uk-text-center lpb-ours-section-imgtop">
                            {article.title}
                          </p>
                        </div>
                      </div>
                    </Link>
                  ))}
              </Slick>
            </div>
          </div>
        </div>
      </div>
      <div className="lpb-ours-section-sectionespace">
        <div className="uk-container">
          <div className="lpb-ours-section-content">
            <h1 className="uk-text-center lpb-ours-section-titleprojet">
              Nos espaces fonctionnels
            </h1>
            <div>
              <div className="uk-grid-column-medium uk-child-width-1-3" uk-grid>
                {categories.length > 2 &&
                  categories[2].blogs.map((article, key) => (
                    <div className="lpb-blog-2">
                      <Link key={key} to={`/posts/${article.id}`}>
                        <div className="uk-text-center">
                          <div className="lpb-ours-section">
                            <div className="lpb-ours-section-cover-suite" />
                            <div
                              className="lpb-blog-2-img"
                              style={{
                                backgroundImage: `url(${config.backofficeURL}${article.cover.url})`,
                              }}
                            >
                              {' '}
                            </div>

                            <div className="uk-text-center lpb-content-text display-text ">
                              {article.title}
                            </div>
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))}
                <div className="lpb-blog-2">
                  <HashLink
                    to="/a-propos-de-nous#blocauraacademy"
                    scroll={(el) => scrollWidthOffset(el)}
                    className="link primary"
                  >
                    <div className="uk-text-center">
                      <div className="lpb-ours-section">
                        <div className="lpb-ours-section-cover-suite  " />
                        <div
                          className="lpb-blog-2-img"
                          style={{ backgroundImage: `url(${Imgespace4})` }}
                        >
                          {' '}
                        </div>

                        <div className="uk-text-center lpb-content-text display-text">
                          N’AIRGYM
                        </div>
                      </div>
                    </div>
                  </HashLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Nurseries />
      <Footer />
    </div>
  );
}
