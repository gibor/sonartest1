import React, { Component } from "react";
import Header from "../../components/Header";
import "./index.css";
import MetaTags from 'react-meta-tags';
import Banner from "../../components/Banner";
import Footer from "../../components/Footer";
import l1 from "../../assets/img/l1.png";
import l1c from "../../assets/img/l1c.png";
import l2 from "../../assets/img/l2.png";
import l2c from "../../assets/img/l2c.png";
import l3 from "../../assets/img/l3.png";
import l3c from "../../assets/img/l3c.png";
import l4 from "../../assets/img/l4.png";
import l4c from "../../assets/img/l4c.png";
import l5 from "../../assets/img/l5.png";
import l5c from "../../assets/img/l5c.png";
import l6 from "../../assets/img/l6.png";
import l6c from "../../assets/img/l6c.png";
import l7 from "../../assets/img/l7.png";
import l7c from "../../assets/img/l7c.png";
import l8 from "../../assets/img/l8.png";
import l8c from "../../assets/img/l8c.png";
import l9 from "../../assets/img/l9.png";
import l9c from "../../assets/img/l9c.png";
import l10 from "../../assets/img/l10.png";
import l10c from "../../assets/img/l10c.png";
import l11 from "../../assets/img/l11.png";
import l11c from "../../assets/img/l11c.png";
import l12 from "../../assets/img/l12.png";
import l12c from "../../assets/img/l12c.png";
import l13 from "../../assets/img/l13.png";
import l13c from "../../assets/img/l13c.png";
import l14 from "../../assets/img/l14.png";
import l14c from "../../assets/img/l14c.png";
import l15 from "../../assets/img/l15.png";
import l15c from "../../assets/img/l15c.png";
import l16 from "../../assets/img/l16.png";
import l16c from "../../assets/img/l16c.png";
import l17 from "../../assets/img/l17.png";
import l17c from "../../assets/img/l17c.png";
import Partenaires from "../../assets/img/partenaires.png";

let partenairesImg = {
  part1: l1,
  part2: l2,
  part3: l3,
  part4: l4,
  part5: l5,
  part6: l6,
  part7: l7,
  part8: l8,
  part9: l9,
  part10: l10,
  part11: l11,
  part12: l12,
  part13: l13,
  part14: l14,
  part15: l15,
  part16: l16,
  part17: l17,
};

export default class index extends Component {
  state = {
    showMore: true,
    partenaires: partenairesImg,
  };
  render() {
    const { partenaires } = this.state;
    return (
      <div>
        <Header />
        <MetaTags>
            <title>Partenaire - Les P’tits Bouchons</title>
            <meta name="description" content="" />
            <meta property="og:title" content="Partenaire - Les P’tits Bouchons" />
        </MetaTags>
        <Banner
          backgroundImage={Partenaires}
          position="center"
          title="Partenaires"
          desc=""
        />

        <div className="uk-container">
          <div className="lpb-partenairepadding" data-uk-grid>
          <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part17 = l17c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part17 = l17;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="http://www.mlqe.lu/" target="_blank">
                {" "}
                <img src={partenaires.part17} alt="image" className="" />
              </a>
            </div>
          
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part2 = l2c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part2 = l2;
                this.setState({ partenaires: partImgList });
              }}
            >
              {" "}
              <a href="https://www.liap.lu/" target="_blank">
                <img src={partenaires.part2} alt="image" className="" />{" "}
              </a>
            </div>

            
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part5 = l5c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part5 = l5;
                this.setState({ partenaires: partImgList });
              }}
            >
              {" "}
              <a href="https://www.dschoul.lu/" target="_blank">
                <img src={partenaires.part5} alt="image" className="" />{" "}
              </a>
            </div>

            
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part1 = l1c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part1 = l1;
                this.setState({ partenaires: partImgList });
              }}
            >
              {" "}
              <a href="http://www.les-enfants-du-jeu.com/" target="_blank">
                <img src={partenaires.part1} alt="image" className="" />{" "}
              </a>
            </div>
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part8 = l8c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part8 = l8;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="https://www.pikler.fr/" target="_blank">
                <img src={partenaires.part8} alt="image" className="" />{" "}
              </a>
            </div>
            
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                
                let partImgList = partenaires;
                partImgList.part3 = l3c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part3 = l3;
                this.setState({ partenaires: partImgList });
              }}
            >
              {" "}
              <a href="http://www.ecoledesparents.be/" target="_blank">
                <img src={partenaires.part3} alt="image" className="" />{" "}
              </a>
            </div>
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part7 = l7c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part7 = l7;
                this.setState({ partenaires: partImgList });
              }}
            >
              <img src={partenaires.part7} alt="image" className="" />{" "}
            </div>
            
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part16 = l16c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part16 = l16;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="https://www.lc-academie.lu/" target="_blank">
                <img src={partenaires.part16} alt="image" className="" />{" "}
              </a>
            </div>
           
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part9 = l9c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part9 = l9;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="https://www.luxcontrol.com/" target="_blank">
                {" "}
                <img src={partenaires.part9} alt="image" className="" />
              </a>
            </div>

            <div
              className="uk-width-1-4 "
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part6 = l6c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part6 = l6;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="http://www.luxsecurity.com/" target="_blank">
                <img src={partenaires.part6} alt="image" className="" />{" "}
              </a>
            </div>

            
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part12 = l12c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part12 = l12;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a
                href="https://nairgym-staging.luxdatadigit.lu/"
                target="_blank"
              >
                <img src={partenaires.part12} alt="image" className="" />{" "}
              </a>
            </div>
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part15 = l15c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part15 = l15;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="http://www.csi.lu/index.php/fr/" target="_blank">
                <img src={partenaires.part15} alt="image" className="" />{" "}
              </a>
            </div>
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part11 = l11c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part11 = l11;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="http://www.compass.lu/" target="_blank">
                <img src={partenaires.part11} alt="image" className="" />{" "}
              </a>
            </div>
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part10 = l10c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part10 = l10;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="http://www.testa-rh.lu/" target="_blank">
                <img src={partenaires.part10} alt="image" className="" />{" "}
              </a>
            </div>
          
           <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part4 = l4c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part4 = l4;
                this.setState({ partenaires: partImgList });
              }}
            >
              {" "}
              <a href="https://lautrementdit.net/" target="_blank">
                <img src={partenaires.part4} alt="image" className="" />
              </a>
            </div>
            
            <div
              className="uk-width-1-4"
              onMouseOver={() => {
                let partImgList = partenaires;
                partImgList.part13 = l13c;
                this.setState({ partenaires: partImgList });
              }}
              onMouseLeave={() => {
                let partImgList = partenaires;
                partImgList.part13 = l13;
                this.setState({ partenaires: partImgList });
              }}
            >
              <a href="https://www.3chardons.com/" target="_blank">
                <img src={partenaires.part13} alt="image" className="" />{" "}
              </a>
            </div>
           
            </div>
        </div>

        <Footer />
      </div>
    );
  }
}
