import React, { useState, useEffect } from 'react';
import Nurseries from '../../components/Nurseries';
import bg from '../../assets/img/bg.png';
import blocimagebouchon from '../../assets/img/blocimagebouchon.png';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import './index.css';
import MetaTags from 'react-meta-tags';
import service from '../../service';
import config from '../../config';
import parse from 'html-react-parser';
import axios from 'axios';
import coverVideo from '../../assets/img/video.mp4';
import interview from '../../assets/img/interview.mp4';
import logoacademynew from '../../assets/img/logoacademynew.png';
import ecranvideo from '../../assets/img/ecranvideo.png';
import iconepause from '../../assets/img/iconepause.png';
import { HashLink } from 'react-router-hash-link';
const scrollWidthOffset = (el, yOffset = -90) => {
  const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
  window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' });
};

const $service = service(
  axios,
  config,
  config.backofficeURL
  // config.tokenBackoffice
);
export default function Pedagogie() {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [pedagogiedata, setPedagogiedata] = useState([]);

  useEffect(() => {
    getPedagogiedata();
  }, []);

  const getPedagogiedata = async () => {
    let response = await $service.getPedagogie({});
    if (response) {
      setPedagogiedata(response[0].sections);
    }

    setIsLoading(false);
  };
  return (
    <div>
      <Header />
      <MetaTags>
        <title>Notre pédagogie - Les P’tits Bouchons</title>
        <meta
          name="description"
          content="Nos structures ont pour objectif premier de soutenir la parentalité en proposant des services innovants qui promeuvent l’épanouissement harmonieux des enfants."
        />
        <meta
          property="og:title"
          content="Notre pédagogie - Les P’tits Bouchons"
        />
      </MetaTags>
      <div
        className="lpb-background"
        id="apropos"
        style={{
          backgroundImage: 'url(' + bg + ')',
        }}
      >
        <div className="lpb-home-pageslider-video-container">
          <div className="uk-cover-container">
            <video
              src={coverVideo}
              controls
              controlsList="nodownload"
              className="lpb-ours-section-pageslider-video-full"
              playsInline
              autoPlay
              loop
              muted
              type="video/mp4"
            />
          </div>
          <div className="uk-text-center lpb-home-pageslider-video-wrap-sections">
            <div className="lpb-home-pageslider-video-title-sections">
              Notre pédagogie
            </div>
          </div>
        </div>
        <div className="uk-container lpb-bloctextesousslide">
          <p className="uk-text-center lpb-titlesousslide">
            Nos structures ont pour objectif premier de{' '}
            <strong>soutenir la parentalité</strong> en proposant des services
            innovants <br />
            qui promeuvent{' '}
            <strong> l’épanouissement harmonieux des enfants.</strong>
          </p>
        </div>

        <div className="uk-container">
          <div className="">
            <div className="uk-text-center lpb-icone-play-pedagogie">
              <a id="open-modal" uk-toggle="target: #modal-id" href="#">
                <img src={iconepause} />
              </a>
              <p className="lpb-texteicone-pedagogie uk-text-center">
                Lire la vidéo
              </p>
            </div>
            <div id="modal-id" data-uk-modal>
              <div class="uk-modal-dialog uk-width-auto lpb-width-modal">
                <button
                  class="uk-modal-close-default lpb-buttonclose-pedagogie "
                  type="button"
                  uk-close
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="13"
                    height="13"
                    viewBox="0 0 13 13"
                    fill="none"
                  >
                    <path
                      d="M7.78906 6.5L11.8125 2.51562L12.6328 1.69531C12.75 1.57812 12.75 1.38281 12.6328 1.22656L11.7734 0.367188C11.6172 0.25 11.4219 0.25 11.3047 0.367188L6.5 5.21094L1.65625 0.367188C1.53906 0.25 1.34375 0.25 1.1875 0.367188L0.328125 1.22656C0.210938 1.38281 0.210938 1.57812 0.328125 1.69531L5.17188 6.5L0.328125 11.3438C0.210938 11.4609 0.210938 11.6562 0.328125 11.8125L1.1875 12.6719C1.34375 12.7891 1.53906 12.7891 1.65625 12.6719L6.5 7.82812L10.4844 11.8516L11.3047 12.6719C11.4219 12.7891 11.6172 12.7891 11.7734 12.6719L12.6328 11.8125C12.75 11.6562 12.75 11.4609 12.6328 11.3438L7.78906 6.5Z"
                      fill="black"
                    />
                  </svg>
                </button>
                <div class="uk-modal-body lpb-modal-pedagogie">
                  <video
                    src={interview}
                    controls
                    controlsList="nodownload"
                    className="lpb-videopause"
                    playsInline
                    autoPlay
                    loop
                    muted
                    type="video/mp4"
                  />
                </div>
              </div>
            </div>

            <div>
              <img
                src={ecranvideo}
                alt=""
                className="lpb-image-damesourire-pedagogie"
              />
            </div>
            <div className=""></div>
          </div>
        </div>

        <div className="lpb-blocgreenpedagogie">
          <div className="uk-container ">
            <div className="uk-child-width-expand@s" data-uk-grid>
              <div className="uk-grid-item-match ">
                <div className="uk-card uk-card-default lpb-blocgreentextpedagogie">
                  {pedagogiedata && pedagogiedata.length > 0 && (
                    <h3 className="lpb-titleblocgreenpedagogie">
                      <strong> {parse(pedagogiedata[1].title)}</strong>
                    </h3>
                  )}

                  {pedagogiedata && pedagogiedata.length > 0 && (
                    <p className="lpb-texteblocgreenpedagogie lpb-texteblocgreenpedagogie-responsive">
                      {parse(pedagogiedata[1].content)}
                    </p>
                  )}
                  {/* <p className="lpb-texteblocgreenpedagogie">
                                        C’est la raison pour laquelle il a été décidé de créer <strong>notre propre centre </strong>de <strong>formation</strong> en 2018 : <strong>
                                            « AurAcademy – Human potential Development », afin d’accompagner au plus près l’équipe éducative dans ses pratiques.{" "}
                                        </strong>
                                    </p> */}
                  <div className="lpb-btn-engagement">
                    <HashLink
                      to="/a-propos-de-nous#blocauraacademy"
                      scroll={(el) => scrollWidthOffset(el)}
                      className="link primary"
                    >
                      <button className=" lpb-btn-engagementcolor">
                        Découvrez notre centre de formation
                      </button>
                    </HashLink>
                  </div>
                </div>
              </div>
              <div className="lpb-blocimagegreenpedagogie uk-text-center">
                <img
                  className="lpb-imgblocgreenpedagogie"
                  src={logoacademynew}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="lpb-blocnocolorpedagogie">
          <div className="uk-container lpb-bloctextesousblocgreen">
            {pedagogiedata && pedagogiedata.length > 0 && (
              <p className="uk-text-center lpb-titlesousblocgreen">
                Le pilier de notre concept d’action général :<br />
                <strong className="lpb-strongcommunication">
                  {' '}
                  {pedagogiedata[0].title}
                </strong>
              </p>
            )}
          </div>

          <div className="uk-container lpb-responsiveblocmarronpedagogique">
            <div className="uk-child-width-expand@s" data-uk-grid>
              {pedagogiedata && pedagogiedata.length > 0 && (
                <div className="lpb-blocimagenocolorpedagogie">
                  <img
                    className="lpb-imgblocnocolorpedagogie"
                    src={config.backofficeURL + pedagogiedata[0].image.url}
                  />
                </div>
              )}
              {pedagogiedata && pedagogiedata.length > 0 && (
                <div className="uk-grid-item-match">
                  <div className="uk-card uk-card-default uk-card-body lpb-blocnocolortextpedagogie">
                    <p className="lpb-texteblocnocolorpedagogie">
                      {parse(pedagogiedata[0].content)}{' '}
                    </p>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>

        <div className="lpb-blocgraypedagogie">
          <div className="uk-container">
            <div className="uk-container lpb-blocgraypedagogie">
              <p className="uk-text-center lpb-titleblocgraypedagogie">
                Nos approches pédagogiques :<br />
                Pikler, Montessori, Gardner
              </p>
              <p className="lpb-texteblocgraypedagogie">
                Dans le but de garantir aux enfants un{' '}
                <strong> climat éducatif de qualité</strong>, la crèche-foyer
                les p’tits Bouchons a à cœur de se{' '}
                <strong>former et de performer </strong> au travers de concepts
                pédagogiques variés, qui sont à ce jour les plus{' '}
                <strong>réputés </strong>
                et les plus <strong>épanouissants </strong> <br />
                pour les enfants.
                <br />
                Toutes les pédagogies humanistes desquelles nous nous inspirons
                ont en commun de placer l’<strong>enfant au cœur</strong> de
                leurs fondements de base : l’enfant est considéré comme
                pleinement acteur dans l’acquisition de ses compétences, ses
                apprentissages.
              </p>
              <p className="lpb-texteblocgraypedagogiesecond">
                Nous ne privilégions pas une approche plutôt qu’une autre, nous
                puisons en chacune d’entre elles leurs idéologies principales
                tout en tenant compte de l’âge des enfants.
              </p>
              <div className="lpb-bloccercle">
                <div class="" data-uk-grid>
                  <div>
                    <div class="uk-inline uk-dark">
                      <img src={blocimagebouchon} alt="" />
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '36%', top: '34%' }}
                        href="/posts/6025756bede2a800236cc25c"
                        data-uk-marker
                      ></a>
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '55%', top: '27%' }}
                        href="/posts/602572baede2a800236cc259"
                        data-uk-marker
                      ></a>
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '72%', top: '79%' }}
                        href="/posts/60260821ede2a800236cc275"
                        data-uk-marker
                      ></a>
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '29%', top: '56%' }}
                        href="/posts/60260232ede2a800236cc265"
                        data-uk-marker
                      ></a>
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '72%', top: '34%' }}
                        href="/posts/602606b3ede2a800236cc26e"
                        data-uk-marker
                      ></a>
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '78%', top: '55%' }}
                        href="/posts/60260742ede2a800236cc271"
                        data-uk-marker
                      ></a>
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '36%', top: '76%' }}
                        href="/posts/60260153ede2a800236cc262"
                        data-uk-marker
                      ></a>
                      <a
                        class="uk-position-absolute uk-transform-center lpb-transform"
                        style={{ left: '54%', top: '86%' }}
                        href="/posts/60257672ede2a800236cc25f"
                        data-uk-marker
                      ></a>
                    </div>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Nurseries />
      </div>

      <Footer />
    </div>
  );
}
