import React, { Component } from "react";
import Header from "../../components/Header";
import bg from "../../assets/img/bg.png";
import Footer from "../../components/Footer";
import "./index.css";
import MetaTags from 'react-meta-tags';
import logoImg from "../../assets/img/logo.png";
export default class index extends Component {
  render() {
    return (
      <div>
        <Header />
        <MetaTags>
          <title>Politique de confidentialité</title>
          <meta name="description" content="" />
          <meta property="og:title" content="Politique de confidentialité" />
        </MetaTags>

        <div
          className="lpb-backgroundfoyer"
          id="apropos"
          style={{
            backgroundImage: "url(" + bg + ")",
          }}
        >
          {" "}
          <div className="uk-container lpb-mentionsbloc">
            <div className="lpb-mentionslegales">
              <div className="uk-text-center logopolitique">
                <img className="" src={logoImg} alt="logo" />
              </div>
              <div className="uk-text-center lpb-titrementions">
               Politique de Confidentialité
              </div>

              <p className="lpb-mentionstext">
                Conformément au Règlement Général concernant la Protection des
                Données (RGPD) entré en vigueur le 25 mai 2018, les données
                personnelles collectées sont utilisées par la Société Aura groupe
                exclusivement à des fins professionnelles, pour le bon
                déroulement des activités commerciales et proportionnellement à
                l’objectif poursuivi. Les données que nous recueillons peuvent
                inclure:<br/> -Les détails personnels, par exemple noms, prénoms,
                sexe, date et lieu de naissance ;<br/> -Les détails personnels, par
                exemple noms, prénoms, sexe, date et lieu de naissance;<br/> -Les
                coordonnées, par exemple adresse, adresse email, numéro de
                téléphone fixes et mobiles;<br/> -D’autres données fournies via
                formulaires, communications écrites par courrier ou courriel, en
                ligne ou autre. <br/>Le client donne son consentement à ce que ses
                données personnelles ainsi que celles de son ou ses enfants
                fassent l’objet d’un traitement exclusivement à des fins de
                gestion commerciale, financière et administrative et s’engage à
                informer immédiatement la Société Aura Groupe de tout changement
                concernant ses données personnelles. L’accès aux données
                personnelles sera strictement limité aux collaborateurs de la
                Société, habilités à les traiter en raison de leurs fonctions.
                Les données seront conservées au sein du département finance et
                seront informatiquement enregistrées dans notre logiciel de
                gestion réservé à cet effet. Les données sont traitées de manière
                confidentielle et ne seront communiquées à aucun tiers. Les
                données sont traitées de façon à garantir une sécurité optimale,
                y compris la protection contre le traitement non autorisé ou
                illicite et contre la perte, la destruction ou les dégâts
                d'origine accidentelle, à l'aide de mesures techniques ou
                organisationnelles appropriées.
              </p>


              <p className="lpb-mentionstext">
                Conformément à la loi n° 78-17
                du 6 janvier 1978 relative à l’informatique, aux fichiers et aux
                libertés, telle que modifiée par la loi n° 2004-801 du 6 août
                2004,et par le Règlement Européen n°2016/.679, le client dispose
                d’un droit d’accès,de rectification, d’effacement, et de
                portabilité des données le concernant, ainsi que du droit de
                s’opposer au traitement pour motif légitime, droits qu’il
                peut exercer en s’adressant au responsable de traitement par
                courrier à l’adresse postale ci-après mentionnée ou par email, en
                joignant un justificatif de son identité valide. Adresse postale:
                10 rue de l’Avenir L-3895 Foetz Email:
                arthur.veissiere@aura-groupe.lu. Les informations et données
                personnelles sont conservées à des fins de sécuritéet dans le
                but de respecter les obligations légales et réglementaires.
                Elles seront conservées aussi longtemps que nécessaire afin de
                garantir l’exécution du présent contrat et conformément aux
                délais légaux de conservation des documents en vigueur.
              </p>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
