import React, { useState, useEffect, Fragment } from "react";
import "./index.css";
import MetaTags from 'react-meta-tags';
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Banner from "../../components/Banner";
import Creches from "../../components/Creches";
import Nurseries from "../../components/Nurseries";
import bg from "../../assets/img/bg.png";
import parse from "html-react-parser";
import service from "../../service";
import config from "../../config";
import axios from "axios";
import { useLocation } from "react-router-dom";

const $service = service(
  axios,
  config,
  config.backofficeURL,
  config.tokenBackoffice
);

export default function Schifflange() {
  const { pathname } = useLocation();
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [page, setPage]=useState(null);
  useEffect(() => {
    getPageSections();
  }, []);

  const  getPageSections = async () => {
    try {
      let response = await $service.getPages({
        url: pathname
      });
      if (response && response.length>0) {
        setPage(response[0]);
        
      }
    } catch (err) {
      setIsError(true);
    }
    setIsLoading(false);
  };
  return (
    <div>
      <Header />
      <MetaTags>
        <title>Crèche à Schifflange - Les P’tits Bouchons</title>
        <meta name="description" content="Retrouvez dans notre structure de Schifflange le savoir-faire des P’tits Bouchons. La crèche est composée de 2 sections distinctes, elles-mêmes divisées en espaces fonctionnels" />
        <meta property="og:title" content="Crèche à Schifflange - Les P’tits Bouchons" />
      </MetaTags>
      <div
        className="lpb-backgroundfoyer"
        id="apropos"
        style={{
          backgroundImage: "url(" + bg + ")",
        }}
      >
        {isLoading && <div><div style={{flex: "initial",width: "auto"}} className="loader"><div data-uk-spinner /></div></div>}
        {
          (!isLoading && page) &&
           
          <div>
              <Banner
                backgroundImage={`${config.backofficeURL}${page.sections[0].image.url}`}
                position="center"
                title={page.sections[0].title}
                desc={parse(page.sections[0].content)}
                typeBtn="comingSoon"
            btnText="COMING SOON..."
              />
              <Creches
              bloctexte={parse(page.sections[1].content)}
              address={parse(page.sections[2].content)}
              titrecreche={page.sections[3].title}
              textecreche={parse(page.sections[3].content)}
              imagebloc2={`${config.backofficeURL}${page.sections[3].image.url}`}
              titlebebe=" Section bébés"
              textebebe=" De 2 à 18 mois"
              titlepetit="Section petits"
              textepetit="De 18 mois à 2 ans et demi"
              titlemoyen=" Section moyens"
              textemoyen=" De 2 ans et demi à 4 ans"
              btncrechetexte="  Pré-inscription"
            />
            <Nurseries/>
          </div>
         }
      </div>
      <Footer />
    </div>
  );
}
