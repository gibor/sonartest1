import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import './index.css';
import MetaTags from 'react-meta-tags';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import LpbNeed from '../../components/Tunnel/Need';
import LpbChoice from '../../components/Tunnel/Choice';
import LpbInfo from '../../components/Tunnel/Info';
import LpbValidation from '../../components/Tunnel/Validation';

import familyImg from '../../assets/img/Family9865.png';
import Bg from '../../assets/img/bg.png';

import service from '../../service';
import config from '../../config';
import axios from 'axios';
const $service = service(axios, config, config.serverURL);

const $serviceBackoffice = service(
  axios,
  config,
  config.backofficeURL
  // config.tokenBackoffice
);

export default function Home() {
  const history = useHistory();
  let localState = {
    step: 1,
    formNeed: {
      nbrChildren: 1,
      children: [
        {
          isValid: false,
          notChildFirstname: false,
          lastname: '',
          firstname: '',
          birthday: '',
          crecheday: '',
          errors: {
            lastname: true,
            firstname: true,
            birthday: true,
            crecheday: true,
          },
        },
      ],
    },
    formInfo: {
      nbrParent: 1,
      parents: [
        {
          isValid: false,
          civility: 'Monsieur',
          lastname: '',
          firstname: '',
          adress: '',
          codepostal: '',
          city: '',
          country: '',
          phone: '',
          email: '',
          errors: {
            civility: true,
            lastname: true,
            firstname: true,
            adress: true,
            codepostal: true,
            city: true,
            country: true,
            phone: true,
            email: true,
          },
        },
      ],
    },
    formChoice: {
      crecheType: [],
      allCreches: [
        {
          id: 'foetz',
          title: 'Crèche de Foetz',
          adress: '3, rue des Artisans L-3895 Foetz',
          phone: '(+352) 26 55 02 98 (secrétariat)',
          number: '+35226550298',
          time: 'Lun. au ven. 05h -22h',
          active: true,
        },
        {
          id: 'foyerFoetz',
          title: 'Foyer de jour de Foetz',
          adress: '3, rue des Artisans L-3895 Foetz',
          phone: '(+352) 26 55 02 98 (secrétariat)',
          number: '+35226550298',
          time: 'Lun. au ven. 11h -20h',
          active: true,
        },
        {
          id: 'leudelange',
          title: 'Crèche de Leudelange',
          adress: '5 Rue Léon Laval L-3372 Leudelange',
          phone: '(+352) 26 55 02 98 (secrétariat)',
          number: '+35226550298',
          time: 'Lun. au ven. 05h -22h',
          active: true,
        },
        {
          id: 'alzette',
          title: 'Foyer de jour Esch-sur-Alzette',
          adress: '31 rue Louis Pasteur L-4276 Esch-sur-Alzette',
          phone: '(352) 26 55 02 98 (Sécrétariat)',
          number: '+35226550298',
          time: '',
          active: false,
        },
        {
          id: 'schifflange',
          title: 'Crèche de Schifflange',
          adress:
            'Centre commercial Op Herbert Z.A Op Herbert L-3862 Schifflange',
          phone: '(352) 26 55 02 98 (Sécrétariat)',
          number: '+35226550298',
          time: '',
          active: false,
        },
        /*
                {
                    id: "schifflange",
                    title: "Crèche de Schifflange",
                    adress: "Schifflange",
                    phone: "",
                    time: "Lun. au ven. 05h - 22h",
                    active: false,
                }
                */
      ],
    },
    formValidation: {
      validCGU: false,
      validNewsletter: false,
    },
  };

  const firstRender = useRef(true);
  const [currentState, setCurrentState] = useState(localState);
  const [successRegister, setSuccessRegister] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [validationForm, setValidationForm] = useState(false);

  useEffect(() => {
    if (firstRender.current) {
      validatedForm();
      firstRender.current = false;
      return;
    }
    validatedForm();
  }, [currentState]);

  const goToStepOne = () => {
    setSuccessRegister(false);
    applyResetTunnel();
    window.scrollTo(0, 0);
  };

  const applyResetTunnel = () => {
    setCurrentState(localState);
    history.push('/');
  };

  const applyFormNeedUpdate = (formNeed) => {
    setCurrentState({ ...currentState, formNeed });
  };

  const applyFormInfoUpdate = (formInfo) => {
    setCurrentState({ ...currentState, formInfo });
  };

  const applyFormChoiceUpdate = (formChoice) => {
    setCurrentState({ ...currentState, formChoice });
  };

  const applyFormValidationUpdate = (formValidation) => {
    setCurrentState({ ...currentState, formValidation });
  };

  const validatedForm = () => {
    let x;
    if (currentState.step == 1) {
      x = currentState.formNeed.children.every(
        (child) => child.isValid === true
      );
    } else if (currentState.step == 2) {
      x = currentState.formInfo.parents.every(
        (parent) => parent.isValid === true
      );
    } else if (currentState.step == 3) {
      x = currentState.formChoice.crecheType.length !== 0;
    } else if (currentState.step == 4) {
      x =
        currentState.formNeed.children.every(
          (child) => child.isValid === true
        ) &&
        currentState.formInfo.parents.every(
          (parent) => parent.isValid === true
        ) &&
        currentState.formChoice.crecheType.length !== 0 &&
        currentState.formValidation.validCGU;
    } else {
      x = false;
    }

    setValidationForm(!x);
  };

  const fetchApplySubmitForm = async (direction) => {
    if (direction == 'prev') {
      setCurrentState({ ...currentState, step: currentState.step - 1 });
      window.scrollTo(0, 380);
    } else if (direction == 'next') {
      if (currentState.step < 4) {
        setCurrentState({ ...currentState, step: currentState.step + 1 });
        window.scrollTo(0, 380);
      } else if (currentState.step == 4) {
        // last Step here = Call Action API
        if (window.fbq) {
          window.fbq('track', 'CompleteRegistration');
        }
        let children = [];
        let parent = [];
        let crecheObject = [];

        setIsLoading(true);
        setIsError(false);
        setSuccessRegister(false);

        currentState.formNeed.children.map((el) => {
          children.push({
            nameChildren: el.lastname,
            surnameChildren: el.firstname,
            birthDateChildren: el.birthday,
            startCrecheDateChildren: el.crecheday,
          });
        });
        currentState.formInfo.parents.map((el) => {
          parent.push({
            gender: el.civility,
            parentName: el.lastname,
            parentFistName: el.firstname,
            parentAddress: el.adress,
            parentPostalCode: el.codepostal,
            parentCity: el.city,
            parentCountry: el.country,
            parentPhone: el.phone,
            parentEmail: el.email,
          });
        });
        currentState.formChoice.allCreches.map((el) => {
          if (currentState.formChoice.crecheType.includes(el.id)) {
            crecheObject.push({
              id: el.id,
              name: el.title,
              address: el.adress,
              phone: el.phone,
              availability: el.time,
            });
          }
        });

        try {
          let byReq = {
            children,
            parent,
            crecheObject,
            acceptCom: currentState.formValidation.validCGU,
            acceptDataprotection: currentState.formValidation.validNewsletter,
          };
          let res = await $service.preRegistration({ ...byReq });
          await $serviceBackoffice.savePreRegisterOffice({ data: byReq });

          setIsLoading(false);
          window.scrollTo(0, 380);

          if (res.status === 'success') {
            setSuccessRegister(true);
            window.scrollTo(0, 0);
            setCurrentState(localState);
          }
        } catch (err) {
          setIsError(true);
          setIsLoading(false);
          setSuccessRegister(false);
        }
      }
    }
  };

  return (
    <div>
      <Header />
      <MetaTags>
        <title>Pré-inscription - Les P’tits Bouchons</title>
        <meta
          name="description"
          content="Désormais, pré-inscrivez vos enfants à nos foyers et crèche de jour aux Luxembourg en passant directement par notre site web. "
        />
        <meta
          property="og:title"
          content="Pré-inscription - Les P’tits Bouchons"
        />
      </MetaTags>
      <div className="lpb-home-parent-page">
        <div
          className={`lpb-home-img-full-container uk-cover-container ${
            successRegister ? 'lpb-register-card-thank-content' : ''
          }`}
        >
          <img src={familyImg} alt="family" data-uk-cover />
        </div>
        <div
          style={{
            backgroundImage: 'url(' + Bg + ')',
          }}
        >
          <div
            className={`lpb-container-center-bloc lpb-register-card-container ${
              successRegister ? 'lpb-register-card-container-thank-box' : ''
            }`}
          >
            {!successRegister && (
              <div className="uk-card uk-card-default uk-card-body lpb-register-card-wrap">
                <div className="uk-text-center lpb-register-card-head-content">
                  {currentState.step == 1 && (
                    <div>
                      <div className="lpb-register-card-head-title">
                        Votre / Vos enfant(s)
                      </div>
                      <div className="lpb-register-card-head-desc">
                        Cette pré-inscription n’a pas de valeur d’engagement. Ce
                        formulaire est <br />
                        une demande de place en crèche/foyer de jour, toutes les
                        données <br />
                        resteront confidentielles. <br />
                        <div>
                          <strong>
                            Nos structures sont agréées et prestataires des
                            Chèques-Services Accueil.
                          </strong>
                        </div>
                      </div>
                    </div>
                  )}
                  {currentState.step == 2 && (
                    <div>
                      <div className="lpb-register-card-head-title">Vous</div>
                      <div className="lpb-register-card-head-desc">
                        Veuillez renseigner vos informations personnelles.
                      </div>
                    </div>
                  )}
                  {currentState.step == 3 && (
                    <div>
                      <div className="lpb-register-card-head-title">
                        Choix de la structure
                      </div>
                      <div className="lpb-register-card-head-desc">
                        Veuillez sélectionner une crèche / un foyer de jour.
                      </div>
                    </div>
                  )}
                  {currentState.step == 4 && (
                    <div>
                      <div className="lpb-register-card-head-title">
                        Confirmation des <br />
                        informations fournies
                      </div>
                      <div className="lpb-register-card-head-desc">
                        Veuillez vérifier et confirmer l’exactitude <br />
                        des informations que vous avez fournies.
                      </div>
                    </div>
                  )}
                </div>
                <div className="lpb-register-step-wrap">
                  <div>
                    <div className="lpb-register-step-cicle-divider">
                      <hr />
                    </div>
                    <div
                      className="uk-text-center lpb-register-step-cicle-container"
                      data-uk-grid
                    >
                      <div className="uk-width-1-4">
                        <div
                          className={`uk-text-center ${
                            currentState.step >= 1
                              ? 'lpb-register-step-cicle-active'
                              : 'lpb-register-step-cicle-inactive'
                          }`}
                        >
                          <svg
                            width={19}
                            height={24}
                            viewBox="0 0 19 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M6.625 3.375C6.625 5.25 8.125 6.75 10 6.75C11.8281 6.75 13.375 5.25 13.375 3.375C13.375 1.54688 11.8281 0 10 0C8.125 0 6.625 1.54688 6.625 3.375ZM18.5312 3.46875C17.9688 2.85938 16.9844 2.85938 16.4219 3.46875L12.3438 7.5H7.60938L3.53125 3.46875C2.96875 2.85938 1.98438 2.85938 1.42188 3.46875C0.8125 4.03125 0.8125 5.01562 1.42188 5.57812L5.875 10.0312V22.5C5.875 23.3438 6.53125 24 7.375 24H8.125C8.92188 24 9.625 23.3438 9.625 22.5V17.25H10.375V22.5C10.375 23.3438 11.0312 24 11.875 24H12.625C13.4219 24 14.125 23.3438 14.125 22.5V10.0312L18.5312 5.57812C19.1406 5.01562 19.1406 4.03125 18.5312 3.46875Z"
                              fill="white"
                            />
                          </svg>
                        </div>
                        <div className="lpb-register-step-cicle-subtitle">
                          Votre /<br />
                          Vos enfant(s)
                        </div>
                      </div>
                      <div className="uk-width-1-4">
                        <div
                          className={`uk-text-center ${
                            currentState.step >= 2
                              ? 'lpb-register-step-cicle-active'
                              : 'lpb-register-step-cicle-inactive'
                          }`}
                        >
                          <svg
                            width={6}
                            height={16}
                            viewBox="0 0 6 16"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M0.625 13.2812C0.25 13.2812 0 13.5625 0 13.9062V15.375C0 15.75 0.25 16 0.625 16H5.375C5.71875 16 6 15.75 6 15.375V13.9062C6 13.5625 5.71875 13.2812 5.375 13.2812H4.75V6.625C4.75 6.28125 4.46875 6 4.125 6H0.625C0.25 6 0 6.28125 0 6.625V8.125C0 8.46875 0.25 8.75 0.625 8.75H1.25V13.2812H0.625ZM3 0C1.75 0 0.75 1.03125 0.75 2.25C0.75 3.5 1.75 4.5 3 4.5C4.21875 4.5 5.25 3.5 5.25 2.25C5.25 1.03125 4.21875 0 3 0Z" />
                          </svg>
                        </div>
                        <div className="lpb-register-step-cicle-subtitle">
                          Vos
                          <br />
                          Informations
                        </div>
                      </div>
                      <div className="uk-width-1-4">
                        <div
                          className={`uk-text-center ${
                            currentState.step >= 3
                              ? 'lpb-register-step-cicle-active'
                              : 'lpb-register-step-cicle-inactive'
                          }`}
                        >
                          <svg
                            width={14}
                            height={16}
                            viewBox="0 0 14 16"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M10.75 11.25H6.25C6.09375 11.25 6 11.375 6 11.5V12.5C6 12.6562 6.09375 12.75 6.25 12.75H10.75C10.875 12.75 11 12.6562 11 12.5V11.5C11 11.375 10.875 11.25 10.75 11.25ZM3.5 13H4.5C4.75 13 5 12.7812 5 12.5V11.5C5 11.25 4.75 11 4.5 11H3.5C3.21875 11 3 11.25 3 11.5V12.5C3 12.7812 3.21875 13 3.5 13ZM3.5 5H4.5C4.75 5 5 4.78125 5 4.5V3.5C5 3.25 4.75 3 4.5 3H3.5C3.21875 3 3 3.25 3 3.5V4.5C3 4.78125 3.21875 5 3.5 5ZM6.25 4.75H10.75C10.875 4.75 11 4.65625 11 4.5V3.5C11 3.375 10.875 3.25 10.75 3.25H6.25C6.09375 3.25 6 3.375 6 3.5V4.5C6 4.65625 6.09375 4.75 6.25 4.75ZM13 0H1C0.4375 0 0 0.46875 0 1V15C0 15.5625 0.4375 16 1 16H13C13.5312 16 14 15.5625 14 15V1C14 0.46875 13.5312 0 13 0ZM12.5 14.5H1.5V1.5H12.5V14.5ZM4.1875 8.96875C4.25 9.03125 4.375 9.03125 4.4375 8.96875L6.4375 6.96875C6.5 6.90625 6.5 6.8125 6.4375 6.75L6.03125 6.34375C5.96875 6.28125 5.875 6.28125 5.8125 6.34375L4.3125 7.8125L3.6875 7.15625C3.59375 7.09375 3.5 7.09375 3.4375 7.15625L3.03125 7.5625C2.96875 7.625 2.96875 7.71875 3.03125 7.78125L4.1875 8.96875ZM10.75 7.25H7.40625C7.34375 7.40625 7.25 7.5625 7.125 7.6875L6.0625 8.75H10.75C10.875 8.75 11 8.65625 11 8.5V7.5C11 7.375 10.875 7.25 10.75 7.25Z" />
                          </svg>
                        </div>
                        <div className="lpb-register-step-cicle-subtitle">
                          Nos crèches <br />
                          et foyers
                        </div>
                      </div>
                      <div className="uk-width-1-4">
                        <div
                          className={`uk-text-center ${
                            currentState.step >= 4
                              ? 'lpb-register-step-cicle-active'
                              : 'lpb-register-step-cicle-inactive'
                          }`}
                        >
                          <svg
                            width={17}
                            height={12}
                            viewBox="0 0 17 12"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M6.40625 11.75C6.71875 12.0625 7.25 12.0625 7.5625 11.75L16.75 2.5625C17.0625 2.25 17.0625 1.71875 16.75 1.40625L15.625 0.28125C15.3125 -0.03125 14.8125 -0.03125 14.5 0.28125L7 7.78125L3.46875 4.28125C3.15625 3.96875 2.65625 3.96875 2.34375 4.28125L1.21875 5.40625C0.90625 5.71875 0.90625 6.25 1.21875 6.5625L6.40625 11.75Z" />
                          </svg>
                        </div>
                        <div className="lpb-register-step-cicle-subtitle">
                          Confirmation
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div />
                {!isLoading && (
                  <div>
                    {currentState.step == 1 && (
                      <LpbNeed
                        formNeedUpdate={applyFormNeedUpdate}
                        formNeed={currentState.formNeed}
                      />
                    )}
                    {currentState.step == 2 && (
                      <LpbInfo
                        formInfoUpdate={applyFormInfoUpdate}
                        formInfo={currentState.formInfo}
                      />
                    )}
                    {currentState.step == 3 && (
                      <LpbChoice
                        formChoiceUpdate={applyFormChoiceUpdate}
                        formChoice={currentState.formChoice}
                      />
                    )}
                    {currentState.step == 4 && (
                      <LpbValidation
                        formValidationUpdate={applyFormValidationUpdate}
                        formChoiceUpdate={applyFormChoiceUpdate}
                        formInfoUpdate={applyFormInfoUpdate}
                        formNeedUpdate={applyFormNeedUpdate}
                        formNeed={currentState.formNeed}
                        formInfo={currentState.formInfo}
                        formChoice={currentState.formChoice}
                        formValidation={currentState.formValidation}
                      />
                    )}
                  </div>
                )}
                {isLoading && <div data-uk-spinner />}
                <div className="uk-text-right">
                  {currentState.step === 1 ? (
                    <a href="/" className="lpb-register-link-not-bg-footer">
                      <button
                        className="uk-button uk-button-default lpb-register-btn-not-bg-footer"
                        disabled={isLoading}
                      >
                        Annuler
                      </button>
                    </a>
                  ) : (
                    <button
                      onClick={() => fetchApplySubmitForm('prev')}
                      className="uk-button uk-button-default lpb-register-btn-not-bg-footer"
                      disabled={isLoading}
                    >
                      Précédent
                    </button>
                  )}
                  <button
                    onClick={() => fetchApplySubmitForm('next')}
                    className={`uk-button uk-button-default lpb-register-btn-full-footer lpb-traking-tunnel-btn-step-${currentState.step}`}
                    disabled={validationForm}
                  >
                    {currentState.step == 4 ? (
                      <span>Valider</span>
                    ) : (
                      <span>Suivant</span>
                    )}
                  </button>
                  {isLoading && <div data-uk-spinner />}
                  {isError && (
                    <div className="lpb-form-erros-send">
                      Une erreur s'est produite lors de la soumission
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      {successRegister && (
        <div
          className={`${
            successRegister ? 'lpb-register-card-thank-container' : ''
          }`}
        >
          <div className="uk-card uk-card-default uk-card-body lpb-register-card-thank-wrap">
            <div className="uk-text-center">
              <div>
                <svg
                  width={94}
                  height={94}
                  viewBox="0 0 94 94"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M47 0.5C21.3125 0.5 0.5 21.5 0.5 47C0.5 72.6875 21.3125 93.5 47 93.5C72.5 93.5 93.5 72.6875 93.5 47C93.5 21.5 72.5 0.5 47 0.5ZM47 9.5C67.625 9.5 84.5 26.375 84.5 47C84.5 67.8125 67.625 84.5 47 84.5C26.1875 84.5 9.5 67.8125 9.5 47C9.5 26.375 26.1875 9.5 47 9.5ZM73.25 34.0625L68.9375 29.75C68.1875 28.8125 66.6875 28.8125 65.75 29.75L39.3125 56L28.0625 44.75C27.125 43.8125 25.8125 43.8125 24.875 44.75L20.5625 48.875C19.8125 49.8125 19.8125 51.3125 20.5625 52.0625L37.625 69.3125C38.5625 70.25 39.875 70.25 40.8125 69.3125L73.25 37.25C74 36.3125 74 34.8125 73.25 34.0625Z"
                    fill="#A8B674"
                  />
                </svg>
              </div>
              <div className="lpb-register-card-thank-title">Merci !</div>
              <div className="lpb-register-card-thank-subtitle">
                Votre demande a été bien prise en compte. Nous vous
                recontacterons dans les plus brefs délais.
              </div>
              <div
                className="lpb-register-card-thank-link"
                onClick={() => goToStepOne()}
              >
                Retour à l’accueil
              </div>
            </div>
          </div>
        </div>
      )}
      <Footer />
    </div>
  );
}
